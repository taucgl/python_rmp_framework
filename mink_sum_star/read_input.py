import sys

def read_data_file(filename):
    """
    Read the data files.
    They are in format:

    num_polygons
        num_boundaries_in_poly1
            number_of_vertices
                x y
                x y
                ...
            number_of_vertices
                x y
                x y
                ..
            ...
        num_boundaries_in_poly2
            .
            .
            .
    """
    with open(filename, 'r') as fp:
        lines = fp.readlines()
    
    polygons = []
    idx = 1
    while idx < len(lines):
        num_boundaries = int(lines[idx])
        idx += 1
        polygon = []
        while len(polygon) < num_boundaries:
            boundary = []
            num_vertices = int(lines[idx])
            idx += 1
            for _ in range(num_vertices):
                point = tuple(lines[idx].strip().split())
                x, y = point
                point = (float(x), float(y))
                boundary.append(point)
                idx += 1
            polygon.append(boundary)
        polygons.append(polygon)

    return polygons

if __name__ == "__main__":
    # Test program for reading the input
    polygons = read_data_file(sys.argv[1])

    for polygon in polygons:
        print(polygon)