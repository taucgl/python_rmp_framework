from bindings import *

###################################################
# The computational geometry code of the software #
###################################################

def minkowski_multi_polygon(polygons_a, polygons_b):
    """
    Get two lists of polygons, and compute minkowski sum of each pair
    Both lists are lists of CGAL Polygon_2 or Polygon_2_with_holes
    (And so is the output)
    """
    res = []
    for poly_a in polygons_a:
        for poly_b in polygons_b:
            res.append(MN2.minkowski_sum_2(poly_a, poly_b))
    return res


def unionize_polygons(polygons: list):
    for i in range(len(polygons)):
        for j in range(i+1, len(polygons)):
            # Try to union
            union = Polygon_with_holes_2()
            res = BSO2.join(polygons[i], polygons[j], union)
            if res:
                # If union is successful
                poly_i = polygons[i]
                poly_j = polygons[j]
                polygons.remove(poly_i)
                polygons.remove(poly_j)
                polygons.append(union)
                return unionize_polygons(polygons)
    return polygons


def minkowski_sum_star(polygons, n):
    """
    Get a list of  CGAL polygons (as specified in that data input) and sum it with himself n times (and divide by n)
    Return the new list of CGAL polygons
    """
    res = polygons

    for _ in range(n-1):
        res = minkowski_multi_polygon(res, polygons)
        res = unionize_polygons(res)
    
    return res


def list_of_point_to_CGAL_poly(points):
    """
    Convert a list of tuples to a CGAL Polygon_2
    """
    cgal_points = []
    for point in points:
        x, y = point
        cgal_points.append(Point_2(x, y))
    return Polygon_2(cgal_points)


def Polygon_2_to_points(cgal_polygon):
    """
    Convert a CGAL Polygon to a list of points
    """
    points = []
    for point in cgal_polygon.vertices():
        x, y = point.x().to_double(), point.y().to_double()
        points.append((x,y))
    return points


def Polygon_with_holes_2_to_points_and_holes(cgal_polygon_with_holes):
    """
    Convert a CGAL Polygon to a list of points and a list of holes
    """
    points = []
    for point in cgal_polygon_with_holes.outer_boundary().vertices():
        x, y = point.x().to_double(), point.y().to_double()
        points.append((x,y))
    holes = []
    for hole in cgal_polygon_with_holes.holes():
        hole_points = []
        for point in hole.vertices():
            x, y = point.x().to_double(), point.y().to_double()
            hole_points.append((x,y))
        holes.append(hole_points)
    return points, holes
