"""
This is a minimal example that demonstrates usage of all different Qt GUI objects
that are available on this framework.
"""

import sys

from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import QFileDialog

from gui_mink_sum_star import GUI_mink_sum_star

import read_input
from cg_algorithms import *


#############################
# Connect everything to GUI #
#############################
def enable(gui):
    """
    Setup the logic of the GUI program
    """
    # Zoom out a bit
    gui.zoom = 20

    # Set also new variables in the GUI state
    gui.snapthot_dict = {}
    gui.current_n = 1

    gui.set_logic('nextButton', lambda: next_button(gui))
    gui.set_logic('prevButton', lambda: prev_button(gui))
    gui.set_logic('loadButton', lambda: load_data(gui))
    gui.set_logic('clearButton', lambda: clear_button(gui))


def next_button(gui):
    """
    The logic of pressing the next iteration button in GUI
    """
    if 1 not in gui.snapthot_dict:
        return
    gui.current_n += 1
    gui.set_label('nLabel', str(gui.current_n))

    if gui.current_n not in gui.snapthot_dict:
        gui.snapthot_dict[gui.current_n] = minkowski_sum_star(gui.snapthot_dict[1], gui.current_n)
    draw_snapshot(gui, gui.current_n)


def prev_button(gui):
    """
    The logic of pressing the previous iteration button in GUI
    """
    if gui.current_n > 1:
        gui.current_n -= 1
    gui.set_label('nLabel', str(gui.current_n))
    draw_snapshot(gui, gui.current_n)


def load_data(gui):
    """
    Opens a dialog and loads a selected data file
    """
    filename, _ = QFileDialog.getOpenFileName(gui.mainWindow, 'Open file', '.', "Text files (*.txt)")
    if filename == '':
        return
    polygons = read_input.read_data_file(filename)
    cgal_polygons = []
    for polygon in polygons:
        points = polygon[0]
        cgal_base_polygon = list_of_point_to_CGAL_poly(points)
        if len(polygon) > 1:
            holes = polygon[1:]
            cgal_holes = []
            for hole in holes:
                cgal_holes.append(list_of_point_to_CGAL_poly(hole))
            cgal_polygons.append(Polygon_with_holes_2(cgal_base_polygon, cgal_holes))
        else:
            cgal_polygons.append(cgal_base_polygon)
    clear_button(gui)
    gui.snapthot_dict[1] = cgal_polygons
    draw_snapshot(gui, 1)


def draw_snapshot(gui, n):
    """
    Draw the nth snapshot on screen
    Note that here we divide by n so all sums will be compatible
    """
    if 1 not in gui.snapthot_dict:
        # If no scene, we cant draw anythinh
        return
    gui.clear_scene()
    for poly in gui.snapthot_dict[n]:
        if type(poly) is Polygon_2:
            points = Polygon_2_to_points(poly)
            points = list(map(lambda p: (p[0] / n, p[1] / n), points))
            gui.add_polygon(points, QtCore.Qt.blue, QtCore.Qt.transparent)
        if type(poly) is Polygon_with_holes_2:
            points, holes = Polygon_with_holes_2_to_points_and_holes(poly)
            points = list(map(lambda p: (p[0] / n, p[1] / n), points))
            holes = [
                list(map(lambda p: (p[0] / n, p[1] / n), hole))
            for hole in holes]
            gui.add_polygon_with_holes(points, holes, QtCore.Qt.blue)


def clear_button(gui):
    """
    Clear the current scene
    """
    gui.clear_scene()
    gui.current_n = 1
    gui.snapthot_dict = {}
    gui.set_label('nLabel', '1')


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    gui = GUI_mink_sum_star()
    gui.set_program_name("Minkowski Sum Star")
    enable(gui)
    gui.mainWindow.show()
    sys.exit(app.exec_())
