# Minkowski Sum Star
A simple app that demonstrates repeated minkowski sum of a polygon with itself.

You can run by `python mink_sum_star/mink_sum_star.py` (from a `pipenv shell` located in the project's root).
Use `"Load data"` button to choose a data file (supplies in the `data` folder).
Then you can use the right buttons to go forward or backwards in the star sum.
You can also use `"Clear"` button to clear the scene.
At any point you may load another file.