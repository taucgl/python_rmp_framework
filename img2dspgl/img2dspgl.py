"""
This is a minimal example that demonstrates usage of all different Qt GUI objects
that are available on this framework.
"""

import sys
import json
import random

import cv2
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtGui import QImage, QPainter, QColor

from gui.gui import GUI

GRID_SIZE = 200
resolution = 1.0

class GUITest(GUI):
    def setupUi(self):
        #########################
        # Setup the Qt UI layout
        #########################
        # Resize the main window and set the stylesheet (CSS)
        MainWindow = self.mainWindow
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        MainWindow.setStyleSheet("QMainWindow { background-color : rgb(54, 57, 63); color : rgb(220, 221, 222); }\n"
                                 "QLabel { background-color : rgb(54, 57, 63); color : rgb(220, 221, 222); }")

        #################
        # Important!: the area of the scene
        #################
        self.area = QtCore.QRect(10, 10, 780, 580)

        # Add a graphics widget to the main window
        self.graphicsView = QtWidgets.QGraphicsView(self.mainWindow)
        self.graphicsView.setEnabled(True)
        self.graphicsView.setGeometry(self.area)
        self.graphicsView.setObjectName("graphicsView")

        # Zoom out a bit
        self.zoom = 40
        self.redraw()

        self.grid = []


    def render_to_image(self, file_name):
        image = QImage(self.area.size(), QImage.Format_ARGB32_Premultiplied)
        painter = QPainter(image)
        self.graphicsView.render(painter, QtCore.QRectF(image.rect()), self.area)
        painter.end()
        image.save(file_name)

    def redraw_grid(self, size):
        for segment in self.grid:
            self.scene.removeItem(segment.line)
        self.grid.clear()
        # color = QtCore.Qt.lightGray
        color = QColor(230, 230, 230)
        length = size * resolution
        for i in range(-size, size):
            if i == 0:
                continue
            i = i * resolution
            self.grid.append(self.add_segment(-length, i, length, i, line_color=color))
            self.grid.append(self.add_segment(i, -length, i, length, line_color=color))
        color = QtCore.Qt.black
        self.grid.append(self.add_segment(-length, 0, length, 0, line_color=color))
        self.grid.append(self.add_segment(0, -length, 0, length, line_color=color))
        for rline in self.grid:
            rline.line.setZValue(-1)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    gui = GUITest()
    gui.set_program_name("TAUCGL GUI Zoo")
    gui.mainWindow.show()

    gui.redraw_grid(GRID_SIZE)
    gui.zoom = 12
    gui.redraw()

    img = cv2.imread("img2dspgl/img.png")
    print(img.shape)

    # hues = [0, 60, 120, 180, 240, 300, 360]
    hues = [300]

    x0 = -30
    y0 = 38
    radius = 0.5
    size = 64

    img = cv2.resize(img, (size, size))

    appearance_rate = 2.0


    for j in range(size):
        for i in range(size):
            if img[j,i,0] < 100:
                if random.uniform(0, 1) > appearance_rate:
                    continue
            
                x = x0 + i * resolution
                y = y0 + (1-j) * resolution
                if random.uniform(0, 1) > 0.5:
                    color = QColor.fromHsv(
                        random.randint(0, 359), # hue
                        210, # saturation
                        240, # value
                        255)
                    fill = color
                else:
                    color = QColor.fromHsv(
                        random.randint(0, 359), # hue
                        210, # saturation
                        220, # value
                        255)
                    fill = QtCore.Qt.transparent
                gui.add_disc(radius, x, y, fill, color)
    

    gui.render_to_image('img2dspgl/res.png')
    sys.exit(0)

    # Uncomment to keep the app running
    sys.exit(app.exec_())
