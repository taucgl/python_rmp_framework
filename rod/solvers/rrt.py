import time
import math
import random

import networkx as nx
import sklearn.neighbors

import conversions
from bindings import *
from rod.solvers.collision_detection import Collision_detector

# the radius by which the rod will be expanded
epsilon = FT(0.1)
# eta for maximum RRT edge length
ETA = 2.0
# lambda of custom rod cspace distance
LAMBDA = 1.0
# epsilon for steering
STEER_EPS = 0.01

class NearestNeighbors(object):
    """
    A class that encapsulates nearest neighbor logic,
    by caching new neighbors and lazy expanding the tree
    """
    def __init__(self, max_cache_size=100):
        self.tree = None
        self.tree_points = None
        self.cache = []
        self.points = []
        self.num_points = 0
        self.max_cache_size = max_cache_size
    
    def add_point(self, point):
        self.points.append(point)
        self.cache.append(point)
        if len(self.cache) == self.max_cache_size:
            # If cache is full, clear it and build a larger tree
            self.tree = sklearn.neighbors.NearestNeighbors(n_neighbors=1, metric=custom_dist, algorithm='auto')
            self.tree_points = list(self.points) # Copy the tree's points
            self.tree.fit([point_d_to_arr(p) for p in self.tree_points])
            self.cache = []
    
    def get_nearest_neighbor(self, point):
        # Find cache neighbor
        nearest_cache = None
        min_dist = None
        for p in self.cache:
            dist = custom_dist(point_d_to_arr(p), point_d_to_arr(point))
            if min_dist is None or dist < min_dist:
                min_dist = dist
                nearest_cache = p

        # Find tree neighbor
        nearest_tree = None
        if self.tree is not None:
            nearest_tree_idx = self.tree.kneighbors([point_d_to_arr(point)], return_distance=False)[0][0]
            nearest_tree = self.tree_points[nearest_tree_idx]
        
        # Return whichever is closest (or the nontrivial one)
        if nearest_cache is None:
            return nearest_tree
        if nearest_tree is None:
            return nearest_cache
        if custom_dist(point_d_to_arr(point), point_d_to_arr(nearest_cache)) < custom_dist(point_d_to_arr(point), point_d_to_arr(nearest_tree)):
            return nearest_cache
        return nearest_tree


def sample_free_point(x_range, y_range, z_range, cd, length):
    """
    Sample a point in free space

    Gets xyz ranges and collision detector (and rod length), and samples random 
    points until we get a valid result
    """
    while True:
        rand_x = FT(random.uniform(x_range[0], x_range[1]))
        rand_y = FT(random.uniform(y_range[0], y_range[1]))
        rand_z = FT(random.uniform(z_range[0], z_range[1]))

        if cd.is_rod_position_valid(rand_x, rand_y, rand_z, length):
            return Point_d(3, [rand_x, rand_y, rand_z])


def steer(x_near, x_rand, eta):
    """
    Steer towards the given point (at at most eta)
    """
    if custom_dist(point_d_to_arr(x_rand), point_d_to_arr(x_near)) < eta:
        # If we are already close then just return
        return x_rand
    
    # Get the normalized direction times eta (to steer towards eta units from x_near)
    x_rand = point_d_to_arr(x_rand)
    x_near = point_d_to_arr(x_near)
    direction = (x_rand[0] - x_near[0], x_rand[1] - x_near[1], x_rand[2] - x_near[2])
    norm = custom_dist(direction, [0, 0, 0])
    direction = (direction[0] * eta / norm, direction[1] * eta / norm, direction[2] * eta / norm)

    # Generate the new point
    x_new = Point_d(3, [
        FT(x_near[0]+direction[0]), 
        FT(x_near[1]+direction[1]), 
        FT(x_near[2]+direction[2])])
    return x_new



def generate_path(length, obstacles, origin, destination, argument, writer, isRunning):
    t0 = time.perf_counter()
    path = []
    try:
        num_landmarks = int(argument)
    except Exception as e:
        print("argument is not an integer", file=writer)
        return path

    # Compute the bounding box for the scene
    polygons = [conversions.tuples_list_to_polygon_2(p) for p in obstacles]
    bbox = calc_bbox(polygons)
    x_range = (bbox[0].to_double(), bbox[1].to_double())
    y_range = (bbox[2].to_double(), bbox[3].to_double())
    z_range = (0, 2 * math.pi)

    # Convert start and endpoint to CGAL 3D point
    begin = Point_d(3, [FT(origin[0]), FT(origin[1]), FT(origin[2])])
    end = Point_d(3, [FT(destination[0]), FT(destination[1]), FT(destination[2])])
    
    # Initiate the graph
    G = nx.Graph()
    G.add_nodes_from([begin])
    nn = NearestNeighbors()
    nn.add_point(begin)

    # Initiate the collision detector
    cd = Collision_detector(polygons, [], epsilon)

    # Run the RRT loop:
    for i in range(num_landmarks):
        x_rand = sample_free_point(x_range, y_range, z_range, cd, length)
        x_near = nn.get_nearest_neighbor(x_rand)
        x_new = steer(x_near, x_rand, ETA)
        for clockwise in (True, False):
            if cd.is_rod_motion_valid(x_near, x_new, clockwise, length):
                G.add_edge(x_new, x_near, clockwise=clockwise)
                nn.add_point(x_new)
        
        if i % 100 == 0:
            print('Connected', i, 'landmarks to RRT', file=writer)

    # Also try to connect the end node to the graph
    G.add_node(end)
    x_rand = end 
    x_near = nn.get_nearest_neighbor(x_rand)
    x_new = x_rand
    for clockwise in (True, False):
        if cd.is_rod_motion_valid(x_near, x_new, clockwise, length):
            G.add_edge(x_new, x_near, clockwise=clockwise)
    
    if nx.has_path(G, begin, end):
        shortest_path = nx.shortest_path(G, begin, end)
        print("path found", file=writer)
        print("distance:", nx.shortest_path_length(G, begin, end, weight='weight'), file=writer)

        if len(shortest_path) == 0:
            return path
        first = shortest_path[0]
        path.append((first[0], first[1], first[2], True))
        for i in range(1, len(shortest_path)):
            last = shortest_path[i-1]
            next = shortest_path[i]
            # determine correct direction
            clockwise = G.get_edge_data(last, next)["clockwise"]
            path.append((next[0], next[1], next[2], clockwise))
    else:
        print("no path was found", file=writer)
    t1 = time.perf_counter()
    print("Time taken:", t1 - t0, "seconds", file=writer)
    return path


def custom_dist(p, q):
    sd = math.sqrt((p[0] - q[0])**2 + (p[1] - q[1])**2) +  LAMBDA * abs(p[2] - q[2])
    return sd


def calc_bbox(obstacles):
    """
    Calculate the scene's bounding box  
    """
    X = []
    Y = []
    for poly in obstacles:
        for point in poly.vertices():
            X.append(point.x())
            Y.append(point.y())
    min_x = min(X)
    max_x = max(X)
    min_y = min(Y)
    max_y = max(Y)

    return min_x, max_x, min_y, max_y


def point_d_to_arr(p: Point_d):
    """
    Convert CGALPY's Point_d object into an array of doubles  
    """
    return [p[i].to_double() for i in range(p.dimension())]