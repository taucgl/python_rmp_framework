from arr2_epec_seg_ex import *
import conversions
import math
import random
import collision_detection

# Dijkstra's algorithm for shortest paths
# David Eppstein, UC Irvine, 4 April 2002

# http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/117228
from priodict import priorityDictionary

def Dijkstra(graph,start,end=None):
  final_distances = {}	# dictionary of final distances
  predecessors = {}	# dictionary of predecessors
  estimated_distances = priorityDictionary()   # est.dist. of non-final vert.
  estimated_distances[start] = 0

  for vertex in estimated_distances:
    final_distances[vertex] = estimated_distances[vertex]
    if vertex == end: break

    for edge in graph[vertex]:
      path_distance = final_distances[vertex] + graph[vertex][edge]
      if edge in final_distances:
        if path_distance < final_distances[edge]:
          raise (ValueError, "Dijkstra: found better path to already-final vertex")
      elif edge not in estimated_distances or path_distance < estimated_distances[edge]:
        estimated_distances[edge] = path_distance
        predecessors[edge] = vertex

  return (final_distances,predecessors)

def shortestPath(graph,start,end):
  final_distances,predecessors = Dijkstra(graph,start,end)
  if end not in predecessors:
    return []
  path = []
  while 1:
      path.append(end)
      if end == start: break
      end = predecessors[end]
  path.reverse()
  return path


def calc_bbox(obstacles, origin, destination, length):
  X = []
  Y = []
  X.append(origin.x())
  X.append(destination.x())
  Y.append(origin.y())
  Y.append(destination.y())
  for poly in obstacles:
    for point in poly.vertices():
      X.append(point.x())
      Y.append(point.y())
  min_x = min(X)
  max_x = max(X)
  min_y = min(Y)
  max_y = max(Y)
  # min_x = min(X)-length
  # max_x = max(X)+length
  # min_y = min(Y)-length
  # max_y = max(Y)+length

  return (min_x, max_x, min_y, max_y)

def edge_valid(start, end, length, polygons, clockwise ,epsilon):
  x1 = start.x().exact()
  y1 = start.y().exact()
  a1 = start.z().exact()

  x2 = end.x().exact()
  y2 = end.y().exact()
  a2 = end.z().exact()

  if (not clockwise and a2 < a1): a1 = a1 - Gmpq(2 * math.pi)
  if (clockwise and a2 > a1): a1 = a1 + Gmpq(2 * math.pi)

  dx = x2 - x1
  dy = y2 - y1
  dz = abs((a2 - a1).to_double())

  sample_count = int(
    math.sqrt(dx.to_double() ** 2 + dy.to_double() ** 2) + dz * (length.to_double() + epsilon.to_double())) // (2*int(epsilon.to_double())) + 1

  for i in range(sample_count + 1):
    x = Gmpq(sample_count - i, sample_count) * x1 + Gmpq(i, sample_count) * x2
    y = Gmpq(sample_count - i, sample_count) * y1 + Gmpq(i, sample_count) * y2
    a = Gmpq(sample_count - i, sample_count) * a1 + Gmpq(i, sample_count) * a2

    if not collision_detection.is_position_valid(FT(x), FT(y), FT(a), length, polygons, epsilon):
      return False
  return True

def point_3_hash(p):
  return (p.x().to_double(), p.y().to_double(), p.z().to_double()).__hash__()

def generate_path(path, length, obstacles, origin, destination):
  Point_3.__hash__ = point_3_hash

  epsilon = FT(5)

  polygons = [conversions.tuples_list_to_polygon_2(p) for p in obstacles]
  s = Point_2(FT(Gmpq(origin[0])), FT(Gmpq(origin[1])))
  d = Point_2(FT(Gmpq(destination[0])), FT(Gmpq(destination[1])))
  bbox = calc_bbox(polygons, s, d, length)
  x_range = (bbox[0].to_double(), bbox[1].to_double())
  y_range = (bbox[2].to_double(), bbox[3].to_double())
  z_range = (0, math.pi)

  # ps = Polygon_set_2()
  # ps.join_polygons(polygons)
  # polygons = ps

  V = {}

  begin = Point_3(FT(Gmpq(origin[0])), FT(Gmpq(origin[1])), FT(Gmpq(origin[2])))
  V.update({begin:{}})

  #set up kd tree
  k = 15
  tree = Kd_tree([begin])
  eps = FT(Gmpq(0.0))  # 0.0 for exact NN, otherwise approximate NN
  search_nearest = True  # set this value to False in order to search farthest
  sort_neighbors = False  # set this value to True in order to obtain the neighbors sorted by distance

  i = 0
  while i < 1000:
    #sample new landmark
    rand_x = FT(random.uniform(x_range[0], x_range[1]))
    rand_y = FT(random.uniform(y_range[0], y_range[1]))
    rand_z = FT(random.uniform(z_range[0], z_range[1]))

    p = Point_3(rand_x, rand_y, rand_z)
    #search neighbors
    if collision_detection.is_position_valid(rand_x, rand_y, rand_z, length, polygons, epsilon):
      V.update({p: {}})
      # #search for neighbors
      # search = K_neighbor_search(tree, p, k, eps, search_nearest, Euclidean_distance(), sort_neighbors)
      # res = []
      # search.k_neighbors(res)
      # #for each neighbor
      # for pair in res:
      #   neighbor = pair[0]
      #   for d in (True, False):
      #     #check if we can add an edge to the graph
      #     if edge_valid(neighbor, p, length, polygons, d, FT(5)):
      #       d = 1
      #       V[p].update({neighbor:d})
      #       V[neighbor].update({p:d})
      #       break
      tree.insert(p)
      i += 1
      print(i)

  end = Point_3(FT(Gmpq(destination[0])), FT(Gmpq(destination[1])), FT(Gmpq(destination[2])))
  V.update({end: {}})
  tree.insert(end)

  # The following function returns the transformed distance between two points
  # (for Euclidean distance the transformed distance is the squared distance)
  def transformed_distance(p1, p2):
    q1 = Point_2(p1.x(), p1.y())
    q2 = Point_2(p2.x(), p2.y())
    sd = squared_distance(q1, q2)
    sd += length * (p1.z() - p2.z()) * (p1.z() - p2.z())
    assert(isinstance(sd, FT))
    return sd

  # The following function returns the transformed distance between the query
  # point q and the point on the boundary of the rectangle r closest to q.
  def min_distance_to_rectangle(q, r):
    return FT(Gmpq(1))  # replace this with your implementation

  # The following function returns the transformed distance between the query
  # point q and the point on the boundary of the rectangle r furthest to q.
  def max_distance_to_rectangle(q, r):
    return FT(Gmpq(1))  # replace this with your implementation

  # The following function returns the transformed distance for a value d
  # Fo example, if d is a value computed using the Euclidean distance, the transformed distance should be d*d
  def transformed_distance_for_value(d):
    return FT(Gmpq(1))  # replace this with your implementation

  # The following function returns the inverse of the transformed distance for a value d
  # Fo example, if d is a squared distance value then its inverse should be sqrt(d)
  def inverse_of_transformed_distance_for_value(d):
    return FT(Gmpq(1))  # replace this with your implementation

  #distance = Distance_python(transformed_distance, min_distance_to_rectangle, max_distance_to_rectangle, transformed_distance_for_value, inverse_of_transformed_distance_for_value)
  distance = Euclidean_distance()

  i = 0
  for p in V:
    #search = K_neighbor_search_python(tree, p, k, eps, search_nearest, distance, sort_neighbors)
    search = K_neighbor_search(tree, p, k, eps, search_nearest, distance, sort_neighbors)
    res = []
    search.k_neighbors(res)
    for pair in res:
      neighbor = pair[0]
      if neighbor not in V[p]:
        for d in (True, False):
          #check if we can add an edge to the graph
          if edge_valid(neighbor, p, length, polygons, d, epsilon):
            d = 1
            V[p].update({neighbor: d})
            V[neighbor].update({p: d})
            break
    print(i)
    i += 1

  # end = Point_3(FT(Gmpq(destination[0])), FT(Gmpq(destination[1])), FT(Gmpq(destination[2])))
  # V.update({end:{}})
  # search = K_neighbor_search(tree, end, k, eps, search_nearest, Euclidean_distance(), sort_neighbors)
  # res = []
  # search.k_neighbors(res)
  # # for each neighbor
  # for pair in res:
  #   neighbor = pair[0]
  #   for d in (True, False):
  #     # check if we can add an edge to the graph
  #     if edge_valid(neighbor, end, length, polygons, d, epsilon):
  #       d = 1
  #       V[end].update({neighbor: d})
  #       V[neighbor].update(({end: d}))
  #       break

  points = []

  for p in V:
      points.append(p)

  temp = shortestPath(V, begin, end)
  if len(temp) == 0: return points
  last = temp[0]
  path.append((last.x(), last.y(), last.z(), True))
  for i in range(1, len(temp)):
    next = temp[i]
    for d in (True, False):
      # check correct direction
      if edge_valid(last, next, length, polygons, d, epsilon):
        last = next
        path.append((last.x(), last.y(), last.z(), d))
        break
  print(path)
  return points





