from arr2_epec_seg_ex import *
import conversions
import math
import random
import better_cd
import my_distance

# Dijkstra's algorithm for shortest paths
# David Eppstein, UC Irvine, 4 April 2002

# http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/117228
from priodict import priorityDictionary

def Dijkstra(graph,start,end=None):
  final_distances = {}	# dictionary of final distances
  predecessors = {}	# dictionary of predecessors
  estimated_distances = priorityDictionary()   # est.dist. of non-final vert.
  estimated_distances[start] = 0

  for vertex in estimated_distances:
    final_distances[vertex] = estimated_distances[vertex]
    if vertex == end: break

    for edge in graph[vertex]:
      path_distance = final_distances[vertex] + graph[vertex][edge]
      if edge in final_distances:
        if path_distance < final_distances[edge]:
          raise (ValueError, "Dijkstra: found better path to already-final vertex")
      elif edge not in estimated_distances or path_distance < estimated_distances[edge]:
        estimated_distances[edge] = path_distance
        predecessors[edge] = vertex

  return (final_distances,predecessors)

def shortestPath(graph,start,end):
  final_distances,predecessors = Dijkstra(graph,start,end)
  if end not in predecessors:
    return []
  path = []
  while 1:
      path.append(end)
      if end == start: break
      end = predecessors[end]
  path.reverse()
  return path


def calc_bbox(obstacles, origin, destination, length):
  X = []
  Y = []
  X.append(origin.x())
  X.append(destination.x())
  Y.append(origin.y())
  Y.append(destination.y())
  for poly in obstacles:
    for point in poly.vertices():
      X.append(point.x())
      Y.append(point.y())
  min_x = min(X)
  max_x = max(X)
  min_y = min(Y)
  max_y = max(Y)
  # min_x = min(X)-length
  # max_x = max(X)+length
  # min_y = min(Y)-length
  # max_y = max(Y)+length

  return (min_x, max_x, min_y, max_y)

def edge_valid(start, end, length, polygons, clockwise ,epsilon, cd):
  x1 = start.x()
  y1 = start.y()
  a1 = start.z()

  x2 = end.x()
  y2 = end.y()
  a2 = end.z()

  if (not clockwise and a2 < a1): a1 = a1 - FT(2 * math.pi)
  if (clockwise and a2 > a1): a1 = a1 + FT(2 * math.pi)

  dx = x2 - x1
  dy = y2 - y1
  dz = abs((a2 - a1).to_double())

  sample_count = int(
    math.sqrt(dx.to_double() ** 2 + dy.to_double() ** 2) + dz * (length.to_double() + epsilon.to_double())) // (2*int(epsilon.to_double())) + 1

  for i in range(sample_count + 1):
    x = FT((sample_count - i)/sample_count) * x1 + FT(i/sample_count) * x2
    y = FT((sample_count - i)/sample_count) * y1 + FT(i/sample_count) * y2
    a = FT((sample_count - i)/sample_count) * a1 + FT(i/sample_count) * a2
    if not cd.is_position_valid(x, y, a):
      return False
  return True

def point_3_hash(p):
  return (p.x().to_double(), p.y().to_double(), p.z().to_double()).__hash__()

def generate_path(path, length, obstacles, origin, destination):
  Point_3.__hash__ = point_3_hash

  epsilon = FT(5)

  polygons = [conversions.tuples_list_to_polygon_2(p) for p in obstacles]
  s = Point_2(FT(Gmpq(origin[0])), FT(Gmpq(origin[1])))
  d = Point_2(FT(Gmpq(destination[0])), FT(Gmpq(destination[1])))
  bbox = calc_bbox(polygons, s, d, length)
  x_range = (bbox[0].to_double(), bbox[1].to_double())
  y_range = (bbox[2].to_double(), bbox[3].to_double())
  z_range = (0, math.pi)

  ps = Polygon_set_2()
  ps.join_polygons(polygons)
  polygons = ps

  cd = better_cd.cd(length, epsilon, polygons)

  V = {}

  begin = Point_3(FT(Gmpq(origin[0])), FT(Gmpq(origin[1])), FT(Gmpq(origin[2])))
  V.update({begin:{}})

  #set up kd tree
  tree = Kd_tree([begin])
  eps = FT(Gmpq(0.0))  # 0.0 for exact NN, otherwise approximate NN
  search_nearest = True  # set this value to False in order to search farthest
  sort_neighbors = False  # set this value to True in order to obtain the neighbors sorted by distance

  end = Point_3(FT(Gmpq(destination[0])), FT(Gmpq(destination[1])), FT(Gmpq(destination[2])))
  V.update({end: {}})
  tree.insert(end)

  my_distance.set_length(length)
  distance = my_distance.my_distance()
  k = 15

  j = 0
  while True:
    print(j)
    j += 100
    i = 0
    new_points = [begin, end]
    while i < 100:
      #sample new landmark
      rand_x = FT(random.uniform(x_range[0], x_range[1]))
      rand_y = FT(random.uniform(y_range[0], y_range[1]))
      rand_z = FT(random.uniform(z_range[0], z_range[1]))

      p = Point_3(rand_x, rand_y, rand_z)
      if cd.is_position_valid(rand_x, rand_y, rand_z):
        V.update({p: {}})
        tree.insert(p)
        new_points.append(p)
        i += 1



    i = 0
    # new_points = []
    # tree.points(new_points)
    for p in new_points:
      search = K_neighbor_search_python(tree, p, k, eps, search_nearest, distance, sort_neighbors)
      #search = K_neighbor_search(tree, p, k, eps, search_nearest, Euclidean_distance(), sort_neighbors)
      res = []
      search.k_neighbors(res)
      for pair in res:
        neighbor = pair[0]
        if neighbor not in V[p]:
          for d in (True, False):
            #check if we can add an edge to the graph
            if edge_valid(neighbor, p, length, polygons, d, epsilon, cd):
              p_2 = Point_2(p.x(), p.y())
              neighbor_2 = Point_2(neighbor.x(), neighbor.y())
              d = squared_distance(p_2, neighbor_2).to_double()
              d = 1
              V[p].update({neighbor: d})
              V[neighbor].update({p: d})
              break
      print(i)
      i += 1

    # points = []
    #
    # for p in V:
    #     points.append(p)

    temp = shortestPath(V, begin, end)
    if len(temp) == 0: continue #add 100 more points
    last = temp[0]
    path.append((last.x(), last.y(), last.z(), True))
    for i in range(1, len(temp)):
      next = temp[i]
      for d in (True, False):
        # check correct direction
        if edge_valid(last, next, length, polygons, d, epsilon, cd):
          last = next
          path.append((last.x(), last.y(), last.z(), d))
          break
    print(path)
    print(j)
    return





