from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import pyqtSignal
from gui.gui import GUI, MainWindowPlus

POINT_RADIUS = 0.1
GRID_SIZE = 200

MODE_POLYGON_OBSTACLE = 0
MODE_BONUSES = 1
MODE_RED_ROBOTS_START = 2
MODE_BLUE_ROBOTS_START = 3
MODE_GOALS = 4

BONUS_RADIUS = 1.0

MODE_NAMES = {
    MODE_POLYGON_OBSTACLE: 'Polygonal Obstacles',
    MODE_BONUSES: 'Bonuses',
    MODE_RED_ROBOTS_START: 'Red Robots Start',
    MODE_BLUE_ROBOTS_START: 'Blue Robots Start',
    MODE_GOALS: 'Goals'
}

MODE_CURRENT_STR = '(Current Mode: {})'

class MainWindowSceneDesigner(MainWindowPlus):
    signal_ctrl_z = pyqtSignal()
    signal_delete = pyqtSignal()
    signal_esc = pyqtSignal()
    signal_drop = pyqtSignal(str)

    def __init__(self, gui):
        super().__init__(gui)

    # Adjust zoom level/scale on +/- key press
    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Plus:
            self.gui.zoom /= 0.9
        if event.key() == QtCore.Qt.Key_Minus:
            self.gui.zoom *= 0.9
        if event.modifiers() & QtCore.Qt.ControlModifier and event.key() == QtCore.Qt.Key_Z:
            self.signal_ctrl_z.emit()
        if event.key() == QtCore.Qt.Key_Escape:
            self.signal_esc.emit()
        if event.key() == QtCore.Qt.Key_Delete:
            self.signal_delete.emit()
        self.gui.redraw()


class GUI_scene_designer_game(GUI):
    # Variables that are added to the given class
    grid = []
    resolution = 1.0

    polygon_obstacles = []
    gui_polygon_obstacles = []

    bonuses = []
    gui_bonuses = []

    red_robots_starts = []
    gui_red_robots_starts = []

    blue_robots_starts = []
    gui_blue_robots_starts = []

    goals = []
    gui_goals = []

    radius = 1.0
    number_of_turns = 1
    turn_time = 1.0
    preprocess_time = 1.0
    makespan = 1.0

    drawing_mode = MODE_POLYGON_OBSTACLE
    polyline = []
    gui_current_polygon_vertices = []
    gui_current_polygon_edges = []

    selected = {
        'index': None,
        'type': None
    }

    def __init__(self):
        super().__init__()
        self.zoom = 50.0
        self.redraw()

    def setupUi(self):
        self.mainWindow = MainWindowSceneDesigner(self)
        MainWindow = self.mainWindow

        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1600, 904)
        MainWindow.setStyleSheet("QMainWindow { background-color : rgb(54, 57, 63); color : rgb(220, 221, 222); }\n"
"#centralwidget { background-color : rgb(54, 57, 63); color : rgb(220, 221, 222); }\n"
"QLabel { background-color : rgb(54, 57, 63); color : rgb(220, 221, 222); }")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.graphicsView = QtWidgets.QGraphicsView(self.centralwidget)
        self.graphicsView.setEnabled(True)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.graphicsView.sizePolicy().hasHeightForWidth())
        self.graphicsView.setSizePolicy(sizePolicy)
        self.graphicsView.setObjectName("graphicsView")
        self.gridLayout.addWidget(self.graphicsView, 3, 4, 1, 1)
        self.gridLayout_8 = QtWidgets.QGridLayout()
        self.gridLayout_8.setObjectName("gridLayout_8")
        self.radiusButton = QtWidgets.QPushButton(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.radiusButton.setFont(font)
        self.radiusButton.setObjectName("radiusButton")
        self.gridLayout_8.addWidget(self.radiusButton, 2, 0, 1, 1)
        self.turnTimeEdit = QtWidgets.QLineEdit(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.turnTimeEdit.setFont(font)
        self.turnTimeEdit.setObjectName("turnTimeEdit")
        self.gridLayout_8.addWidget(self.turnTimeEdit, 6, 0, 1, 1)
        self.radiusEdit = QtWidgets.QLineEdit(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.radiusEdit.setFont(font)
        self.radiusEdit.setObjectName("radiusEdit")
        self.gridLayout_8.addWidget(self.radiusEdit, 1, 0, 1, 1)
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setObjectName("label_7")
        self.gridLayout_8.addWidget(self.label_7, 0, 0, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(300, 20, QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_8.addItem(spacerItem, 16, 0, 1, 1)
        self.preprocessTimeEdit = QtWidgets.QLineEdit(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.preprocessTimeEdit.setFont(font)
        self.preprocessTimeEdit.setObjectName("preprocessTimeEdit")
        self.gridLayout_8.addWidget(self.preprocessTimeEdit, 8, 0, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_8.addItem(spacerItem1, 13, 0, 1, 1)
        self.makespanEdit = QtWidgets.QLineEdit(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.makespanEdit.setFont(font)
        self.makespanEdit.setText("")
        self.makespanEdit.setObjectName("makespanEdit")
        self.gridLayout_8.addWidget(self.makespanEdit, 10, 0, 1, 1)
        self.totalTurnsEdit = QtWidgets.QLineEdit(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.totalTurnsEdit.setFont(font)
        self.totalTurnsEdit.setObjectName("totalTurnsEdit")
        self.gridLayout_8.addWidget(self.totalTurnsEdit, 4, 0, 1, 1)
        self.label_9 = QtWidgets.QLabel(self.centralwidget)
        self.label_9.setObjectName("label_9")
        self.gridLayout_8.addWidget(self.label_9, 5, 0, 1, 1)
        self.label_11 = QtWidgets.QLabel(self.centralwidget)
        self.label_11.setObjectName("label_11")
        self.gridLayout_8.addWidget(self.label_11, 9, 0, 1, 1)
        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setObjectName("label_8")
        self.gridLayout_8.addWidget(self.label_8, 3, 0, 1, 1)
        self.label_10 = QtWidgets.QLabel(self.centralwidget)
        self.label_10.setObjectName("label_10")
        self.gridLayout_8.addWidget(self.label_10, 7, 0, 1, 1)
        self.label_12 = QtWidgets.QLabel(self.centralwidget)
        self.label_12.setObjectName("label_12")
        self.gridLayout_8.addWidget(self.label_12, 11, 0, 1, 1)
        self.goalRadiusEdit = QtWidgets.QLineEdit(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.goalRadiusEdit.setFont(font)
        self.goalRadiusEdit.setText("")
        self.goalRadiusEdit.setObjectName("goalRadiusEdit")
        self.gridLayout_8.addWidget(self.goalRadiusEdit, 12, 0, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_8, 3, 3, 1, 1)
        self.gridLayout_0 = QtWidgets.QGridLayout()
        self.gridLayout_0.setObjectName("gridLayout_0")
        self.helpButton = QtWidgets.QPushButton(self.centralwidget)
        self.helpButton.setObjectName("helpButton")
        self.gridLayout_0.addWidget(self.helpButton, 0, 0, 1, 1)
        self.modeGoalsButton = QtWidgets.QPushButton(self.centralwidget)
        self.modeGoalsButton.setObjectName("modeGoalsButton")
        self.gridLayout_0.addWidget(self.modeGoalsButton, 46, 0, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout_0.addItem(spacerItem2, 49, 0, 1, 1)
        self.saveButton = QtWidgets.QPushButton(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.saveButton.setFont(font)
        self.saveButton.setObjectName("saveButton")
        self.gridLayout_0.addWidget(self.saveButton, 9, 0, 1, 1)
        self.resolutionButton = QtWidgets.QPushButton(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.resolutionButton.setFont(font)
        self.resolutionButton.setObjectName("resolutionButton")
        self.gridLayout_0.addWidget(self.resolutionButton, 16, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.gridLayout_0.addWidget(self.label, 39, 0, 1, 1)
        spacerItem3 = QtWidgets.QSpacerItem(300, 20, QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout_0.addItem(spacerItem3, 52, 0, 1, 1)
        self.writerEdit = QtWidgets.QTextEdit(self.centralwidget)
        self.writerEdit.setObjectName("writerEdit")
        self.gridLayout_0.addWidget(self.writerEdit, 10, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.gridLayout_0.addWidget(self.label_2, 11, 0, 1, 1)
        self.polygonEdit = QtWidgets.QLineEdit(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.polygonEdit.setFont(font)
        self.polygonEdit.setObjectName("polygonEdit")
        self.gridLayout_0.addWidget(self.polygonEdit, 12, 0, 1, 1)
        self.modePolyObstButton = QtWidgets.QPushButton(self.centralwidget)
        self.modePolyObstButton.setObjectName("modePolyObstButton")
        self.gridLayout_0.addWidget(self.modePolyObstButton, 40, 0, 1, 1)
        self.modeBonusesButton = QtWidgets.QPushButton(self.centralwidget)
        self.modeBonusesButton.setObjectName("modeBonusesButton")
        self.gridLayout_0.addWidget(self.modeBonusesButton, 41, 0, 1, 1)
        self.clearButton = QtWidgets.QPushButton(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.clearButton.setFont(font)
        self.clearButton.setObjectName("clearButton")
        self.gridLayout_0.addWidget(self.clearButton, 1, 0, 1, 1)
        self.label_1 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_1.setFont(font)
        self.label_1.setObjectName("label_1")
        self.gridLayout_0.addWidget(self.label_1, 5, 0, 1, 1)
        self.label_0 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_0.setFont(font)
        self.label_0.setObjectName("label_0")
        self.gridLayout_0.addWidget(self.label_0, 2, 0, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.gridLayout_0.addWidget(self.label_3, 14, 0, 1, 1)
        self.resolutionEdit = QtWidgets.QLineEdit(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.resolutionEdit.setFont(font)
        self.resolutionEdit.setObjectName("resolutionEdit")
        self.gridLayout_0.addWidget(self.resolutionEdit, 15, 0, 1, 1)
        self.fileBrowseButton = QtWidgets.QToolButton(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.fileBrowseButton.setFont(font)
        self.fileBrowseButton.setObjectName("fileBrowseButton")
        self.gridLayout_0.addWidget(self.fileBrowseButton, 3, 1, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.gridLayout_0.addWidget(self.label_4, 38, 0, 1, 1)
        self.inputEdit = QtWidgets.QLineEdit(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.inputEdit.setFont(font)
        self.inputEdit.setObjectName("inputEdit")
        self.gridLayout_0.addWidget(self.inputEdit, 3, 0, 1, 1)
        self.loadButton = QtWidgets.QPushButton(self.centralwidget)
        self.loadButton.setObjectName("loadButton")
        self.gridLayout_0.addWidget(self.loadButton, 4, 0, 1, 1)
        self.modeRedRobotsStartButton = QtWidgets.QPushButton(self.centralwidget)
        self.modeRedRobotsStartButton.setObjectName("modeRedRobotsStartButton")
        self.gridLayout_0.addWidget(self.modeRedRobotsStartButton, 44, 0, 1, 1)
        self.outputEdit = QtWidgets.QLineEdit(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.outputEdit.setFont(font)
        self.outputEdit.setObjectName("outputEdit")
        self.gridLayout_0.addWidget(self.outputEdit, 6, 0, 1, 1)
        self.modeBlueRobotsStartButton = QtWidgets.QPushButton(self.centralwidget)
        self.modeBlueRobotsStartButton.setObjectName("modeBlueRobotsStartButton")
        self.gridLayout_0.addWidget(self.modeBlueRobotsStartButton, 45, 0, 1, 1)
        self.polygonButton = QtWidgets.QPushButton(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.polygonButton.setFont(font)
        self.polygonButton.setObjectName("polygonButton")
        self.gridLayout_0.addWidget(self.polygonButton, 13, 0, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_0, 3, 2, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.mapElements()

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.radiusButton.setText(_translate("MainWindow", "Set Radius"))
        self.label_7.setText(_translate("MainWindow", "Robot radius:"))
        self.label_9.setText(_translate("MainWindow", "Turn time:"))
        self.label_11.setText(_translate("MainWindow", "Turn distance:"))
        self.label_8.setText(_translate("MainWindow", "Total time:"))
        self.label_10.setText(_translate("MainWindow", "Preprocess time:"))
        self.label_12.setText(_translate("MainWindow", "CurrentGoalRadius:"))
        self.helpButton.setText(_translate("MainWindow", "Help"))
        self.modeGoalsButton.setText(_translate("MainWindow", "Goals"))
        self.saveButton.setText(_translate("MainWindow", "Save Scene"))
        self.resolutionButton.setText(_translate("MainWindow", "Set Resolution"))
        self.label.setText(_translate("MainWindow", "(Current Mode: Polygonal Obstacles)"))
        self.label_2.setText(_translate("MainWindow", "Polygon Index:"))
        self.modePolyObstButton.setText(_translate("MainWindow", "Polygonal Obstacles"))
        self.modeBonusesButton.setText(_translate("MainWindow", "Bonuses"))
        self.clearButton.setText(_translate("MainWindow", "Clear Scene"))
        self.label_1.setText(_translate("MainWindow", "Output Path:"))
        self.label_0.setText(_translate("MainWindow", "Input Path:"))
        self.label_3.setText(_translate("MainWindow", "Resolution:"))
        self.fileBrowseButton.setText(_translate("MainWindow", "..."))
        self.label_4.setText(_translate("MainWindow", "Drawing Mode:"))
        self.loadButton.setText(_translate("MainWindow", "Load Scene"))
        self.modeRedRobotsStartButton.setText(_translate("MainWindow", "Red Robots"))
        self.modeBlueRobotsStartButton.setText(_translate("MainWindow", "Blue Robots"))
        self.polygonButton.setText(_translate("MainWindow", "Select Polygon"))

    def mapElements(self):
        self.lineEdits['input_path'] = self.inputEdit
        self.lineEdits['output_path'] = self.outputEdit
        self.lineEdits['polygon_index'] = self.polygonEdit
        self.lineEdits['resolution'] = self.resolutionEdit
        self.lineEdits['robot_radius'] = self.radiusEdit
        self.lineEdits['goal_radius'] = self.goalRadiusEdit
        self.lineEdits['number_of_turns'] = self.totalTurnsEdit
        self.lineEdits['turn_time'] = self.turnTimeEdit
        self.lineEdits['preprocess_time'] = self.preprocessTimeEdit
        self.lineEdits['makespan'] = self.makespanEdit
        
        self.pushButtons['help'] = self.helpButton
        self.pushButtons['clear'] = self.clearButton
        self.pushButtons['load'] = self.loadButton
        self.pushButtons['save'] = self.saveButton
        self.pushButtons['select_polygon'] = self.polygonButton
        self.pushButtons['set_resolution'] = self.resolutionButton
        self.pushButtons['set_radius'] = self.radiusButton
        self.pushButtons['file_browser'] = self.fileBrowseButton

        # Drawing modes
        self.pushButtons['mode_polygonal_obstacles'] = self.modePolyObstButton
        self.pushButtons['mode_bonuses'] = self.modeBonusesButton
        self.pushButtons['mode_red_robots_start'] = self.modeRedRobotsStartButton
        self.pushButtons['mode_blue_robots_start'] = self.modeBlueRobotsStartButton
        self.pushButtons['mode_goals'] = self.modeGoalsButton

        self.labels['current_mode'] = self.label