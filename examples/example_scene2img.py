"""
This is a minimal example that demonstrates usage of all different Qt GUI objects
that are available on this framework.
"""

import sys

from PyQt5 import QtWidgets, QtCore
from PyQt5.QtGui import QImage, QPainter

from gui.gui import GUI

class GUITest(GUI):
    def setupUi(self):
        #########################
        # Setup the Qt UI layout
        #########################
        # Resize the main window and set the stylesheet (CSS)
        MainWindow = self.mainWindow
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        MainWindow.setStyleSheet("QMainWindow { background-color : rgb(54, 57, 63); color : rgb(220, 221, 222); }\n"
                                 "QLabel { background-color : rgb(54, 57, 63); color : rgb(220, 221, 222); }")

        #################
        # Important!: the area of the scene
        #################
        self.area = QtCore.QRect(10, 10, 780, 580)

        # Add a graphics widget to the main window
        self.graphicsView = QtWidgets.QGraphicsView(self.mainWindow)
        self.graphicsView.setEnabled(True)
        self.graphicsView.setGeometry(self.area)
        self.graphicsView.setObjectName("graphicsView")

        # Zoom out a bit
        self.zoom = 40

        #######################################
        # This is how you add stuff to the gui
        #######################################
        self.add_text("RText", 0.0, 4.0, 1)
        self.add_text("This is some text", -2.0, 3.0, 1)

        self.add_text("RPolygon", 4.0, 4.0, 1)
        points = [(0.0, 0.0), (0.0, 2.0), (1.0, 1.0), (2.0, 2.0), (2.0, 0.0)]
        self.add_polygon(map(lambda p: (p[0]+4.0, p[1]+1.8), points), QtCore.Qt.transparent)

        self.add_text("RDisc", 8.0, 4.0, 1)
        self.add_disc(1.0, 8.0, 3.0)

        self.add_text("RDiscRobot", 11.0, 4.0, 1)
        self.add_disc_robot(1.0, 11.0, 3.0, "123", QtCore.Qt.red)

        self.add_text("RSegment", 0.0, 0.0, 1)
        self.add_segment(-1.0, -1.5, 0.83, 0.35 - 0.5)

        self.add_text("RSegment_angle", 4.0, 0.0, 1)
        self.add_segment_angle(3.7, -2.0, 1.5, 67.0 / 180.0 * 3.14152865)

        self.add_text("RCircleSegment", 9.0, 0.0, 1)
        self.add_circle_segment(1.0, 9.5, -1.5, 0.0, 70.0 / 180.0 * 3.14159265, False)

    def render_to_image(self, file_name):
        image = QImage(self.area.size(), QImage.Format_ARGB32_Premultiplied)
        painter = QPainter(image)
        self.graphicsView.render(painter, QtCore.QRectF(image.rect()), self.area)
        painter.end()
        image.save(file_name)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    gui = GUITest()
    gui.set_program_name("TAUCGL GUI Zoo")
    gui.mainWindow.show()

    # Rendering can be done only after the window is shown!
    gui.render_to_image("test1.png")
    gui.add_polygon([[-10, -10], [0, 7], [10, -10]], QtCore.Qt.magenta)
    gui.render_to_image("test2.png")
    sys.exit(0)

    # Uncomment to keep the app running
    # sys.exit(app.exec_())
