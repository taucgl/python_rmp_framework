from PyQt5 import QtWidgets
from PyQt5.QtCore import QObject, pyqtSignal, QTimer
from PyQt5.QtWidgets import QFileDialog
import importlib
from importlib import util
from gui_interface import GUI_interface
import sys


def action():
    path_name = gui.get_field('script')
    params = gui.get_field('input0'), gui.get_field('input1')
    spec = importlib.util.spec_from_file_location(path_name, path_name)
    gp = util.module_from_spec(spec)
    spec.loader.exec_module(gp)
    gp.action(gui, params)


def clear():
    gui.clear_scene()


def getfile():
    dlg = QFileDialog()
    dlg.setFileMode(QFileDialog.AnyFile)
    if dlg.exec_():
        filenames = dlg.selectedFiles()
        return filenames[0]


def set_script():
    s = getfile()
    if s:
        gui.set_field('script', s)


def set_input():
    s = getfile()
    if s:
        gui.set_field('input', s)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    gui = GUI_interface()
    gui.set_program_name("interface")
    gui.set_logic('script', action)
    gui.set_logic('clear', clear)
    gui.set_logic('searchScript', set_script)
    gui.set_logic('searchInput', set_input)
    gui.set_button_text('script', "Start")
    gui.set_button_text('clear', "Clear")
    gui.set_button_text('searchScript', "...")
    gui.set_button_text('searchInput', "...")
    i = 1
    for field in gui.lineEdits:
        if i >= len(sys.argv):
            break
        gui.set_field(field, sys.argv[i])
        i += 1
    gui.set_animation_finished_action(lambda: None)
    gui.mainWindow.show()
    sys.exit(app.exec_())
