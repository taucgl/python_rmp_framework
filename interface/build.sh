#!/bin/sh
project=interface
rm -r ./build
rm -r ./dist
pyinstaller $project.py
cp -r ../gui dist/$project/gui
cp -r examples dist/$project/
rm $project.spec
curl https://www.cs.tau.ac.il/~cgl/python_rmp/libgmp-10.dll -o dist/$project/libgmp-10.dll
curl https://www.cs.tau.ac.il/~cgl/python_rmp/libmpfr-4.dll -o dist/$project/libmpfr-4.dll
CGALPY=CGALPY.pyd
curl https://www.cs.tau.ac.il/~cgl/python_rmp/$CGALPY -o dist/$project/$CGALPY
rm -r ./build
