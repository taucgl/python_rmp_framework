import CGALPY
from PyQt5 import QtCore

Ker = CGALPY.Ker
Aos2 = CGALPY.Aos2

Point_2 = Ker.Point_2
Segment_2 = Ker.Segment_2
Curve_2 = Aos2.Traits.Curve_2
X_monotone_curve_2 = Aos2.Traits.X_monotone_curve_2

def to_ker_point_2(point: Point_2):
    x, y = point.x(), point.y()
    assert (not x.is_extended())
    assert (not y.is_extended())
    return Ker.Point_2(x.a0(), y.a0())

def draw_bfs(queue: list, gui):
    while(queue):
        face = queue.pop(0)
        if face.data() == "visited":
            continue
        if not face.is_unbounded():
            lst = []
            for edge in face.outer_ccb():
                lst.append(edge.source().point())
            points = [(point.x().to_double(), point.y().to_double()) for point in lst]
            if face.data():
                gui.add_polygon(points, fill_color=QtCore.Qt.black, line_color=QtCore.Qt.gray)
            else:
                gui.add_polygon(points, fill_color=QtCore.Qt.white, line_color=QtCore.Qt.gray)
        face.set_data("visited")

        for edge in face.outer_ccb():
            outer = edge.twin().face()
            queue.append(outer)
        for ccb in face.inner_ccbs():
            for edge in ccb:
                inner = edge.twin().face()
                queue.append(inner)

def draw_arrangement(arr: Aos2.Arrangement_2, gui):
    root = arr.unbounded_face() 
    lst = [root]
    draw_bfs(lst, gui)

def action(gui, params):
    overlay_traits = Aos2.Arr_face_overlay_traits(lambda x, y: x or y)

    bbox_arr = Aos2.Arrangement_2()
    points = [Point_2(0, 0), Point_2(10, 0), Point_2(10, 10), Point_2(0, 10)]
    bbox_curves = [Curve_2(Segment_2(points[i], points[(i+1)%len(points)])) for i in range(len(points))]
    Aos2.insert(bbox_arr, bbox_curves)

    # mark bbox face as valid, unbounded face as invalid
    for face in bbox_arr.faces():
        face.set_data(face.is_unbounded())


    obstacles_arr = Aos2.Arrangement_2()
    points = [Point_2(3, 3), Point_2(6, 3), Point_2(4, 6)]
    obstacle_curves = [Curve_2(Segment_2(points[i], points[(i+1)%len(points)])) for i in range(len(points))]
    Aos2.insert(obstacles_arr, obstacle_curves)

    # mark all faces as invalid except the unbounded one
    for face in obstacles_arr.faces():
        face.set_data(not face.is_unbounded())

    arr = Aos2.Arrangement_2()
    Aos2.overlay(obstacles_arr, bbox_arr, arr, overlay_traits)

    res = []
    Aos2.decompose(arr, res)
    vertical_walls = []
    for pair in res:
        v = pair[0]
        objects = pair[1]
        for object in objects:
            v_point = to_ker_point_2(v.point())
            if type(object) == Aos2.Arrangement_2.Vertex:
                v_other = to_ker_point_2(object.point())
                wall = Curve_2(Segment_2(v_point, v_other))
                vertical_walls.append(wall)
            if type(object) == Aos2.Arrangement_2.Halfedge:
                line = Ker.Line_2(to_ker_point_2(object.source().point()), to_ker_point_2(object.target().point()))
                y_at_x = line.y_at_x(v_point.x())
                wall = Curve_2(Segment_2(v_point, Ker.Point_2(v_point.x(), y_at_x)))
                vertical_walls.append(wall)

    walls_arr = Aos2.Arrangement_2()
    Aos2.insert(walls_arr, vertical_walls)
    for face in walls_arr.faces():
        face.set_data(False)

    res = Aos2.Arrangement_2()
    Aos2.overlay(arr, walls_arr, res, overlay_traits)
    draw_arrangement(res, gui)

