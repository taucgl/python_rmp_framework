project=interface
rm -r package
mkdir package
mkdir package/$project
cp -r ../gui package/$project/gui
cp $project.py package/$project/$project.py
cp gui_interface.py package/$project/gui_interface.py
cp -r examples package/$project/examples

curl https://www.cs.tau.ac.il/~cgl/python_rmp/libgmp-10.dll -o package/$project/libgmp-10.dll
curl https://www.cs.tau.ac.il/~cgl/python_rmp/libmpfr-4.dll -o package/$project/libmpfr-4.dll
CGALPY=CGALPY.pyd
curl https://www.cs.tau.ac.il/~cgl/python_rmp/$CGALPY -o package/$project/$CGALPY
