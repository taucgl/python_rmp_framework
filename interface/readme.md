# Interface
Provides a minimal interface for passing inputs to an external script and drawing 2D graphics from that script.

Pressing on the start button will run the ``action`` function in the
external script provided in the first field, onto which will be passed in
this order:
* ``gui`` - The programs GUI object
* ``params`` - A pair of the first and second arguments provided in the
second and third fields

## Drawing to the GUI from the External Script
The external script can draw 2D objects on the program's graphics scene.

#### To add a polygon:
```
polygon = gui.add_polygon(points, fill_color, line_color)
```
With ``points`` being a list of pairs of floats, and ``fill_color``
and ``line_color`` being optional parameters that can take values such as ``QtCore.Qt.black`` .

#### To add a disc:
```
disc = gui.add_disc(r, x, y, fill_color, line_color)
```
With ``r`` being the disc's radius, ``x``, ``y`` the x y coordinates of the disc's center.

#### To add a segment:
```
segment = gui.add_segment(x1, y1, x2, y2, line_color)
```
With ``x1``, ``y1`` being one end's coordinates and ``x2``, ``y2`` the other end's coordinates

### Animations
Animations are also supported. See ``example_animation.py`` for an example.
