# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'toolkit.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(796, 222)
        MainWindow.setStyleSheet("QMainWindow { background-color : rgb(54, 57, 63); color : rgb(220, 221, 222); }\n"
"#centralwidget { background-color : rgb(54, 57, 63); color : rgb(220, 221, 222); }\n"
"QLabel { background-color : rgb(54, 57, 63); color : rgb(220, 221, 222); }\n"
"QGroupBox {\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    padding: 10px;\n"
"}\n"
"QPushButton {\n"
"    background-color: rgb(0, 0, 0, 10);\n"
"}\n"
"QPushButton:hover {\n"
"    background-color: rgb(0, 0, 0, 50);\n"
"}")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.label = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(32)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.rewindButton = QtWidgets.QPushButton(self.centralwidget)
        self.rewindButton.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("../img/replay-video.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.rewindButton.setIcon(icon)
        self.rewindButton.setIconSize(QtCore.QSize(80, 80))
        self.rewindButton.setObjectName("rewindButton")
        self.gridLayout.addWidget(self.rewindButton, 0, 1, 1, 1)
        self.playButton = QtWidgets.QPushButton(self.centralwidget)
        self.playButton.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("../img/video-game-gamepad.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.playButton.setIcon(icon1)
        self.playButton.setIconSize(QtCore.QSize(80, 80))
        self.playButton.setObjectName("playButton")
        self.gridLayout.addWidget(self.playButton, 0, 0, 1, 1)
        self.designButton = QtWidgets.QPushButton(self.centralwidget)
        self.designButton.setAutoFillBackground(False)
        self.designButton.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("../img/paint.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.designButton.setIcon(icon2)
        self.designButton.setIconSize(QtCore.QSize(80, 80))
        self.designButton.setObjectName("designButton")
        self.gridLayout.addWidget(self.designButton, 0, 3, 1, 1)
        self.configureButton = QtWidgets.QPushButton(self.centralwidget)
        self.configureButton.setText("")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap("../img/configuration.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.configureButton.setIcon(icon3)
        self.configureButton.setIconSize(QtCore.QSize(80, 80))
        self.configureButton.setObjectName("configureButton")
        self.gridLayout.addWidget(self.configureButton, 0, 2, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 1, 1, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setAlignment(QtCore.Qt.AlignCenter)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 1, 2, 1, 1)
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setAlignment(QtCore.Qt.AlignCenter)
        self.label_5.setObjectName("label_5")
        self.gridLayout.addWidget(self.label_5, 1, 3, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "CoMotion Toolkit"))
        self.label_2.setText(_translate("MainWindow", "Play"))
        self.label_3.setText(_translate("MainWindow", "Rewind"))
        self.label_4.setText(_translate("MainWindow", "Configure"))
        self.label_5.setText(_translate("MainWindow", "Design"))
