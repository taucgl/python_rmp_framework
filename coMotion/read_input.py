import json
from bindings import *

def coordinate_to_FT(coord):
    if isinstance(coord, float) or isinstance(coord, int):
        return FT(Gmpq(coord))
    if "." in coord:
        return FT(Gmpq(float(coord)))
    else:
        return FT(Gmpq(coord))

def read_scene(filename):
    out = {}
    # try:
    with open(filename, "r") as f:
        d = json.load(f)
        out['radius'] = coordinate_to_FT(d['radius'])
        out['turn_time'] = d['turn_time']
        out['preprocess_time'] = d['preprocess_time']
        out['number_of_turns'] = d['number_of_turns']
        out['makespan'] = d['makespan']
        out['red_robots'] = [Point_2(coordinate_to_FT(p[0]), coordinate_to_FT(p[1])) for p in d['red_robots']]
        out['blue_robots'] = [Point_2(coordinate_to_FT(p[0]), coordinate_to_FT(p[1])) for p in d['blue_robots']]
        out['goals'] = [{ 'center': Point_2(coordinate_to_FT(g['center'][0]), coordinate_to_FT(g['center'][1])), 'radius': coordinate_to_FT(g['radius']) } for g in d['goals']]
        out['obstacles'] = []
        for obstacle in d['obstacles']:
            p = Polygon_2([Point_2(coordinate_to_FT(p[0]), coordinate_to_FT(p[1])) for p in obstacle])
            if p.is_clockwise_oriented():
                p.reverse_orientation()
            out['obstacles'].append(p)
        out['bonuses'] = [Point_2(coordinate_to_FT(p[0]), coordinate_to_FT(p[1])) for p in d['bonuses']]
    # except Exception as e:
    #     print('read_scene:', e)
    #     out = None
    return out


def save_path(path, filename):
    file = open(filename, 'w')
    for i in range(len(path)):
        p = path[i]
        x = p.x().exact()
        y = p.y().exact()
        line = str(x.numerator()) + '/' + str(x.denominator()) + ' ' + str(y.numerator()) + '/' + str(y.denominator())
        if i < len(path) - 1:
            line = line + '\n'
        file.write(line)
    file.close()


def load_path(path, filename, number_of_robots):
    with open(filename, 'r') as file:
        for line in file:
            coords = line.split(" ")
            tup = []
            for i in range(number_of_robots):
                x = coordinate_to_FT(coords[i * 2])
                y = coordinate_to_FT(coords[i * 2 + 1])
                p = Point_2(x, y)
                tup.append(p)
            path.append(tup)
