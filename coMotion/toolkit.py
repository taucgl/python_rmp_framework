import sys
import subprocess

from PyQt5 import QtWidgets, QtCore

from toolkit_gui import ToolkitGUI

CMD_PLAY = 'python coMotion/game.py'
CMD_REWIND = 'python coMotion/rewind.py'
CMD_CONFIGURE = 'python coMotion/configure.py'
CMD_DESIGN = 'python coMotion/designer.py'

# Toolkit icons were taken from:
#   https://uxwing.com

class ToolkitGUIApp(QtWidgets.QMainWindow, ToolkitGUI):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)

        self.setWindowTitle('CoMotion Toolkit')

        self.playButton.clicked.connect(lambda: self.run_command(CMD_PLAY))
        self.rewindButton.clicked.connect(lambda: self.run_command(CMD_REWIND))
        self.configureButton.clicked.connect(lambda: self.run_command(CMD_CONFIGURE))
        self.designButton.clicked.connect(lambda: self.run_command(CMD_DESIGN))
    
    def run_command(self, cmd):
        subprocess.Popen(cmd, shell=True, stdin=None, stdout=None, stderr=None, close_fds=True)

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    gui = ToolkitGUIApp()
    gui.show()
    sys.exit(app.exec_())
    