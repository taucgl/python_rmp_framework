"""
The DiscoPygal version of the CoMotion game

You may add the class here to add any GUI widgets to your liking
"""
from os import path
from PyQt5 import QtCore
from PyQt5.QtCore import QSequentialAnimationGroup
from PyQt5.QtGui import QPen, QColor

import geometry_utils.conversions as conversions

from game.comotion_events import *

FIRST_TEAM_COLOR = QtCore.Qt.red
SECOND_TEAM_COLOR = QtCore.Qt.blue

BONUS_COLOR = QtCore.Qt.yellow
GOAL_HALO_COLOR_SECOND = QtCore.Qt.green
GOAL_HALO_COLOR_FIRST = QColor(255, 128, 0)

TURN_DURATION = 500

class CoMotion_DiscoPygal(object):
    """
    A helper class that handles all the GUI drawing of the CoMotion game
    """
    def __init__(self, gui):
        # Connect the helper to the gui of DiscoPygal
        self.gui = gui

        # A LUT that for every CoMotion_Robot returns the corresponding gui disc
        self.robot_to_gui_lut = {}
        # A LUT that for every CoMotion_Bonus returns the corresponding gui disc
        self.bonus_to_gui_lut = {}
    
    def connect_game(self, game):
        """
        Connect the DiscoPygal helper to the CoMotion game
        """
        # Draw obstacles, bonuses and goals
        for obstacle in game.obstacles:
            self.gui.add_polygon(conversions.polygon_2_to_tuples_list(obstacle), fill_color=QtCore.Qt.gray)
        for bonus in game.bonuses:
            self.bonus_to_gui_lut[bonus] = self.gui.add_disc(bonus.entity_radius.to_double(), bonus.location.x().to_double(), bonus.location.y().to_double(), BONUS_COLOR)
        for goal in game.goals:
            self.gui.add_disc(goal.entity_radius.to_double(), goal.location.x().to_double(), goal.location.y().to_double(), QtCore.Qt.transparent, QtCore.Qt.magenta)
        
        # Draw player robots
        for robot in game.first_player.robots:
            self.robot_to_gui_lut[robot] = self.gui.add_disc_robot(robot.radius.to_double(), robot.location.x().to_double(), robot.location.y().to_double(), "", FIRST_TEAM_COLOR, QtCore.Qt.transparent)
        for robot in game.second_player.robots:
            self.robot_to_gui_lut[robot] = self.gui.add_disc_robot(robot.radius.to_double(), robot.location.x().to_double(), robot.location.y().to_double(), "", SECOND_TEAM_COLOR, QtCore.Qt.transparent)


    def update_robot_halos(self, events):
        """
        Update the halos of robots entering and exiting goals
        """
        for event_list in events:
            for event in event_list:
                if event.event_type == COMOTION_EVENT_GOAL_ENTER:
                    robot = self.robot_to_gui_lut[event.causer]
                    pen = QPen()
                    pen.setWidthF(self.gui.base_line_width / self.gui.zoom)
                    color = GOAL_HALO_COLOR_FIRST
                    if event.causer.player.player_id == 1:
                        color = GOAL_HALO_COLOR_SECOND
                    pen.setColor(color)
                    robot.disc.setPen(pen)
                if event.event_type == COMOTION_EVENT_GOAL_EXIT:
                    robot = self.robot_to_gui_lut[event.causer]
                    pen = QPen()
                    pen.setWidthF(self.gui.base_line_width / self.gui.zoom)
                    pen.setColor(QtCore.Qt.transparent)
                    robot.disc.setPen(pen)

    def animate_robot_paths(self, paths, events):
        """
        Animate all robots along their according paths (in parallel)
        Also animate the events
        """
        if len(paths) == 0:
            return

        # For every edge segment of the path, move in parallel all the robots
        # After that, add all event animations (if relevant)
        path_len = len(list(paths.values())[0])
        animations = []
        for i in range(path_len):
            # All robots move in parallel along their edge
            animation_edges = []
            for robot in paths:
                robot_gui = self.robot_to_gui_lut[robot]
                edge = paths[robot][i]
                ix = edge.source().x().to_double()
                iy = edge.source().y().to_double()
                x = edge.target().x().to_double()
                y = edge.target().y().to_double()
                animation_edges.append(self.gui.linear_translation_animation(robot_gui, ix, iy, x, y, TURN_DURATION))
            animations.append(self.gui.parallel_animation(*animation_edges))

            # After the robots reach their destination, add also event animations
            for event in events[i]:
                if event.event_type == COMOTION_EVENT_COLLECT_BONUS:
                    animations.append(self.gui.visibility_animation(self.bonus_to_gui_lut[event.entity], False))
        
        self.gui.queue_animation(*animations)
        self.gui.play_queue()