# CoMotion - The Coordinated Motion Game

CoMotion lets you test both your (coordinated) motion planning abilities and AI descision making against 
another team of robots.
Each game has two teams of disc robots which fight in an arena - cluttered with obstacles, goals and bonus coupons.

## Rules:
1. At each turn, one of the players can move its robots in the arena.
2. During the game, both players can do actions that gain them points for
    the player's score.
3. When the game ends - the player with the highest score wins.
4. The game ends after a fixed (per scene) amount of turns.
5. Both players has some limit on the amount of distance their robots can travel.
    Each turn, after every edge, the game computes the robots' total distance.
    If at some edge the total distance surpasses the limit - then the game only playes
    up to (but not including) the violating edge. The same rule applies to collision - 
    if at some edge one of the robots collides with an obstacle/another robot the game
    discards motion from that edge and further.
6. The robots can collect coupons - when a robot collides with a coupon,
    the coupon disappears from the game and the player that his robot
    collided with it gets the score.
7. The robots can also capture zones ("goals") - at the end of the game, each robot that 
        lies inside one ofthe designated zones grants the player with additional score.

## How to run

You can run the game using the CoMotion Toolkit:

> python coMotion/toolkit.py

From there you can open the scene designer, game configuration tool (which creates a game 
configuration like described below) and of course playing the game (in real time or rewinding
from a history file).

Here is a video tutorial on using the toolkit:

[![CoMotion Toolkit Demonstration](http://img.youtube.com/vi/xOGmXjM12c8/0.jpg)](http://www.youtube.com/watch?v=xOGmXjM12c8 "CoMotion Toolkit Demonstration")

## The game configuration

The game configuration is defined as a JSON file, with the following format:

> {
>
>       "scene": "JSON file that describes the obstacles, robot locations, goals/coupons and settings",
>
>       "first_module": "Pythonic path to the module containing the class code, e.g. coMotion.game.comotion_player",
>       "first_class": "The player behaviour class defined in the above module, e.g. CoMotion_ScriptReaderPlayer",
>       "first_args": "string of comma seperated arguments for constructor of player, can be empty string",
>
>       "second_module": "Pythonic path to the module containing the class code, e.g. coMotion.game.comotion_player",
>       "second_class": "The player behaviour class defined in the above module, e.g. CoMotion_ScriptReaderPlayer",
>       "second_args": "string of comma seperated arguments for constructor of player, can be empty string"
> }

## Folder structure

Below is a list describing all the different folders in the project, but note that (as explained below),
the most important folder - the one that contains the logic of the game - is `coMotion/game`.

* gui - the DiscoPygal gui module.
* bindings - CGALPY bindigns
* geometry_utils - contains various computational geometry utilities, including *collision detection*.
* coMotion - the root directory of the framework. contains all the Toolkit parts and the modules and resource files
    needed for the CoMotion game.
* coMotion/configs - game configurations (like described above). Look at `random_game.json` for an example. These can also
    be creating using the "Configure" tool in the Toolkit.
* coMotion/game - *the most important folder.* a module that contains the logic behind the coMotion game. note that all the code that is relevant to any AI
    or game logic will be exclusively in this module - all the rest of the code is just GUI wrappings and utilities.
* coMotion/img - image resources for the toolkit icons.
* coMotion/misc - PyQt5 ui files for the various apps in the Toolkit.
* coMotion/scenes - contains the different arenas (scenes) of the game.
* coMotion/scenes/scripts - various game histories that can be replayed later
* coMotion/tests - unit tests