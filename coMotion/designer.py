import os
import webbrowser

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QFileDialog
from designer_gui import *
from gui import RPolygon, RDisc
import sys
import json
import math
import circular_room


def setup_logic(gui):
    """
    Connect and setup the logic of the UI
    """
    gui.set_logic('mode_polygonal_obstacles', lambda: set_drawing_mode(gui, MODE_POLYGON_OBSTACLE))
    gui.set_logic('mode_bonuses', lambda: set_drawing_mode(gui, MODE_BONUSES))
    gui.set_logic('mode_red_robots_start', lambda: set_drawing_mode(gui, MODE_RED_ROBOTS_START))
    gui.set_logic('mode_blue_robots_start', lambda: set_drawing_mode(gui, MODE_BLUE_ROBOTS_START))
    gui.set_logic('mode_goals', lambda: set_drawing_mode(gui, MODE_GOALS))
    set_drawing_mode(gui, MODE_POLYGON_OBSTACLE) # This is the default mode
    gui.set_animation_finished_action(lambda: None)

    # Set resolution
    gui.set_field('resolution', str(gui.resolution))
    gui.set_logic('set_resolution', lambda: set_resolution(gui))

    # Set defaults
    gui.set_field('robot_radius', str(gui.radius))
    gui.set_field('number_of_turns', str(gui.number_of_turns))
    gui.set_field('turn_time', str(gui.turn_time))
    gui.set_field('preprocess_time', str(gui.preprocess_time))
    gui.set_field('makespan', str(gui.makespan))
    gui.set_field('goal_radius', '1.0')

    gui.set_logic('help', display_help)
    gui.set_logic('clear', lambda: clear(gui))
    gui.set_logic('select_polygon', lambda: select_button(gui))
    gui.set_logic('save', lambda: export_scene(gui))
    gui.set_logic('load', lambda: load_scene(gui))
    gui.set_logic('file_browser', lambda: browse_input_file(gui))
    gui.set_logic('set_radius', lambda: set_radius(gui))

    gui.scene.right_click_signal.connect(lambda x, y: right_click(gui, x, y))
    gui.scene.left_click_signal.connect(lambda x, y: left_click(gui, x, y))
    gui.mainWindow.signal_ctrl_z.connect(lambda: undo(gui))
    gui.mainWindow.signal_delete.connect(lambda: remove_selected(gui))
    gui.mainWindow.signal_esc.connect(lambda: clear_current_polyline(gui))
    gui.mainWindow.signal_drop.connect(lambda file_path: drop_input_file(gui, file_path))



def display_help():
    webbrowser.open_new('file://' + os.path.realpath(r".\\readme.md"))


def clear(gui):
    for obstacle in gui.gui_polygon_obstacles:
        gui.scene.removeItem(obstacle.polygon)
    gui.gui_polygon_obstacles.clear()
    gui.polygon_obstacles.clear()

    for bonus in gui.gui_bonuses:
        gui.scene.removeItem(bonus.polygon)
    gui.gui_bonuses.clear()
    gui.bonuses = []

    for robot in gui.gui_red_robots_starts:
        gui.scene.removeItem(robot.disc)
    gui.gui_red_robots_starts.clear()
    gui.red_robots_starts.clear()

    for robot in gui.gui_blue_robots_starts:
        gui.scene.removeItem(robot.disc)
    gui.gui_blue_robots_starts.clear()
    gui.blue_robots_starts.clear()

    for goal in gui.goals:
        gui.scene.removeItem(goal.disc)
    gui.gui_goals.clear()
    gui.goals.clear()


    clear_current_polyline(gui)


def redraw_grid(gui, size):
    """
    Draw a grid in out gui with a given size
    """
    # Clear any old grid lines
    for segment in gui.grid:
        gui.scene.removeItem(segment.line)
    gui.grid.clear()

    length = size * gui.resolution
    
    # Draw the grid lines (except axes)
    color = QtCore.Qt.lightGray
    for i in range(-size, size):
        if i == 0:
            continue # We will draw the main axes sperately
        i = i * gui.resolution
        gui.grid.append(gui.add_segment(-length, i, length, i, line_color=color))
        gui.grid.append(gui.add_segment(i, -length, i, length, line_color=color))
    
    # Draw the axes
    color = QtCore.Qt.black
    gui.grid.append(gui.add_segment(-length, 0, length, 0, line_color=color))
    gui.grid.append(gui.add_segment(0, -length, 0, length, line_color=color))

    # Push all segments to the back
    for segment in gui.grid:
        segment.line.setZValue(-1)

def set_radius(gui):
    gui.radius = float(gui.get_field('robot_radius'))

    for robot in gui.gui_red_robots_starts:
        gui.scene.removeItem(robot.disc)
    red_robots_starts = gui.red_robots_starts
    gui.red_robots_starts = []
    for robot in red_robots_starts:
        submit_red_start(gui, *robot)

    for robot in gui.gui_blue_robots_starts:
        gui.scene.removeItem(robot.disc)
    blue_robots_starts = gui.blue_robots_starts
    gui.blue_robots_starts = []
    for robot in blue_robots_starts:
        submit_blue_start(gui, *robot)

def set_drawing_mode(gui, mode):
    """
    Set the drawing mode to the selected one
    (And update the labels accordingly)
    """
    gui.drawing_mode = mode
    gui.set_label('current_mode', MODE_CURRENT_STR.format(MODE_NAMES[mode])) 
    clear_current_polyline(gui)


def clear_current_polyline(gui):
    """
    Discard the polygline we currently draw
    """
    gui.polyline = []
    for vertex in gui.gui_current_polygon_vertices:
        gui.scene.removeItem(vertex.disc)
    gui.gui_current_polygon_vertices.clear()
    for edge in gui.gui_current_polygon_edges:
        gui.scene.removeItem(edge.line)
    gui.gui_current_polygon_edges.clear()


def submit_polygon_obstacle(gui):
    gui.polygon_obstacles.append(gui.polyline)
    gui.gui_polygon_obstacles.append(
        gui.add_polygon(gui.polyline, fill_color=QtCore.Qt.transparent, line_color=QtCore.Qt.blue)
    )
    clear_current_polyline(gui)

def submit_goal(gui, x, y, radius):
    gui.goals.append({'center': [x, y], 'radius': radius})
    gui.gui_goals.append(gui.add_disc(radius, x, y, fill_color=QtCore.Qt.transparent, line_color=QtCore.Qt.magenta))

def submit_red_start(gui, x, y):
    gui.red_robots_starts.append([x, y])
    gui.gui_red_robots_starts.append(gui.add_disc(gui.radius, x, y, fill_color=QtCore.Qt.red))

def submit_blue_start(gui, x, y):
    gui.blue_robots_starts.append([x, y])
    gui.gui_blue_robots_starts.append(gui.add_disc(gui.radius, x, y, fill_color=QtCore.Qt.blue))
    
def submit_bonus(gui, x, y):
    gui.bonuses.append([x, y])
    gui.gui_bonuses.append(gui.add_disc(BONUS_RADIUS, x, y, fill_color=QtCore.Qt.yellow))


def right_click(gui, x: float, y: float):
    """
    Handles the drawing by user (which is done by right clicking)
    """
    print('x', x, 'y', y)
    x = gui.resolution * round(x / gui.resolution)
    y = gui.resolution * round(y / gui.resolution)
    print(x, y)
    if gui.drawing_mode == MODE_POLYGON_OBSTACLE:
        if [x, y] in gui.polyline:
            if len(gui.polyline) >= 3:
                submit_polygon_obstacle(gui)
            else:
                clear_current_polyline(gui)
        else:
            gui.polyline.append([x,y])
            gui.gui_current_polygon_vertices.append(gui.add_disc(POINT_RADIUS, x, y, fill_color=QtCore.Qt.red))
            if len(gui.polyline) > 1:
                gui.gui_current_polygon_edges.append(
                    gui.add_segment(*gui.polyline[-2], *gui.polyline[-1], line_color=QtCore.Qt.red))
    elif gui.drawing_mode == MODE_BONUSES:
        submit_bonus(gui, x, y)
    elif gui.drawing_mode == MODE_RED_ROBOTS_START:
        submit_red_start(gui, x, y)
    elif gui.drawing_mode == MODE_BLUE_ROBOTS_START:
        submit_blue_start(gui, x, y)
    elif gui.drawing_mode == MODE_GOALS:
        radius = float(gui.get_field('goal_radius'))
        submit_goal(gui, x, y, radius)


def left_click(gui, x: float, y: float):
    x = gui.resolution * round(x / gui.resolution)
    y = gui.resolution * round(y / gui.resolution)
    polygons = get_selected(gui, x, y)
    new_selection = {
        'index': None,
        'type': None
    }
    if polygons:
        new_selection['index'] = polygons[-1]
        new_selection['type'] = MODE_POLYGON_OBSTACLE
    select(gui, new_selection)


def get_selected(gui, x, y):
    print(x, y)
    gui.writerEdit.setPlainText(str((x, y)))
    polygon_indices = []
    for i, polygon in enumerate(gui.polygon_obstacles):
        if [x, y] in polygon:
            polygon_indices.append(i)
    print('Polygon indices sharing the selected vertex:', *polygon_indices)
    gui.writerEdit.append('Polygon indices sharing the selected vertex:' + str(polygon_indices))
    return polygon_indices


def select(gui, new_selection):
    index = gui.selected['index']
    print("previously selected:", index)
    if index is not None:
        if gui.selected['type'] == MODE_POLYGON_OBSTACLE and 0 <= index < len(gui.polygon_obstacles):
            gui.scene.removeItem(gui.gui_polygon_obstacles[index].polygon)
            gui.gui_polygon_obstacles[index] = gui.add_polygon(gui.polygon_obstacles[index], fill_color=QtCore.Qt.transparent,
                                                           line_color=QtCore.Qt.blue)

    gui.selected['index'] = new_selection['index']
    gui.selected['type'] = new_selection['type']
    index = gui.selected['index']
    print("new selected:", index)
    if index is not None:
        if gui.selected['type'] == MODE_POLYGON_OBSTACLE and 0 <= index < len(gui.polygon_obstacles):
            gui.scene.removeItem(gui.gui_polygon_obstacles[index].polygon)
            gui.gui_polygon_obstacles[index] = gui.add_polygon(gui.polygon_obstacles[index], fill_color=QtCore.Qt.transparent,
                                                           line_color=QtCore.Qt.darkBlue)


def undo(gui):
    index = len(gui.polyline) - 1
    if 0 <= index < len(gui.polyline):
        gui.polyline.pop()
        gui.scene.removeItem(gui.gui_current_polygon_vertices.pop().disc)
        if len(gui.gui_current_polygon_edges) > 0:
            gui.scene.removeItem(gui.gui_current_polygon_edges.pop().line)


def remove_selected(gui):
    index = gui.selected['index']
    type = gui.selected['type']
    if type == MODE_POLYGON_OBSTACLE:
        if index is not None and 0 <= index < len(gui.polygon_obstacles):
            gui.polygon_obstacles.pop(index)
            gui.scene.removeItem(gui.gui_polygon_obstacles.pop(index).polygon)
    gui.selected['index'] = None


def select_button(gui):
    try:
        select(gui, {
            'index': int(gui.get_field('polygon_index')),
            'type': MODE_POLYGON_OBSTACLE
        })
    except Exception as e:
        print('select_button:', e)


def set_resolution(gui):
    try:
        gui.resolution = float(gui.get_field('resolution'))
        print('Resolution set to:', gui.resolution)
        gui.writerEdit.setPlainText('Resolution set to: ' + str(gui.resolution))
    except Exception as e:
        print('Failed to set resolution:', e)
    redraw_grid(gui, GRID_SIZE)


def export_scene(gui):
    filename = gui.get_field('output_path')
    d = {
        'red_robots': gui.red_robots_starts,
        'blue_robots': gui.blue_robots_starts,
        'goals': gui.goals,
        'obstacles': gui.polygon_obstacles,
        'bonuses': gui.bonuses,
        'radius': gui.radius,
        'turn_time': float(gui.get_field('turn_time')),
        'preprocess_time': float(gui.get_field('preprocess_time')),
        'number_of_turns': float(gui.get_field('number_of_turns')),
        'makespan': float(gui.get_field('makespan'))
    }
    try:
        with open(filename, 'w') as fp:
            fp.write(json.dumps(d, sort_keys=True))
            gui.writerEdit.setPlainText("Scene saved to: " + filename)
    except Exception as e:
        print("Failed to write to file", filename + ":", e)
        gui.writerEdit.setPlainText("Failed to write to file " + filename + ": " + str(e))


def load_scene(gui):
    filename = gui.get_field('input_path')
    try:
        with open(filename, 'r') as fp:
            d = json.load(fp)
        clear(gui)
        gui.radius = d['radius']
        gui.set_field('robot_radius', str(d['radius']))
        gui.set_field('makespan', str(d['makespan']))
        gui.set_field('preprocess_time', str(d['preprocess_time']))
        gui.set_field('number_of_turns', str(d['number_of_turns']))
        gui.set_field('makespan', str(d['makespan']))
        for polygon in d['obstacles']:
            gui.polyline = polygon
            submit_polygon_obstacle(gui)
        for bonus in d['bonuses']:
            submit_bonus(gui, *bonus)
        for robot in d['red_robots']:
            submit_red_start(gui, *robot)
        for robot in d['blue_robots']:
            submit_blue_start(gui, *robot)
        for goal in d['goals']:
            submit_goal(gui, *goal['center'], goal['radius'])
        
    except Exception as e:
        print("Failed to load from file " + filename + ": " + str(e))
        gui.writerEdit.setPlainText("Failed to load from file " + filename + ": " + str(e))


def get_file():
    dlg = QFileDialog()
    dlg.setFileMode(QFileDialog.AnyFile)
    dlg.setDirectory('')
    if dlg.exec_():
        filenames = dlg.selectedFiles()
        return filenames[0]


def browse_input_file(gui):
    file_path = get_file()
    if file_path:
        gui.set_field('input_path', file_path)


def drop_input_file(gui, file_path):
    gui.set_field('input_path', file_path)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    gui = GUI_scene_designer_game()
    gui.set_program_name("Robot Motion Planning - Game - Scene Designer")

    setup_logic(gui)

    # Draw the grid with origin
    redraw_grid(gui, GRID_SIZE)
    gui.add_disc(POINT_RADIUS, 0, 0)

    gui.mainWindow.show()
    sys.exit(app.exec_())