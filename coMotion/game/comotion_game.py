import time
from typing import List

from bindings import *
import geometry_utils.collision_detection as collision_detection

from .comotion_config import comotion_config
from .comotion_entities import CoMotion_Bonus, CoMotion_Goal
from .comotion_events import *

class CoMotion_Game(object):
    """
    The CoMotion Game managaer class
    Manages the gameflow and rules of the CoMotion game.

    Rules:
        1. At each turn, one of the players can move its robots in the arena.
        2. During the game, both players can do actions that gain them points for
            the player's score.
        3. When the game ends - the player with the highest score wins.
        4. The game can end in three situations - either the turn limit has ended, or when
            one of the players "dominated" the game (got such score that the other player
            cannot surpass).
        5. Both players has some limit on the amount of time/distance their robots
            can travel. Each turn, after every edge the game computes the robots' makespace.
            If at some edge the makespan surpasses the limit - then the game only playes
            up to (but not including) the violating edge. The same rule applies to collision - 
            if at some edge one of the robots collides with an obstacle/another robot the game
            discards motion from that edge and further.
        6. The robots can collect coupons - when a robot collides with a coupon,
            the coupon disappears from the game and the player that his robot
            collided with it gets the score.
        7. The robots can also capture zones - at the end of the game, each robot that 
            lies inside one ofthe designated zones grants the player with additional score.
    """
    def __init__(self, config, first_player, second_player):
        """
        Setup a new game setting, with given config arguments
        """

        ##############################
        # Scene Geometry & Structure
        ##############################
        # Robot radius
        self.radius: FT = config['radius']
        # The goals are CoMotion_Goal objects
        self.goals = []
        for goal in config['goals']:
            self.goals.append(CoMotion_Goal(goal['center'], goal['radius'], self.radius))
        # The bonuses are CoMotion_Bonus objects
        self.bonuses = []
        for bonus_location in config['bonuses']:
            self.bonuses.append(CoMotion_Bonus(bonus_location, FT(comotion_config.bonus_radius), self.radius))
        # Array of booleans, each is True iff the respective bonus has been collected
        self.collected: List[bool] = []
        # The obstacles are Polygon_2 objects
        self.obstacles: List[Polygon_2] = config['obstacles']
        self.obstacles_cd = collision_detection.Collision_detector(self.obstacles, [], self.radius)

        ################
        # Players
        ################
        self.first_player = first_player
        self.second_player = second_player
        self.current_player = self.first_player
        self.other_player = self.second_player


        # Turn counter, we start at -2 since both players will first preprocess
        self.turn: int = 0
        # Turn time
        self.turn_time: float = config['turn_time']
        # Preprocessing time
        self.preprocess_time: float = config['preprocess_time']
        # Total number of turns
        self.max_turns: int = config['number_of_turns']
        
        # Turn makespan
        self.makespan: float = config['makespan']
    

    def swap_current_player(self):
        self.current_player, self.other_player = self.other_player, self.current_player


    def check_for_makespan(self, paths):
        """
        Get the dict of paths returned by player and return the index
        of edge where he exceeded the makespan amount (or -1 if everything is okay)
        """
        if len(paths) == 0:
            return -1
        
        current_makespan = 0
        path_len = len(list(paths.values())[0])
        for i in range(path_len):
            for robot in paths:
                edge = paths[robot][i]
                current_makespan += (edge.squared_length().to_double())**0.5
                if current_makespan > self.makespan:
                    return i
        return -1
    

    def was_bonus_collected(self, events, bonus):
        """
        Bugfix: we actually collect the bonus only a posteriori
        Hence while adding events in path traversal, we need to check
        if we already added the bonus collection event to the pool
        """
        for event_list in events:
            for event in event_list:
                if event.entity == bonus:
                    return True
        return False

    
    def play_turn(self):
        """
        Play the current turn

        Return: 
            paths (dict<CoMotion_Robot, list<Segment_2>): the paths (as list of linear segments)
                that each robot did this turn
            events (list<list<CoMotion_Event>>): for each time/segment store a list of all the events
                that occurred on that time segment
        """
        paths = self.current_player.get_turn_robot_paths()
        events = []

        # Make sure that all robot paths are equal
        lengths = []
        for robot in paths:
            lengths.append(len(paths[robot]))
        if len(lengths) > 0:
            assert(min(lengths) == max(lengths))

        trim = -1

        for robot in paths:
            path = paths[robot]
            curr_trim = 0

            for i, edge in enumerate(path):
                if len(events) <= i:
                    events.append([])

                #################
                # Validate paths
                #################
                path_valid = True
                # Collision with obstacles
                if not self.obstacles_cd.is_edge_valid(edge):
                    events[i].append(CoMotion_Event(COMOTION_EVENT_COLLISION_OBSTACLES, robot, None)) # Notify as event
                    path_valid = False
                
                # Collision with robots
                parallel_edges = [paths[other_robot][i] for other_robot in paths]
                other_team_edges = [Segment_2(other_robot.location, other_robot.location) for other_robot in self.other_player.robots]
                total_robot_edges = parallel_edges + other_team_edges
                if collision_detection.check_intersection_against_robots(total_robot_edges, [self.radius] * len(total_robot_edges)):
                    events[i].append(CoMotion_Event(COMOTION_EVENT_COLLISION_ROBOTS, robot, None)) # Notify as event
                    path_valid = False
                
                if not path_valid:
                    break
                
                #################
                #################

                ##########
                # Events
                ##########
                # Bonuses
                for bonus in self.bonuses:
                    if not bonus.is_collected and bonus.is_colliding_player(robot, edge) and not self.was_bonus_collected(events, bonus):
                        events[i].append(CoMotion_Event(COMOTION_EVENT_COLLECT_BONUS, robot, bonus))
                ##########
                ##########
                curr_trim += 1

            if trim == -1 or curr_trim < trim:
                trim = curr_trim
        
        # Check also for makespan
        makespan_trim = self.check_for_makespan(paths)
        if makespan_trim >= 0 and makespan_trim <= trim:
            events[makespan_trim].clear() # clear previous since we dont want to accidenetly grand bonuses
            events[makespan_trim].append(CoMotion_Event(COMOTION_EVENT_MAKESPAN_OVER, None, None))
            trim = makespan_trim
                
        # Trim all paths (for cases of makespan/collision violation)
        for robot in paths:
            paths[robot] = paths[robot][:trim]
        events = events[:trim+1]

        # Update all robot locations properly
        for robot in paths:
            if len(paths[robot]) > 0:
                robot.location = paths[robot][-1].target()

        # Update all bonuses
        for events_i in events:
            for event in events_i:
                if event.event_type == COMOTION_EVENT_COLLECT_BONUS:
                    event.entity.collect(event.causer)
                    pass

        # At the end of the turn, also check if robots entered/exited goals
        # and update the statuses accordingly
        for robot in paths:
            for goal in self.goals:
                res = goal.update_robot_status(robot)
                if res == 1:
                    events[-1].append(CoMotion_Event(COMOTION_EVENT_GOAL_ENTER, robot, goal))
                elif res == -1:
                    events[-1].append(CoMotion_Event(COMOTION_EVENT_GOAL_EXIT, robot, goal))

        self.swap_current_player()
        self.turn += 1

        # Also return the paths for extrenal gui to animate
        # (or for a writer to write to file)
        return paths, events


    def check_winner(self):
        """
        Check the scores of both players, and return the index (1 or 2) of the winning player
        If draw then we return 0
        """
        score_1 = self.first_player.score
        score_2 = self.second_player.score
        if score_1 == score_2:
            return 0
        if score_1 > score_2:
            return 1
        return 2
