import json
import random

import numpy as np
import networkx as nx
import sklearn.neighbors

from bindings import *
import geometry_utils.collision_detection as collision_detection


class CoMotion_Robot(object):
    """
    A class that represents the data of each robot, and a pointer to its
    player parent
    """
    def __init__(self, location, radius, player):
        global id

        self.location = location
        self.radius = radius
        self.player = player
        self.score = 0


class CoMotion_Player(object):
    """
    An abstract player class, which should be overriden to implement the actual logic
    The player also receives an id (0, 1) if it is first or second
    """
    def __init__(self, player_id):
        self.robots = []
        self.score = 0
        self.game = None # Store a reference to the game parent. READ ONLY! WRITING IS FORBIDDEN
        self.player_id = player_id

    def add_robots(self, robots):
        self.robots += robots

    def add_score(self, score):
        self.score += score

    def attach_game(self, game):
        self.game = game

    def preprocess(self):
        """
        The "-1" and "-2" turns of the game are preprocessing the scene.
        Should not return anythings, and you may only update internal
        structues of the player class.
        Note that this function should take reasonable time (maximum
        preprocessing time is defined by the game/scene rules).
        """
        pass

    def get_turn_robot_paths(self):
        """
        The main function to implement for the CoMotion player - 
        this is the function that computes and returns the motion path for
        each of the robot.
        The returned result is a dict where the keys are robots and the values
        are list of edges of motion for any robot.
        Note that all robots that move must have same amount of edges! (edges can be
        of zero length)
        """
        return {}


class CoMotion_ScriptReaderPlayer(CoMotion_Player):
    """
    A player that gets a ready scripts and at each turn, just execute what it was told
    (without any further processing).
    This is useful for testing the game framework.
    """
    def __init__(self, player_id, paths_script):
        """
        Args:
            paths_script (list<dict<int, list<Segment_2>>>): list of path
                commands for all the robots. Note that each dict stores 
                the *index* of the robot - as opposed to the usual CoMotion_Robot.
        """
        super().__init__(player_id)

        self.paths_script = self.read_scritps(paths_script)[int(player_id)]
        self.idx = 0 # increased every turn

    def read_scritps(self, filename):
        first_player = []
        second_player = []

        with open(filename, "r") as fp:
            d = json.load(fp)

        for item in d['first_player']:
            paths = {}
            for idx in item:
                paths[int(idx)] = []
                for j in range(len(item[idx])-1):
                    x1, y1 = item[idx][j]
                    x2, y2 = item[idx][j+1]
                    paths[int(idx)].append(Segment_2(Point_2(x1, y1), Point_2(x2, y2)))
            first_player.append(paths)

        for item in d['second_player']:
            paths = {}
            for idx in item:
                paths[int(idx)] = []
                for j in range(len(item[idx])-1):
                    x1, y1 = item[idx][j]
                    x2, y2 = item[idx][j+1]
                    paths[int(idx)].append(Segment_2(Point_2(x1, y1), Point_2(x2, y2)))
            second_player.append(paths)
        
        return first_player, second_player
    
    def get_turn_robot_paths(self):
        paths = self.paths_script[self.idx]

        # Fix paths to have pointer to robot instead of index
        new_paths = {}
        for idx in paths:
            new_paths[self.robots[idx]] = paths[idx]

        self.idx += 1
        return new_paths

class CoMotion_RandomPRMPlayer(CoMotion_Player):
    """
    A player that plays random PRM for its robots
    This is meant only as a test/example to how true logic should work.
    """
    def __init__(self, player_id, num_landmarks, K):
        super().__init__(player_id)

        # Store a (static) probabilistic roadmap of the scene
        # Also all robot locations are vertices in the roadmap
        self.scene_prm = None
        self.K = int(K)
        self.num_landmarks = int(num_landmarks)

    def preprocess(self):
        self.scene_prm = nx.Graph()

        # Compute the scene bounding box and sampling range
        bbox = collision_detection.calc_bbox(self.game.obstacles)
        x_range = (bbox[0].to_double(), bbox[1].to_double())
        y_range = (bbox[2].to_double(), bbox[3].to_double())

        # Init the PRM with the robot location vertices
        points = []
        for robot in self.robots:
            points.append(robot.location)
        self.scene_prm.add_nodes_from(points)

        # Sample landmarks
        i = 0
        while i < self.num_landmarks:
            rand_x = random.uniform(*x_range)
            rand_y = random.uniform(*y_range)
            point = Point_2(rand_x, rand_y)

            if self.game.obstacles_cd.is_point_valid(point):
                points.append(point)
                self.scene_prm.add_node(point)
                i += 1
                if i % 500 == 0:
                    print(i, "landmarks samples")
        
        # Connect by nearest neighbors
        _points = np.array([[p.x().to_double(), p.y().to_double()] for p in points])
        def custom_dist(p, q):
            return ((p[0]-q[0])**2 + (p[1]-q[1])**2)**0.5
        
        nearest_neighbors = sklearn.neighbors.NearestNeighbors(n_neighbors=self.K, metric=custom_dist, algorithm='auto')
        nearest_neighbors.fit(_points)

        for i in range(len(points)):
            p = points[i]
            k_neighbors = nearest_neighbors.kneighbors([_points[i]], return_distance=False)

            for j in k_neighbors[0]:
                neighbor = points[j]
                edge = Segment_2(p, neighbor)
                if self.game.obstacles_cd.is_edge_valid(edge):
                    weight = custom_dist([p.x().to_double(), p.y().to_double()], [neighbor.x().to_double(), neighbor.y().to_double()])
                    self.scene_prm.add_edge(p, neighbor, weight=weight)
            if i % 100 == 0:
                print("Connected", i, "landmarks to their nearest neighbors")
        
    
    def advance_one_step(self):
        # Advance all robots in one random step, and return the total makespan
        makespan = 0
        steps = {}
        for robot in self.robots:
            neighbors = self.scene_prm[robot.location]
            random_next = random.choice(list(neighbors.keys()))
            weight = neighbors[random_next]['weight']
            edge = Segment_2(robot.location, random_next)
            # Check intersection with robots
            parallel_edges = [edge]
            for other_robot in steps:
                parallel_edges.append(steps[other_robot])
            other_team_edges = [Segment_2(other_robot.location, other_robot.location) for other_robot in self.game.other_player.robots]
            total_robot_edges = parallel_edges + other_team_edges
            if collision_detection.check_intersection_against_robots(total_robot_edges, [robot.radius] * len(total_robot_edges)):
                print('hi')
                steps[robot] = Segment_2(robot.location, robot.location) # stay in place
            else:
                steps[robot] = edge
                makespan += weight
                robot.location = random_next # Typically dangerous, but we return all robots back to place in the end
        
        return steps, makespan
    
    def get_turn_robot_paths(self):
        total_makespan = 0
        paths = {robot: [] for robot in self.robots}

        original_locations = {robot: robot.location for robot in self.robots}

        while True:
            steps, makespan = self.advance_one_step()
            if total_makespan + makespan >= self.game.makespan:
                break
            total_makespan += makespan
            for robot in steps:
                paths[robot].append(steps[robot])

        # restore original locations
        for robot in original_locations:
            robot.location = original_locations[robot]

        return paths
