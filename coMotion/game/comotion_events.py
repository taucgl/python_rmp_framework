###############
# Event Types #
###############
COMOTION_EVENT_COLLECT_BONUS = 0
COMOTION_EVENT_COLLISION_OBSTACLES = 1
COMOTION_EVENT_COLLISION_ROBOTS = 2
COMOTION_EVENT_MAKESPAN_OVER = 3
COMOTION_EVENT_GOAL_ENTER = 4
COMOTION_EVENT_GOAL_EXIT = 5

COMOTION_EVENT_NAMES = {
    COMOTION_EVENT_COLLECT_BONUS: '(Event: Collect Bonus)',
    COMOTION_EVENT_COLLISION_OBSTACLES: '(Event: Collision with obstacle)',
    COMOTION_EVENT_COLLISION_ROBOTS: '(Event: Collision with robots)',
    COMOTION_EVENT_MAKESPAN_OVER: '(Event: Makespan limit over)',
    COMOTION_EVENT_GOAL_ENTER: '(Event: Robot entered goal)',
    COMOTION_EVENT_GOAL_EXIT: '(Event: Robot exited goal)',
}

class CoMotion_Event(object):
    """
    An abstact class that represents a game event, where each event has
    the event type, the affected entity and the causing robot (if relevant)
    Also store any extra data needed
    """
    def __init__(self, event_type, causer, entity, data=None):
        self.event_type = event_type
        self.causer = causer
        self.entity = entity
        self.data = data

    def __repr__(self):
        return "{}: {} => {}".format(COMOTION_EVENT_NAMES[self.event_type], self.causer, self.entity)