from bindings import *
import geometry_utils.collision_detection as collision_detection

from .comotion_config import comotion_config

class CoMotion_Entity(object):
    """
    An abstact class that represents a game entity (except the players and obstacles)
    Each entity has some geometry, properties, and handles for collision with player
    We represent entities as discs - which have a center coordiante and radius.
    """
    def __init__(self, location: Point_2, entity_radius: FT, player_radius: FT):
        # Remember the geometry of the entity
        self.location = location
        self.entity_radius = entity_radius

        # Compute the expanded entity for collision detection
        self.player_radius = player_radius
        self.collision_detector = collision_detection.Collision_detector(
            [], [{'center': self.location, 'radius': self.entity_radius}], self.player_radius)
    
    def is_colliding_player(self, robot, edge):
        """
        Get reference to the current player and its edge of motion,
        and return if it collides with him.
        Also internally handle the collision if necessary.
        """
        return not self.collision_detector.is_edge_valid(edge)


class CoMotion_Bonus(CoMotion_Entity):
    """
    The bonus entity of the CoMotion game.
    Can be redeemed exactly once, and its value goes to the score of the 
    player who collected it.
    """
    def __init__(self, location: Point_2, entity_radius: FT, player_radius: FT):
        super().__init__(location, entity_radius, player_radius)

        # Each bonus also needs to remember if it was already collected
        self.is_collected = False
    
    def collect(self, robot):
        if self.is_collected:
            return
        self.is_collected = True
        robot.player.add_score(comotion_config.bonus_score)


class CoMotion_Goal(CoMotion_Entity):
    """
    The goal entity of the CoMotion game.
    Can contain one or more robots, and at the end of the game - each robot gets some score
    """
    def __init__(self, location: Point_2, entity_radius: FT, player_radius: FT):
        super().__init__(location, entity_radius, player_radius)

        # Maintain which robots are inside the goal at any given time
        self.colliding_robots = []
    
    def is_robot_inside(self, robot):
        x1, y1 = robot.location.x().to_double(), robot.location.y().to_double()
        x2, y2 = self.location.x().to_double(), self.location.y().to_double()
        dist = ((x1 - x2)**2 + (y1 - y2)**2)**0.5
        sum_radii = robot.radius.to_double() + self.entity_radius.to_double()
        return dist <= sum_radii
    
    def update_robot_status(self, robot):
        """
        For a given robot, update its status with respect to the goal
        If the robot entered the goal append it to the internal data structure,
        and otherwise remove it.

        Return 1 if goal enter, -1 if goal exit or 0 if none
        """
        inside = self.is_robot_inside(robot)
        if inside and robot not in self.colliding_robots:
            self.colliding_robots.append(robot)
            robot.player.add_score(comotion_config.goal_score)
            return 1
        if not inside and robot in self.colliding_robots:
            self.colliding_robots.remove(robot)
            robot.player.add_score(-comotion_config.goal_score)
            return -1
        return 0
