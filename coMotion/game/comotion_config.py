class CoMotion_Config(object):
    def __init__(self):
        self.max_turns = 20
        self.p1_robots = 3
        self.p2_robots = 3

        self.bonus_score = 10
        self.bonus_radius = 0.5
        self.goal_score = 25

comotion_config = CoMotion_Config()