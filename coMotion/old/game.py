from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QFileDialog
import time
import importlib
from importlib import util
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QGraphicsLineItem
from PyQt5.QtCore import Qt
from gui_mp_workshop import GUI_workshop
from Scene import Scene
from gui.Worker import Worker
import copy_list


def set_up_scene():
    global game_over
    game_over = False
    gui.clear_scene()
    scene_file = gui.get_field('scene')
    try:
        scene.load_scene(scene_file)
        print("loaded scene from", scene_file)

        gui.set_label('totalTurns', 'Total number of turns: ' + str(scene.number_of_turns))
        gui.set_label('makespan', "Remaining makespan ("+str(scene.makespan)+"):")

        if autoplay:
            gui.set_animation_finished_action(play_turn)  # Play the next turn automatically
            gui.set_button_text('play', "Play game")
            gui.set_logic('play', initialize)
        else:
            gui.set_animation_finished_action(enable_advance)  # Enable playing the next turn
            gui.set_button_text('play', "Unavailable")
            gui.set_button_text('advance', "Advance")
            gui.set_logic('advance', initialize)
    except Exception as e:
        print('failed to load scene:', e)
        raise(e)


def initialize():
    disable_advance()
    scene.path = []
    gui.empty_queue()
    if scene.turn % 2 == 0:
        path_name = gui.get_field('red')
        team_robots = scene.red_robots
        opponent_robots = scene.blue_robots
        data = scene.red_data
    else:
        path_name = gui.get_field('blue')
        team_robots = scene.blue_robots
        opponent_robots = scene.red_robots
        data = scene.blue_data
    spec = importlib.util.spec_from_file_location(path_name, path_name)
    gp = util.module_from_spec(spec)
    spec.loader.exec_module(gp)
    params = [
        scene.radius,
        scene.turn_time,
        scene.preprocess_time,
        scene.number_of_turns,
        scene.makespan,
        ]

    params.extend([
        copy_list.copy_points([robot[0] for robot in team_robots]),
        copy_list.copy_points([robot[0] for robot in opponent_robots]),
        copy_list.copy_points([goal['center'] for goal in scene.goals]),
        [goal['radius'] for goal in scene.goals],
        copy_list.copy_polygons(scene.obstacles),
        copy_list.copy_points(scene.bonuses),
        data
    ])

    if scene.turn % 2 == 0:
        color = Qt.red
    else:
        color = Qt.blue
    gui.set_label('turn', "Initializing: " + str(scene.turn), color)
    print("Player " + str(scene.turn % 2 + 1) + " initializing started")
    worker = Worker(gp.initialize, params)
    worker.signals.finished.connect(preprocess)
    global t
    t = scene.preprocess_time
    preprocess_countdown()
    global timer
    timer = QTimer()
    timer.setInterval(100)
    timer.timeout.connect(preprocess_countdown)
    timer.start()
    global t0
    t0 = time.perf_counter()
    threadpool.start(worker)


# for debugging
def display_graph(G, n):
    colors = [Qt.green, Qt.magenta, Qt.darkYellow]
    for item in gui.scene.items():
        if isinstance(item, QGraphicsLineItem):
            gui.scene.removeItem(item)
    for edge in list(G.edges):
        for i in range(n):
            s = (edge[0][i*2].to_double(), edge[0][i*2+1].to_double())
            t = (edge[1][i*2].to_double(), edge[1][i*2+1].to_double())
            gui.add_segment(*s, *t, line_color=colors[i % len(colors)])


def preprocess():
    global game_over
    t1 = time.perf_counter()
    turn_time = t1 - t0
    timer.stop()
    print("Pre-processing took:", turn_time, "seconds")
    if turn_time > scene.preprocess_time:
        print("Pre-processing took too long!")
        scene.path = []
        game_over = True
        gui.set_label('totalTurns', "Pre-processing took too long!")
        gui.set_label('turnTime', "Player " + str((scene.turn + 1) % 2 + 1) + " wins!")
    else:
        # Uncomment to display the graph after pre-processing
        # G = scene.red_data[-5] # change this to where the graph is stored
        # display_graph(G, 1) # change "1" to the (effective) dimension of the points in the graph divided by 2
        scene.turn += 1
        print("Initializing done for player", 2 - (scene.turn % 2))
        if scene.turn < 0:
            initialize()
        else:
            scene.turn -= 1 # Since play turn raises scene.turn upon entering
            play_turn()


def play_turn():
    disable_advance()
    global game_over
    if game_over:
        return
    if scene.check_win_condition():
        game_over = True
        gui.set_label('totalTurns', "Win condition reached!")
        return
    if scene.turn + 1 == scene.number_of_turns:
        # Tie breaker
        game_over = True
        gui.set_label('turnTime', "")
        scene.determine_winner()
        return
    scene.turn += 1
    if scene.turn % 2 == 1:
        color = Qt.blue
    else:
        color = Qt.red
    gui.set_label('turn', "Turn: " + str(scene.turn), color)
    scene.path = []
    if scene.turn % 2 == 0:
        path_name = gui.get_field('red')
        team_robots = scene.red_robots
        opponent_robots = scene.blue_robots
        data = scene.red_data
    else:
        path_name = gui.get_field('blue')
        team_robots = scene.blue_robots
        opponent_robots = scene.red_robots
        data = scene.blue_data

    spec = importlib.util.spec_from_file_location(path_name, path_name)
    gp = util.module_from_spec(spec)
    spec.loader.exec_module(gp)
    params = [scene.path]

    params.extend([copy_list.copy_robots(team_robots),
                   copy_list.copy_robots(opponent_robots),
                   [collected for collected in scene.collected],
                   data,
                   scene.turn])
    print("Player " + str(scene.turn % 2 + 1) + " turn started")
    worker = Worker(gp.play_turn, params)
    worker.signals.finished.connect(process_turn)
    global t
    t = scene.turn_time
    countdown()
    global timer
    timer = QTimer()
    timer.setInterval(100)
    timer.timeout.connect(countdown)
    timer.start()
    global t0
    t0 = time.perf_counter()
    threadpool.start(worker)


def process_turn():
    global game_over
    t1 = time.perf_counter()
    turn_time = t1 - t0
    timer.stop()
    print("Turn took:", turn_time, "seconds")
    if turn_time > scene.turn_time:
        print("Turn took too long!")
        scene.path = []
        game_over = True
        gui.set_label('gameTime', "Turn took too long!")
        gui.set_label('turnTime', "Player " + str((scene.turn + 1) % 2 + 1) + " wins!")
    else:
        scene.process_turn()
        print("Done processing turn for player", (scene.turn % 2) + 1)
        animate_moves()
    if scene.turn % 2 == 0:
        gui.set_label('redScore', "Red team score: " + str(scene.red_score))
        # Uncomment to display the graph after each turn
        G = scene.red_data[-5] # change this to where the graph is stored
        display_graph(G, 1) # change "1" to the (effective) dimension of the points in the graph divided by 2
    else:
        gui.set_label('blueScore', "Blue team score: " + str(scene.blue_score))


def getfile():
    dlg = QFileDialog()
    dlg.setFileMode(QFileDialog.AnyFile)
    if dlg.exec_():
        filenames = dlg.selectedFiles()
        return filenames[0]


def set_scene_file():
    s = getfile()
    if s:
        gui.set_field('scene', s)


def set_red_file():
    s = getfile()
    if s:
        gui.set_field('red', s)


def set_blue_file():
    s = getfile()
    if s:
        gui.set_field('blue', s)


def animate_moves():
    gui.play_queue()


def countdown():
    global t
    t = max(0, t - 0.1)
    gui.set_label('turnTime', "Remaining turn time: " + str(round(t, 2)))


def preprocess_countdown():
    global t
    t = max(0, t - 0.1)
    gui.set_label('turnTime', "Remaining pre-processing time: " + str(round(t, 2)))


def enable_advance():
    gui.set_button_text('advance', "Advance")
    gui.set_logic('advance', play_turn)


def disable_advance():
    gui.set_button_text('advance', "Unavailable")
    gui.set_logic('advance', lambda: None)


if __name__ == "__main__":
    scene_file = ""
    red_file = ""
    blue_file = ""
    autoplay = False
    import sys
    if len(sys.argv) >= 2:
        scene_file = sys.argv[1]
    if len(sys.argv) >= 3:
        red_file = sys.argv[2]
    if len(sys.argv) >= 4:
        blue_file = sys.argv[3]
    if len(sys.argv) >= 5:
        autoplay = True
    t0 = None
    t = None
    timer = QTimer()
    game_over = False
    app = QtWidgets.QApplication(sys.argv)
    gui = GUI_workshop()
    scene = Scene(gui)
    gui.set_program_name("CoMotion")
    gui.set_field('scene', scene_file)
    gui.set_field('red', red_file)
    gui.set_field('blue', blue_file)
    gui.set_logic('load', set_up_scene)
    gui.set_logic('searchScene', set_scene_file)
    gui.set_logic('searchRed', set_red_file)
    gui.set_logic('searchBlue', set_blue_file)
    gui.set_button_text('load', "Load scene")
    gui.set_button_text('play', "Unavailable")
    gui.set_button_text('advance', "Unavailable")
    gui.set_button_text('searchScene', "...")
    gui.set_button_text('searchRed', "...")
    gui.set_button_text('searchBlue', "...")
    gui.set_label('turn', "Turn: -")
    gui.set_label('redScore', "Red team score: 0")
    gui.set_label('blueScore', "Blue team score: 0")
    gui.set_label('totalTurns', "Total number of turns: ")
    gui.set_label('turnTime', "Remaining turn time: ")
    gui.set_label('makespan', "Remaining makespan: ")
    # gui.set_animation_finished_action(lambda: None)
    gui.mainWindow.signal_drop.connect(lambda path: gui.set_field(0, path))
    threadpool = QtCore.QThreadPool()
    gui.mainWindow.show()
    sys.exit(app.exec_())
