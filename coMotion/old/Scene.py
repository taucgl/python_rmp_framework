import time

from bindings import *
from PyQt5.QtGui import QColor
from PyQt5.QtCore import Qt
from conversions import point_2_to_xy, tuples_list_to_polygon_2, polygon_2_to_tuples_list
import geometry_utils.collision_detection as collision_detection
import read_input
from gui_mp_workshop import GUI_workshop
import itertools
import math
from typing import List

GOAL_VALUE = 25
BONUS_VALUE = 10
BONUS_RADIUS = 1.0
colors = [Qt.yellow, Qt.green, Qt.red, Qt.blue, Qt.magenta]
OBJECTIVE_RADIUS_SCALE = 0.1

class Scene():
    def __init__(self, gui: GUI_workshop):
        self.gui = gui
        self.reset()

    def reset(self):
        # Turn counter
        self.turn: int = -2
        # Turn time
        self.turn_time: float = 0.0
        # Preprocessing time
        self.preprocess_time: float = 0.0
        # Total number of turns
        self.max_turns: int = 0
        # Robot radius
        self.radius: FT = FT(0)
        # Turn makespan
        self.makespan: float = 0.0
        # The robots are pairs of Point_2(center) and float (accumulated score)
        self.red_robots = []
        self.blue_robots = []
        # The goals have a center of type Point_2 objects and radius of type FT
        self.goals = []
        # The bonuses are Point_2 objects
        self.bonuses = []
        # Array of booleans, each is True iff the respective bonus has been collected
        self.collected: List[bool] = []
        # The obstacles are Polygon_2 objects
        self.obstacles: List[Polygon_2] = []
        self.gui_red_robots = []
        self.gui_blue_robots = []
        self.gui_goals = []
        self.gui_bonuses = []
        self.gui_obstacles = []
        # To be filled by the two teams each turn
        self.path = []
        self.red_score: float = 0
        self.blue_score: float = 0

        # Collision detector for intersection with obstacles
        self.collision_detector = None
        # Collision detectors for intersections with bonuses
        self.expanded_bonuses = []

        self.red_data = []
        self.blue_data = []

        self.gui.set_label('turn', "Turn: -")
        self.gui.set_label('redScore', "Red team score: 0")
        self.gui.set_label('blueScore', "Blue team score: 0")
        self.gui.set_label('totalTurns', "Total number of turns: ")
        self.gui.set_label('turnTime', "Remaining turn time: ")
        self.gui.set_progressbar_value('makespan', 0)

    def draw_scene(self):
        self.gui.empty_queue()
        self.gui.clear_scene()
        self.gui_robots = []
        for obstacle in self.obstacles:
            self.gui_obstacles.append(
                self.gui.add_polygon([point_2_to_xy(p) for p in obstacle.vertices()], Qt.darkGray))
        for robot in self.red_robots:
            self.gui_red_robots.append(
                self.gui.add_disc_robot(self.radius.to_double(), *point_2_to_xy(robot[0]), fill_color=QColor(Qt.red).lighter(130)))
        for robot in self.blue_robots:
            self.gui_blue_robots.append(
                self.gui.add_disc_robot(self.radius.to_double(), *point_2_to_xy(robot[0]), fill_color=QColor(Qt.blue).lighter(130)))
        for bonus in self.bonuses:
            p = self.gui.add_disc(BONUS_RADIUS, *point_2_to_xy(bonus), fill_color=Qt.yellow)
            self.gui_bonuses.append(p)
        for goal in self.goals:
            self.gui_goals.append(
                self.gui.add_disc(goal['radius'].to_double(), *point_2_to_xy(goal['center']), fill_color=Qt.transparent, line_color=QColor(Qt.magenta).darker(130)))
        self.gui.redraw()

    def load_scene(self, filename):
        self.reset()
        scene = read_input.read_scene(filename)
        if scene is None:
            raise Exception
        self.radius = scene['radius']
        self.turn_time = scene['turn_time']
        self.preprocess_time = scene['preprocess_time']
        self.number_of_turns = scene['number_of_turns']
        self.makespan = scene['makespan']
        for robot in scene['red_robots']:
            self.red_robots.append([robot, 0.0])
        for robot in scene['blue_robots']:
            self.blue_robots.append([robot, 0.0])
        self.goals = scene['goals']
        self.obstacles = scene['obstacles']
        self.bonuses = scene['bonuses']
        self.collected = [False for _ in self.bonuses]
        self.build_ex_sets()
        self.draw_scene()

    def build_ex_sets(self):
        self.collision_detector = collision_detection.Collision_detector(self.obstacles, [], self.radius)
        for bonus in self.bonuses:
            self.expanded_bonuses.append(collision_detection.Collision_detector([], [ {'center': bonus, 'radius': FT(BONUS_RADIUS)} ], self.radius))

    def is_step_valid(self, edges, d):
        # check that d is large enough
        s = 0
        for edge in edges:
            s += (edge.squared_length().to_double()) ** 0.5
        if s > d:
            print("Travel distances exceed D")
            return False
        # check intersection with robots
        if self.turn % 2 == 0:
            opponent_robots = self.blue_robots
        else:
            opponent_robots = self.red_robots
        if collision_detection.check_intersection_against_robots(edges+[Segment_2(robot[0], robot[0]) for robot in opponent_robots], [self.radius for _ in opponent_robots] * 2):
            print("Intersection with robots")
            return False
        # check intersection with obstacles
        for edge in edges:
            if not self.collision_detector.is_edge_valid(edge):
                print("Intersection with obstacles")
                return False
        return True

    def process_turn(self):
        robots = None
        gui_robots = None
        m = self.makespan
        if self.turn % 2 == 0:
            robots = self.red_robots
            gui_robots = self.gui_red_robots
        else:
            robots = self.blue_robots
            gui_robots = self.gui_blue_robots
        for step in self.path:
            edges = []
            for i in range(len(robots)):
                edges.append(Segment_2(robots[i][0], step[i]))

            if self.is_step_valid(edges, m):
                # decrease d by the largest distance traveled by a robot
                s = max(edge.squared_length().to_double() for edge in edges) ** 0.5
                m -= s

                # Collect bonuses
                # TODO currently the bonus may be attributed to the wrong robot
                # if during the same coordinated motion another robot has claimed it already
                bonus_anims = []
                for i in range(len(robots)):
                    edge = edges[i]
                    for i, expanded_bonus in enumerate(self.expanded_bonuses):
                        bonus = self.bonuses[i]
                        if not self.collected[i]:
                            if not expanded_bonus.is_edge_valid(edge):
                                if self.turn % 2 == 0:
                                    self.red_score += BONUS_VALUE
                                else:
                                    self.blue_score += BONUS_VALUE
                                robots[i][1] += BONUS_VALUE
                                self.collected[i] = True
                                bonus_anims.append(
                                    self.gui.visibility_animation(self.gui_bonuses[i], False))
                    bonus_anims.append(
                        self.gui.text_animation(gui_robots[i], int(robots[i][1])))

                # Queue animations
                for i in range(len(robots)):
                    robots[i][0] = step[i]
                anims = []
                for i in range(len(robots)):
                    edge = edges[i]
                    anims.append(self.gui.linear_translation_animation
                                 (gui_robots[i], *point_2_to_xy(edge.source()), *point_2_to_xy(edge.target())
                                                                              , duration=2000/len(self.path)))

                anim = self.gui.value_animation(self.gui.progressBars['makespan'], int(100*(m+s)/self.makespan), (100*m/self.makespan),
                                                duration=2000 / len(self.path))
                anim = self.gui.parallel_animation(*anims, anim)
                self.gui.queue_animation(anim)
                anim = self.gui.parallel_animation(*bonus_anims)
                self.gui.queue_animation(anim)
            else:
                break

    # TODO update
    def check_win_condition(self) -> bool:
        # if self.check_win_condition_for_team(self.red_robots, self.red_objectives, self.red_score):
        #     print("Player 1 wins!")
        #     self.gui.set_label('turnTime', "Player 1 wins!")
        #     return True
        # elif self.check_win_condition_for_team(self.blue_robots, self.blue_objectives, self.blue_score):
        #     print("Player 2 wins!")
        #     self.gui.set_label('turnTime', "Player 2 wins!")
        #     return True
        return False

    # TODO update
    def check_win_condition_for_team(self, robots, objectives: List[Point_2], team_score: float):
        if 2 * team_score >= self.total_bonuses_value:
            for robot in robots:
                flag: bool = False
                for objective in objectives:
                    if robot[0] == objective:
                        flag = True
                        break
                if not flag:  # No objective matches robot location
                    return False
            return True
        return False  # Score is less than 0.5 of total bonuses score

    def is_inside_goal(self, robot_center, goal):
        radius_diff = (goal["radius"] - self.radius)
        if Ker.squared_distance(robot_center, goal["center"]) <= radius_diff * radius_diff:
            return True

    def calculate_goal_bonus(self, robots):
        bonus = 0
        for robot in robots:
            for goal in self.goals:
                if self.is_inside_goal(robot[0], goal):
                    bonus += GOAL_VALUE
        return bonus

    def determine_winner(self):
        print("Calculating final score for each team")
        # Final score is number of robots inside a goal * GOAL_VALUE + collected bonuses * BONUS_VALUE
        red_score = self.calculate_goal_bonus(self.red_robots) + self.red_score
        blue_score = self.calculate_goal_bonus(self.blue_robots) + self.blue_score
        print("Player 1 score:", red_score)
        print("Player 2 score:", blue_score)
        if red_score > blue_score:
            print("Player 1 wins!")
            self.gui.set_label('turnTime', "Player 1 wins!")
        elif red_score < blue_score:
            print("Player 2 wins!")
            self.gui.set_label('turnTime', "Player 2 wins!")
        else:
            print("Tie!")
            self.gui.set_label('turnTime', "Tie!")
