from time import process_time_ns
import math
s = 0
start = process_time_ns()
for i in range(10000000):
    s += math.sqrt(i)
end = process_time_ns()
print(s)
time = end - start
print(time)