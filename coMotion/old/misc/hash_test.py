from CGALPY_kerEpec_arr2CsExPl_bso2_bv_ch2_pol2_ms2_pp_ss6_tri2.Ker import *
from CGALPY_kerEpec_arr2CsExPl_bso2_bv_ch2_pol2_ms2_pp_ss6_tri2.SS import *
a = Point_d(6, [FT(0), FT(1.52), FT(1), FT(1), FT(1.52), FT(1)])
b = Point_d(6, [FT(-0.0), FT(1.52), FT(1), FT(1), FT(1.52), FT(1)])
#b = Point_d(6, [FT(0)+b[i] for i in range(6)])
print(a)
print(b)
assert (a == b)
assert (hash(a) == hash(b))
