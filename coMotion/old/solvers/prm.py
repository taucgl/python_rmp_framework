import sys
import os.path
from typing import Tuple
sys.path.insert(0, os.path.dirname(__file__))

from functools import cmp_to_key

from solvers.prm_util.bindings import *
import solvers.prm_util.Collision_detection as Collision_detection
import networkx as nx
import random
import time
import math
from gui.gui import GUI
import conversions

BONUS_VALUE = 10

class Edge_distance():
    # The following function returns the transformed distance between two points
    # (for Euclidean distance the transformed distance is the squared distance)
    def transformed_distance(self, p: Point_d, q: Point_d):
        dim = 6
        d = 0
        for i in range(dim // 2):
            p_i = Point_2(p[2 * i], p[2 * i + 1])
            q_i = Point_2(q[2 * i], q[2 * i + 1])
            d += math.sqrt(Ker.squared_distance(p_i, q_i).to_double())
        return FT(d)


def calc_bbox(obstacles):
    X = []
    Y = []
    for poly in obstacles:
        for point in poly.vertices():
            X.append(point.x())
            Y.append(point.y())
    min_x = min(X)
    max_x = max(X)
    min_y = min(Y)
    max_y = max(Y)
    return min_x, max_x, min_y, max_y


def get_point_d(points) -> Point_d:
    dim = 6
    res = []
    for p in points:
        res.append(p.x())
        res.append(p.y())
    for p in range(len(points)*2, dim):
        res.append(FT(0))
    res = Point_d(dim, res)
    return res


def is_valid_position(cd: Collision_detection.Collision_detector, p: Point_d):
    p = Point_2(p[0], p[1])
    # Check intersection with obstacles
    if not cd.is_point_valid(p):
        return False
    return True


def edge_valid(cd: Collision_detection.Collision_detector, p: Point_d, q: Point_d):
    p = Point_2(p[0], p[1])
    q = Point_2(q[0], q[1])
    edge = Segment_2(p, q)
    if not cd.is_edge_valid(edge):
        return False
    return True


def tensor_edge_valid(cd: Collision_detection.Collision_detector, p: Point_d, q: Point_d, number_of_robots: int,
                      radius):
    edges = []
    radius = FT(radius)
    for i in range(number_of_robots):
        p_i = Point_2(p[2 * i], p[2 * i + 1])
        q_i = Point_2(q[2 * i], q[2 * i + 1])
        edges.append(Segment_2(p_i, q_i))
    for edge in edges:
        if not cd.is_edge_valid(edge):
            return False

    # Check intersection between two robots
    for i in range(len(edges)):
        for j in range(i + 1, len(edges)):
            arr = Arrangement_2()
            c = Circle_2(edges[j].source(), FT(4) * radius * radius, Ker.CLOCKWISE)
            Aos2.insert(arr, Curve_2(c))
            v1 = Vector_2(edges[i])
            v2 = Vector_2(edges[j])
            v = v1 - v2
            cv = X_monotone_curve_2(edges[i].source(), edges[i].source() + v)
            if cv.source() != cv.target():
                if Aos2.do_intersect(arr, cv):
                    return False
    return True


def edge_valid_against_opponent(edge, opponent_arrangement):
    edge = Segment_2(Point_2(edge[0][0], edge[0][1]), Point_2(edge[1][0], edge[1][1]))
    return edge.is_degenerate() or not Aos2.do_intersect(opponent_arrangement,
                                                    X_monotone_curve_2(edge.source(), edge.target()))


def tensor_edge_valid_against_opponents(tensor_edge, opponent_arrangement, number_of_robots):
    for i in range(number_of_robots):
        edge = Segment_2(Point_2(tensor_edge[0][i * 2], tensor_edge[0][i * 2 + 1]),
                         Point_2(tensor_edge[1][i * 2], tensor_edge[1][i * 2 + 1]))
        if not edge.is_degenerate() and Aos2.do_intersect(opponent_arrangement,
                                                     X_monotone_curve_2(edge.source(), edge.target())):
            return False
    return True


def find_close_tensor_vertices(p, G: nx.DiGraph, radius):
    res = []
    radius = FT(radius)
    dist = Edge_distance()
    for node in G.nodes:
        if dist.transformed_distance(p, node) < radius * FT(4):
            res.append(node)
    return res


def remove_edges(G: nx.DiGraph, robots, radius, kd_tree, edge_length):
    # remove invalid edges
    ms_squared_radius = FT(4) * radius * radius
    # Check intersection against opponent robots
    arr = Arrangement_2()
    for robot in robots:
        c = Circle_2(robot, ms_squared_radius, Ker.CLOCKWISE)
        Aos2.insert(arr, Curve_2(c))

    edges_to_check = []
    for robot in robots:
        fuzzy_sphere = Fuzzy_sphere(get_point_d([robot]), FT(2) * radius + FT(edge_length), FT(0))
        res = []
        kd_tree.search(fuzzy_sphere, res)
        for p in res:
            edges_to_check.extend([(p, neighbor) for neighbor in G.neighbors(p)])
            edges_to_check.extend([(neighbor, p) for neighbor in G.neighbors(p)])

    removed_edges = []
    for edge in set(edges_to_check):
        if not edge_valid_against_opponent(edge, arr):
            removed_edges.append(edge)
            G.remove_edge(edge[0], edge[1])
    return removed_edges


def remove_edges_naive(G: nx.DiGraph, robots, radius, kd_tree, edge_length):
    # remove invalid edges
    ms_squared_radius = FT(4) * radius * radius
    # Check intersection against opponent robots
    arr = Arrangement_2()
    for robot in robots:
        c = Circle_2(robot, ms_squared_radius, Ker.CLOCKWISE)
        Aos2.insert(arr, Curve_2(c))

    removed_edges = []
    edges_to_check = list(G.edges())
    for edge in set(edges_to_check):
        if not edge_valid_against_opponent(edge, arr):
            removed_edges.append(edge)
            G.remove_edge(edge[0], edge[1])
    return removed_edges


def jamming(team_robots, team_goals, coupons, bonus_points, G, radius, i, j, kd_tree, edge_length):
    res = False
    robot1 = team_robots[i]
    robot2 = team_robots[j]
    radius = FT(radius)
    # too far to cause jam
    if Ker.squared_distance(team_robots[i], team_robots[j]) > radius * radius * FT(16):
        return False
    destinations = [goal for goal in team_goals]
    for i in range(len(bonus_points)):
        if coupons[i][1] > 0:
            destinations.append(bonus_points[i])
    other_robots = [robot for robot in team_robots if robot != robot1 and robot != robot2]
    globally_removed_edges = remove_edges(G, other_robots, radius, kd_tree, edge_length)
    robot1_point_d = get_point_d([robot1])
    robot2_point_d = get_point_d([robot2])
    for destination in destinations:
        # check destination is reachable for both when ignoring each other
        if nx.has_path(G, robot1_point_d, destination) and nx.has_path(G, robot2_point_d, destination):
            removed_edges = remove_edges(G, [robot2], radius, kd_tree, edge_length)
            if not nx.has_path(G, robot1_point_d, destination):
                G.add_edges_from(removed_edges)
                removed_edges = remove_edges(G, [robot1], radius, kd_tree, edge_length)
                if not nx.has_path(G, robot2_point_d, destination):
                    # if we reached here the robots are blocking each other
                    G.add_edges_from(removed_edges)
                    res = True
                    break
                else:
                    G.add_edges_from(removed_edges)
            else:
                G.add_edges_from(removed_edges)
    G.add_edges_from(globally_removed_edges)
    return res


def is_non_jamming_configuration(c, r, number_of_robots):
    for i in range(number_of_robots):
        for j in range(i + 1, number_of_robots):
            p1 = Point_2(c[i * 2], c[i * 2 + 1])
            p2 = Point_2(c[j * 2], c[j * 2 + 1])
            if Ker.squared_distance(p1, p2) < r * r * FT(16):
                return False
    return True


# Find a path to a non jamming configuration for robots i, j
def fix_local_jam(s: Point_d, G: nx.DiGraph, kd_tree: Kd_tree, cd: Collision_detection, opponent_robots, other_robots,
                  radius, i, j):
    # connect s to the graph
    kd_tree.insert(s)
    k = 50
    eps = FT(0)  # 0.0 for exact NN, otherwise approximate NN
    search_nearest = True  # set this value to False in order to search farthest
    sort_neighbors = False  # set this value to True in order to obtain the neighbors sorted by distance
    distance = Edge_distance()
    search = K_neighbor_search(kd_tree, s, k, eps, search_nearest, Euclidean_distance(), sort_neighbors)
    res = []
    search.k_neighbors(res)
    for pair in res:
        neighbor = pair[0]
        if not G.has_edge(s, neighbor):
            # if not ds.connected(p, neighbor):
            # check if we can add an edge to the graph
            if tensor_edge_valid(cd, s, neighbor, 2, radius):
                d = distance.transformed_distance(s, neighbor).to_double()
                G.add_edge(s, neighbor, weight=d)
                G.add_edge(neighbor, s, weight=d)

    opponent_arrangement = Arrangement_2()
    ms_squared_radius = FT(4) * radius * radius
    arr = Arrangement_2()
    for robot in opponent_robots + other_robots:
        c = Circle_2(robot, ms_squared_radius, Ker.CLOCKWISE)
        Aos2.insert(arr, Curve_2(c))

    dist = Edge_distance()
    visited = set()
    queue = [s]
    removed_edges = []
    removed_nodes = []
    visited.update({s})
    c = None
    found_reachable_non_jamming_configuration = False
    while queue:
        c = queue.pop(0)
        if is_non_jamming_configuration(c, FT(radius), 2):
            found_reachable_non_jamming_configuration = True
            break
        for n in list(G.neighbors(c)):
            if n not in visited:
                if dist.transformed_distance(s, n) < FT(6) * radius:  # so we don't go too far
                    if tensor_edge_valid_against_opponents((c, n), opponent_arrangement, 2):
                        queue.append(n)
                        visited.update({n})
                    else:
                        G.remove_edge((n, c))
                        G.remove_edge((c, n))
                        removed_edges.extend([(n, c), (c, n)])
                else:
                    G.remove_node(n)
                    removed_nodes.append(n)

    path = None
    cost = None
    # get path to c
    if found_reachable_non_jamming_configuration:
        temp = []
        path = nx.shortest_path(G, s, c)
        cost = nx.shortest_path_length(G, s, c)
        # account for i,j in the returned path
        if not other_robots:
            other_robots = [Point_2(0, 0)]  # Dummy for last coordinates
        for p in path:
            q = []
            for l in range(3):
                if l == i:
                    q.append(p[0])
                    q.append(p[1])
                elif l == j:
                    q.append(p[2])
                    q.append(p[3])
                else:
                    q.append(other_robots[0].x())
                    q.append(other_robots[0].y())
            temp.append(q)
        path = [Point_d(6, q) for q in temp]

    # restore G
    G.add_nodes_from(removed_nodes)
    G.add_edges_from(removed_edges)
    return path, cost


class DistanceToPointComparator:
    def __init__(self, p: Point_2):
        self.p = p
        self.ed = Euclidean_distance()

    #  return 1 iff p2 is swept before p1

    def compare_points(self, p1: Point_2, p2: Point_2) -> int:
        self.ed: Euclidean_distance
        if self.ed.transformed_distance(self.p, p2) < self.ed.transformed_distance(self.p, p1):
            return 1
        else:
            return -1


def nearest_neighbors_naive(point, neighbors, k):
    distance_to_point = DistanceToPointComparator(point)
    neighbors = sorted(neighbors, key=cmp_to_key(distance_to_point.compare_points))
    return neighbors[:k]


def connect_node(G: nx.DiGraph, p: Point_d, cd, edge_length, distance, k_neighbor_search):
    search = k_neighbor_search
    res = []
    l = edge_length
    search.k_neighbors(res)
    for pair in res:
        neighbor = pair[0]
        if not G.has_edge(p, neighbor):
            # if not ds.connected(p, neighbor):
            # check if we can add an edge to the graph
            if edge_valid(cd, neighbor, p):
                d = math.sqrt(distance.transformed_distance(p, neighbor).to_double())
                if d <= l:
                    G.add_edge(p, neighbor, weight=d)
                    G.add_edge(neighbor, p, weight=d)
                else:  # split long edges
                    lst = [p]
                    for j in range(1, int(d // l)):
                        s = get_point_d([Point_2(p[0] - p[0] * FT(j * (l / d)) + neighbor[0] * FT(j * (l / d)),
                                                 p[1] - p[1] * FT(j * (l / d)) + neighbor[1] * FT(j * (l / d)))])
                        lst.append(s)
                    lst.append(neighbor)
                    G.add_nodes_from(lst)
                    for m in range(len(lst) - 1):
                        s = lst[m]
                        t = lst[m + 1]
                        d = math.sqrt(distance.transformed_distance(s, t).to_double())
                        G.add_edge(s, t, weight=d)
                        G.add_edge(t, s, weight=d)


def build_single_robot_graph(bbox, cd, edge_length, initial_points, k, allotted_time):
    t0 = time.perf_counter()
    x_range = (bbox[0].to_double(), bbox[1].to_double())
    y_range = (bbox[2].to_double(), bbox[3].to_double())
    G = nx.DiGraph()
    tree = Kd_tree(initial_points)
    G.add_nodes_from(initial_points)
    eps = FT(0)  # 0.0 for exact NN, otherwise approximate NN
    search_nearest = True  # set this value to False in order to search farthest
    sort_neighbors = False  # set this value to True in order to obtain the neighbors sorted by distance

    # sample valid points
    i = 0
    while i < k:
        points = []
        rand_x = FT(random.uniform(x_range[0], x_range[1]))
        rand_y = FT(random.uniform(y_range[0], y_range[1]))
        points.append(Point_2(rand_x, rand_y))
        p = get_point_d(points)

        if is_valid_position(cd, p):
            G.add_node(p)
            tree.insert(p)
            i += 1

        elapsed = time.perf_counter() - t0
        remaining_time = allotted_time - elapsed
        if remaining_time < 0.5 * allotted_time:  # leave time to connect edges
            break

    # try connecting close vertices
    k = 12
    i = 0
    l = edge_length
    new_points = []
    tree.points(new_points)
    distance = Euclidean_distance()
    for p in new_points:
        # search = K_neighbor_search(tree, p, k, eps, search_nearest, distance, sort_neighbors)
        search = K_neighbor_search(tree, p, k, eps, search_nearest, Euclidean_distance(), sort_neighbors)
        connect_node(G, p, cd, edge_length, distance, search)
        # if i % 1000 == 0:
        #     print(i)
        i += 1

        elapsed = time.perf_counter() - t0
        remaining_time = allotted_time - elapsed
        if remaining_time < 0.1 * allotted_time:
            break

    return G, tree


def build_tensor_product(bbox, cd: Collision_detection.Collision_detector, radius, k, allotted_time):
    t0 = time.perf_counter()
    eps = FT(0)  # 0.0 for exact NN, otherwise approximate NN
    search_nearest = True  # set this value to False in order to search farthest
    sort_neighbors = False  # set this value to True in order to obtain the neighbors sorted by distance
    G = nx.DiGraph()
    ed = Euclidean_distance()
    first_x_range = (bbox[0].to_double(), bbox[1].to_double())
    first_y_range = (bbox[2].to_double(), bbox[3].to_double())
    radius = radius.to_double()
    i = 0

    # sample close valid pairs
    while i < k:
        first_rand_x = random.uniform(first_x_range[0], first_x_range[1])
        first_rand_y = random.uniform(first_y_range[0], first_y_range[1])
        p1 = get_point_d([Point_2(first_rand_x, first_rand_y)])
        if is_valid_position(cd, p1):
            second_x_range = (
                max(first_rand_x - 6 * radius, first_x_range[0]), min(first_rand_x + 6 * radius, first_x_range[1]))
            second_y_range = (
                max(first_rand_y - 6 * radius, first_y_range[0]), min(first_rand_y + 6 * radius, first_y_range[1]))
            for j in range(5):
                second_rand_x = random.uniform(second_x_range[0], second_x_range[1])
                second_rand_y = random.uniform(second_y_range[0], second_y_range[1])
                p2 = get_point_d([Point_2(second_rand_x, second_rand_y)])
                if is_valid_position(cd, p2) and ed.transformed_distance(p1, p2) > FT(2) * FT(radius):
                    G.add_node(Point_d(6, [p1[0], p1[1], p2[0], p2[1], FT(0), FT(0)]))
                    i += 1

        elapsed = time.perf_counter() - t0
        remaining_time = allotted_time - elapsed
        if remaining_time < 0.5 * allotted_time:
            break

    # try connecting close vertices
    tree = Kd_tree(list(G.nodes))
    k = 15
    i = 0
    new_points = []
    tree.points(new_points)
    distance = Edge_distance()
    # print(new_points)
    for p in new_points:
        # search = SS.K_neighbor_search(tree, p, k, eps, search_nearest, distance, sort_neighbors)
        search = SS.K_neighbor_search(tree, p, k, eps, search_nearest, SS.Euclidean_distance(), sort_neighbors)
        res = []
        search.k_neighbors(res)
        for pair in res:
            neighbor = pair[0]
            if not G.has_edge(p, neighbor):
                # if not ds.connected(p, neighbor):
                # check if we can add an edge to the graph
                if tensor_edge_valid(cd, neighbor, p, 2, radius):
                    d = distance.transformed_distance(p, neighbor).to_double()
                    G.add_edge(p, neighbor, weight=d)
                    G.add_edge(neighbor, p, weight=d)
        # i += 1
        # if i % 1000 == 0: print(i)
        elapsed = time.perf_counter() - t0
        remaining_time = allotted_time - elapsed
        if remaining_time < 0.1 * allotted_time:
            break

    return G, tree


def find_valid_bonus_point(bonus: Point_2, cd: Collision_detection.Collision_detector, radius: FT, N):
    bonus_center = bonus
    # approximate circle
    v_base = Vector_2(radius, FT(0))
    angle = 0
    step = 2 * math.pi / N
    for i in range(N):
        s = math.sin(angle)
        c = math.cos(angle)
        t = Aff_transformation_2(Rotation(), Direction_2(c, s), FT(1), FT(1000))
        v = t.transform(v_base)
        angle += step
        p = bonus_center + v
        if cd.is_point_valid(p):
            return p
    return None


# todo adjust for time
def initialize(params, isRunning):
    if SS.get_spatial_searching_dimension() < 6:
        print("Please supply python bindings with spatial searching dimension of at least 6")
        return
    t0 = time.perf_counter()
    radius = FT(params[0])
    t_turn = params[1]
    t_preprocess = params[2]
    t_total = params[3]
    max_distance = params[4]
    team_start_positions = params[5]
    opponent_start_positions = params[6]
    team_goals = params[7]
    goal_radii = params[8]
    obstacles = params[9]
    coupons = params[10]
    data = params[11]
    number_of_robots = len(team_start_positions)
    data.extend(params[:10])

    bbox = calc_bbox(obstacles)
    cd = Collision_detection.Collision_detector(obstacles, FT(radius))

    bonus_points = [find_valid_bonus_point(bonus, cd, radius, 6) for bonus in coupons]

    initial_points = [get_point_d([p]) for p in team_start_positions]
    initial_points.extend([get_point_d([p]) for p in team_goals])
    initial_points.extend([get_point_d([p]) for p in bonus_points if p is not None])
    l = min(4 * radius.to_double(), max_distance)

    elapsed = time.perf_counter() - t0
    remaining_time = t_preprocess - elapsed

    c = 1500
    k = c * t_turn // (number_of_robots ** 2)
    print("k:", k)
    G, tree = build_single_robot_graph(bbox, cd, l, initial_points, k, remaining_time)

    elapsed = time.perf_counter() - t0
    remaining_time = t_preprocess - elapsed

    TG, TG_tree = None, None
    print("1")
    if number_of_robots >= 2:
        TG, TG_tree = build_tensor_product(bbox, cd, radius, k, remaining_time)
    print("2")

    total_coupons_value = sum([BONUS_VALUE for coupon in coupons])

    print("number of vertices in the graph:", G.number_of_nodes())
    print("number of edges in the graph:", G.number_of_edges())

    priority_goals = [None, None, None]

    data.append(priority_goals)
    data.append(total_coupons_value)
    data.append([])
    data.append(max_distance)
    data.append(t_turn)
    data.append(cd)
    data.append(TG)
    data.append(TG_tree)
    data.append(radius)
    data.append(l)
    data.append(G)
    data.append(tree)
    data.append([get_point_d([p]) for p in team_start_positions])
    data.append([get_point_d([p]) for p in team_goals])
    data.append([get_point_d([p]) if p is not None else None for p in bonus_points])


# Determine if goals should be reassigned and return a path for the two robots to reach their new goals (i - blocking
# robot index, j - blocked robot index)
def reassign_goals(G, occupied_goal, desired_goal, blocked_robot, other_robots, i, j, radius, kd_tree, edge_length) -> Tuple[list, float]:
    t0 = time.perf_counter()
    cost = 0
    path = []
    globally_removed_edges = remove_edges(G, other_robots, radius, kd_tree, edge_length)
    # Check new goal is reachable from occupied goal
    removed_edges = []
    removed_edges = remove_edges(G, [Point_2(blocked_robot[0], blocked_robot[1])], radius, kd_tree, edge_length)
    if nx.has_path(G, occupied_goal, desired_goal):
        path1 = nx.shortest_path(G, occupied_goal, desired_goal)
        cost = nx.shortest_path_length(G, occupied_goal, desired_goal)
        G.add_edges_from(removed_edges)
        # Check old goal is now reachable from current robot location
        removed_edges = remove_edges(G, [Point_2(desired_goal[0], desired_goal[1])], radius, kd_tree, edge_length)
        if nx.has_path(G, blocked_robot, occupied_goal):
            path = path1
            # path2 = nx.shortest_path(G, blocked_robot, occupied_goal)
            # cost += nx.shortest_path_length(G, blocked_robot, occupied_goal)
            # # account for i,j in the returned path
            # if not other_robots:
            #     other_robots = [Point_2(0, 0)]  # Dummy for last coordinates
            # temp = []
            # print("path1", path1)
            # for p in path1:
            #     q = []
            #     for l in range(3):
            #         if l == i:
            #             q.append(p[0])
            #             q.append(p[1])
            #         elif l == j:
            #             q.append(blocked_robot[0])
            #             q.append(blocked_robot[1])
            #         else:
            #             q.append(other_robots[0].x())
            #             q.append(other_robots[0].y())
            #     temp.append(q)
            # path.extend([Point_d(6, q) for q in temp])
            # temp = []
            # for p in path2:
            #     q = []
            #     for l in range(3):
            #         if l == j:
            #             q.append(p[0])
            #             q.append(p[1])
            #         elif l == j:
            #             q.append(desired_goal.x())
            #             q.append(desired_goal.y())
            #         else:
            #             q.append(other_robots[0].x())
            #             q.append(other_robots[0].y())
            #     temp.append(q)
            # path.extend([Point_d(6, q) for q in temp])

    # restore G
    G.add_edges_from(globally_removed_edges)
    G.add_edges_from(removed_edges)
    t1 = time.perf_counter()
    # print("reassign attempt took: " + str(t1 - t0) + "seconds")
    return path, cost


# todo complete:
# Returns the cost of the step
def get_next_path(path, team_robots, opponent_robots, coupons, data, cd, TG, TG_tree, radius, edge_length, G, kd_tree,
                  team_goals, bonus_points, allotted_time, go_for_goals, priority_goals) -> float:
    t0 = time.perf_counter()
    # if there's enough time...
    if allotted_time < 2:
        return 0.0

    # if there's nothing left to do
    if go_for_goals and all(get_point_d([robot]) in team_goals for robot in team_robots):
        return 0.0

    search_nearest = True
    sort_neighbors = False
    for robot in team_robots:
        p = get_point_d([robot])
        if p not in G.nodes:
            G.add_node(p)
            search = SS.K_neighbor_search(kd_tree, p, 50, FT(0), search_nearest, SS.Euclidean_distance(), sort_neighbors)
            connect_node(G, p, cd, edge_length, Edge_distance(), search)
            kd_tree.insert(p)

    number_of_robots = len(team_robots)
    # check for jams
    # for i in range(number_of_robots):
    #     for j in range(i + 1, number_of_robots):
    #         if jamming(team_robots, team_goals, coupons, bonus_points, G, radius, i, j, kd_tree, edge_length):
    #             print("jam found")
    #             other_robots = [robot for robot in team_robots if robot != team_robots[i] and robot != team_robots[j]]
    #             s = get_point_d([team_robots[i], team_robots[j]])
    #             fix_jam_path, cost = fix_local_jam(s, TG, TG_tree, cd, opponent_robots, other_robots, radius, i, j)
    #             if not fix_jam_path:  # try reversing the roles
    #                 print("reversing roles")
    #                 fix_jam_path, cost = fix_local_jam(s, TG, TG_tree, cd, opponent_robots, other_robots, radius, j, i)
    #             if fix_jam_path:  # todo: choose whether to fix the jam
    #                 print("clearing jam")
    #                 for p in fix_jam_path:
    #                     q = []
    #                     for k in range(number_of_robots):
    #                         q.append(Point_2(p[2 * k], p[2 * k + 1]))
    #                     path.append(q)
    #                 return cost
    #         elapsed = time.perf_counter() - t0
    #         remaining_time = allotted_time - elapsed
    #         if remaining_time < 1.0:
    #             return 0.0

    # get paths to coupons and goals
    paths = [{} for i in range(number_of_robots)]
    elapsed = time.perf_counter() - t0
    remaining_time = allotted_time - elapsed
    if remaining_time >= number_of_robots * 1.0:
        for i in random.sample(range(number_of_robots), number_of_robots):
            robot = team_robots[i]
            removed_edges = []
            removed_edges = remove_edges(G, [team_robot for team_robot in team_robots if team_robot != robot], radius,
                                         kd_tree, edge_length)
            robot = get_point_d([robot])
            if not go_for_goals:
                for j, bonus in enumerate(bonus_points):
                    # if coupon was not collected and we found a valid point for pickup
                    if not coupons[j] and bonus is not None:
                        score = BONUS_VALUE
                        if nx.has_path(G, robot, bonus) and robot != bonus:
                            cost = nx.shortest_path_length(G, robot, bonus)
                            score /= cost
                            paths[i][bonus] = [nx.shortest_path(G, robot, bonus), score, i, cost]
                    elapsed = time.perf_counter() - t0
                    remaining_time = allotted_time - elapsed
                    if remaining_time < number_of_robots * 1.0:
                        G.add_edges_from(removed_edges)
                        break

            # todo: get closer to a goal
            # if not has_path_to_goal:
            #     for goal in team_goals:
            #         score = -1
            #         reachable = nx.descendants(G, source=robot)
            #         nn = nearest_neighbors_naive(goal, reachable, 5)  # todo: euclidean distance, clear shortest path distance would be better
            #         if nn:
            #             neighbor = nn[0]  # todo choose better?
            #             cost = nx.shortest_path_length(G, robot, neighbor)
            #             score *= cost
            #             paths[i][goal] = [nx.shortest_path(G, robot, neighbor), score, i, cost]

            # try reaching destinations if there are no paths to coupons
            if not paths[i]:
                has_path_to_goal = False
                if robot not in team_goals:
                    for goal in team_goals:
                        score = 0  # todo adjust score based on how much the situation improved
                        if nx.has_path(G, robot, goal):
                            if priority_goals[i] == goal:
                                score = 1
                            else:
                                print(goal, priority_goals[i])
                            cost = nx.shortest_path_length(G, robot, goal)
                            paths[i][goal] = [nx.shortest_path(G, robot, goal), score, i, cost]
                            has_path_to_goal = True
                # Try reassigning goals
                if not has_path_to_goal:
                    G.add_edges_from(removed_edges)
                    for goal in team_goals:
                        for j, blocking_robot in enumerate(team_robots):
                            blocking_robot_d = get_point_d([blocking_robot])
                            if i != j and blocking_robot_d in team_goals and blocking_robot_d != goal:
                                other_robots = [other for other in team_robots if
                                                other != team_robots[i] and other != blocking_robot]
                                reassign_path, cost = reassign_goals(G, blocking_robot_d, goal, robot, other_robots, j,
                                                                     i, radius, kd_tree, edge_length)
                                if reassign_path:
                                    score = -1
                                    if robot in team_goals:  # for cases where two robots on goals are blocking the third
                                        score = -math.inf
                                    paths[j][goal] = [reassign_path, score, j, cost]
                                    break
                        if remaining_time < number_of_robots * 1.0:
                            break

            G.add_edges_from(removed_edges)
            elapsed = time.perf_counter() - t0
            remaining_time = allotted_time - elapsed
            if remaining_time < number_of_robots * 1.0:
                break

    # the best paths for each robot
    best_paths = []
    for d in paths:
        if d:
            best_paths_for_robot = [d[bonus] for bonus in d]
            random.shuffle(best_paths_for_robot)
            best_paths.append(max(best_paths_for_robot, key=lambda x: x[1]))

    if best_paths:
        random.shuffle(best_paths)
        best = max(best_paths, key=lambda x: x[1])  # best overall path
        for p in best[0]:  # the path itself
            s = []
            for i in range(number_of_robots):
                if i != best[2]:  # for matching the robot's index
                    s.append(team_robots[i])
                else:
                    s.append(Point_2(p[0], p[1]))
            path.append(s)
        priority_goals[best[2]] = best[0][-1]  # set goal to have higher priority for this robot in the next iteration
        print(path)
        return best[3]  # cost
    else:
        print("no good paths were found")
        return 0.0


def update_bonus_state(path, bonuses, expanded_bonuses):
    if not path:
        return
    collected = 0
    for i in range(len(path) - 1):
        for j in range(len(path[0])):
            edge = Segment_2(path[i][j], path[i + 1][j])
            for expanded_bonus in expanded_bonuses:
                bonus = bonuses[expanded_bonus[1]]
                if not bonus[1] == 0:
                    if not expanded_bonus[0].is_edge_valid(edge):
                        collected += bonus[1]
                        bonus[1] = 0
    return collected


def play_turn(params, isRunning):
    if SS.get_spatial_searching_dimension() < 6:
        print("Please supply python bindings with spatial searching dimension of at least 6")
        return
    t0 = time.perf_counter()
    path = params[0]
    team_status = params[1]
    opponent_status = params[2]
    collected = params[3]
    data = params[4]
    priority_goals = data[-15]
    total_coupons_value = data[-14]
    expanded_bonuses = data[-13]
    remaining_distance = data[-12]
    turn_time = data[-11]
    cd = data[-10]
    TG = data[-9]
    TG_tree = data[-8]
    radius = data[-7]
    edge_length = data[-6]
    G: nx.DiGraph = data[-5]
    kd_tree = data[-4]
    team_start_positions = data[-3]
    team_goals = data[-2]
    bonus_points = data[-1]

    number_of_robots = len(team_status)

    team_robots = [robot[0] for robot in team_status]
    current_score = sum([robot[1] for robot in team_status])
    opponent_robots = [robot[0] for robot in opponent_status]

    globally_removed_edges = remove_edges(G, opponent_robots, radius, kd_tree, edge_length)
    # print("removed edges:", len(globally_removed_edges))
    # globally_removed_edges = remove_edges_naive(G, opponent_robots, radius, kd_tree, edge_length)

    t1 = time.perf_counter()
    elapsed = t1 - t0
    remaining_time = turn_time - elapsed
    path_length = len(path)
    go_for_goals = False
    last_turn_time = 0.0
    while remaining_distance > 0.0:
        t0 = time.perf_counter()
        if current_score >= 0.5 * total_coupons_value:
            go_for_goals = True
        cost = get_next_path(path, team_robots, opponent_robots, collected, data, cd, TG, TG_tree, radius, edge_length, G,
                             kd_tree,
                             team_goals, bonus_points, remaining_time - 0.5, go_for_goals, priority_goals)
        if cost <= 0:  # todo: try using remaining time for something useful
            break
        remaining_distance -= cost
        if path:
            team_robots = path[-1]
            current_score += update_bonus_state(path[path_length:], collected, expanded_bonuses)
        path_length = len(path)
        t1 = time.perf_counter()
        last_turn_time = t1 - t0
        remaining_time -= last_turn_time

    G.add_edges_from(globally_removed_edges)
