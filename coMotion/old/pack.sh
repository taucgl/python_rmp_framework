#!/bin/sh
project=game
rm -r ./package
mkdir package
mkdir package/$project
cp -r ../gui package/$project/gui

cp readme.md package/$project/readme.md
cp bindings.py package/$project/bindings.py
cp $project.py package/$project/$project.py
cp approximate_cspace.py package/$project/approximate_cspace.py
cp Collision_detection.py package/$project/Collision_detection.py
cp conversions.py package/$project/conversions.py
cp copy_list.py package/$project/copy_list.py
cp gui_mp_workshop.py package/$project/gui_mp_workshop.py
cp motion_planner.py package/$project/motion_planner.py
cp read_input.py package/$project/read_input.py
cp Scene.py package/$project/Scene.py
cp -r scenes package/$project/scenes
cp -r solvers package/$project/solvers


curl https://www.cs.tau.ac.il/~cgl/python_rmp/libgmp-10.dll -o package/$project/libgmp-10.dll
curl https://www.cs.tau.ac.il/~cgl/python_rmp/libmpfr-4.dll -o package/$project/libmpfr-4.dll
curl https://www.cs.tau.ac.il/~cgl/python_rmp/CGALPY_kerFsclgInt_kerdCdlgDynamic_aos2CsExPl_bso2_bv_ch2_pol2_ms2_pp_ss8_tri2PlainNci.pyd -o package/$project/CGALPY_kerFsclgInt_kerdCdlgDynamic_aos2CsExPl_bso2_bv_ch2_pol2_ms2_pp_ss8_tri2PlainNci.pyd
