"""
This is a minimal example that demonstrates usage of all different Qt GUI objects
that are available on this framework.
"""

import json
import os
import sys

from PyQt5 import QtWidgets, QtCore
from coMotion.game.comotion_player import CoMotion_RandomPRMPlayer, CoMotion_ScriptReaderPlayer
from coMotion.read_input import read_scritps

from bindings import *
from gui.gui import GUI
from read_input import read_scene
from game.comotion_game import CoMotion_Game
from game.comotion_player import CoMotion_Robot, CoMotion_Player
from game_dspgl import CoMotion_DiscoPygal

RUN_FROM_SCRIPT = True

class GUITest(GUI):
    def setupUi(self):
        #########################
        # Setup the Qt UI layout
        #########################
        # Resize the main window and set the stylesheet (CSS)
        MainWindow = self.mainWindow
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        MainWindow.setStyleSheet("QMainWindow { background-color : rgb(54, 57, 63); color : rgb(220, 221, 222); }\n"
                                 "QLabel { background-color : rgb(54, 57, 63); color : rgb(220, 221, 222); }")

        # Add a graphics widget to the main window
        self.graphicsView = QtWidgets.QGraphicsView(self.mainWindow)
        self.graphicsView.setEnabled(True)
        self.graphicsView.setGeometry(QtCore.QRect(10, 10, 780, 580))
        self.graphicsView.setObjectName("graphicsView")

        # Zoom out a bit
        self.zoom = 30


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    gui = GUITest()
    gui.set_program_name("CoMotion Playground")
    gui.set_animation_finished_action(lambda: None)

    ##################
    # Load the scene
    ##################
    config = read_scene('comotion/scenes/clutter_multi.json')
    first_player_script, second_player_script = read_scritps('comotion/scenes/scripts/history.json')

    # P1
    if RUN_FROM_SCRIPT:
        first_player = CoMotion_ScriptReaderPlayer(first_player_script)
    else:
        first_player = CoMotion_RandomPRMPlayer(1000, 15)
    first_player_robots = []
    for robot_location in config['red_robots']:
        first_player_robots.append(CoMotion_Robot(robot_location, config['radius'], first_player))
    first_player.add_robots(first_player_robots)

    # P2
    if RUN_FROM_SCRIPT:
        second_player = CoMotion_ScriptReaderPlayer(second_player_script)
    else:
        second_player = CoMotion_RandomPRMPlayer(1000, 15)
    
    second_player_robots = []
    for robot_location in config['blue_robots']:
        second_player_robots.append(CoMotion_Robot(robot_location, config['radius'], second_player))
    second_player.add_robots(second_player_robots)

    # Init game + gui
    game = CoMotion_Game(config, first_player, second_player)
    first_player.attach_game(game)
    second_player.attach_game(game)
    game_dspgl = CoMotion_DiscoPygal(gui)
    game_dspgl.connect_game(game)

    # Preprocess both players
    first_player.preprocess()
    second_player.preprocess()
    
    # Animate robots doing stuff
    first_player_history = []
    second_player_history = []
    current_history, other_history = first_player_history, second_player_history
    for _ in range(20):
        paths, events = game.play_turn()
        print(events)
        print('p1: {}, p2: {}'.format(first_player.score, second_player.score))
        game_dspgl.animate_robot_paths(paths, events)
        
        # store player history
        d = {}
        for i, robot in enumerate(game.other_player.robots):
            if robot not in paths:
                continue
            d[str(i)] = [[paths[robot][0].source().x().to_double(), paths[robot][0].source().y().to_double()]] # add first vertex
            for edge in paths[robot]:
               d[str(i)].append([edge.target().x().to_double(), edge.target().y().to_double()])
            
        current_history.append(d)
        current_history, other_history = other_history, current_history
    
    if not RUN_FROM_SCRIPT:
        history = {
            'first_player': first_player_history,
            'second_player': second_player_history
        }
        with open('coMotion/scenes/scripts/history.json', 'w') as fp:
            json.dump(history, fp)

    gui.mainWindow.show()
    sys.exit(app.exec_())
