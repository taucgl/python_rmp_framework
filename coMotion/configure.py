import sys
import json

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMainWindow, QFileDialog

from configure_gui import Ui_MainWindow

class ConfigureGUI(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()

        self.setupUi(self)

        # Connect both browse buttons
        self.loadConfigBrowse.clicked.connect(lambda: self.set_load_configuration())
        self.sceneBrowse.clicked.connect(lambda: self.set_scene())

        # Load and save configurations
        self.loadConfigButton.clicked.connect(lambda: self.load_configuration())
        self.saveConfigButton.clicked.connect(lambda: self.save_configuration())
        
    def load_configuration(self):
        config_path = self.loadConfigLine.text()
        with open(config_path, 'r') as fp:
            config = json.load(fp)
        
        self.sceneEdit.setText(config['scene'])
        self.firstModuleEdit.setText(config['first_module'])
        self.firstClassnameEdit.setText(config['first_class'])
        self.firstArgsEdit.setText(config['first_args'])
        self.secondModuleEdit.setText(config['second_module'])
        self.secondClassnameEdit.setText(config['second_class'])
        self.secondArgsEdit.setText(config['second_args'])
    
    def save_configuration(self):
        config = {
            'scene': self.sceneEdit.text(),
            'first_module': self.firstModuleEdit.text(),
            'first_class': self.firstClassnameEdit.text(),
            'first_args': self.firstArgsEdit.text(),
            'second_module': self.secondModuleEdit.text(),
            'second_class': self.secondClassnameEdit.text(),
            'second_args': self.secondArgsEdit.text(),
        }

        msg = QtWidgets.QMessageBox()
        msg.setWindowTitle("Save game history")

        try:
            config_path = self.saveConfigEdit.text()
            with open(config_path, 'w') as fp:
                json.dump(config, fp)

            msg.setText("Configuration saved successfuly")
            msg.setInformativeText("Path: " + config_path)
            msg.setIcon(QtWidgets.QMessageBox.Information)
        except Exception as e:
            msg.setText("Failed saving configuration:")
            msg.setInformativeText(repr(e))
            msg.setIcon(QtWidgets.QMessageBox.Critical)
        
        msg.exec_()

        

    def get_file(self):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.AnyFile)
        dlg.setDirectory('')
        if dlg.exec_():
            filenames = dlg.selectedFiles()
            return filenames[0]
    
    def set_load_configuration(self):
        filename = self.get_file()
        if filename:
            self.loadConfigLine.setText(filename)
    
    def set_scene(self):
        filename = self.get_file()
        if filename:
            self.sceneEdit.setText(filename)

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    gui = ConfigureGUI()
    gui.show()
    sys.exit(app.exec_())