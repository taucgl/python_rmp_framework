import sys
import json
import time
import importlib

from numpy import e

from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import QFileDialog

from game_gui import GameGUI
from bindings import *
from read_input import *
from gui.logger import Writer
from game.comotion_game import CoMotion_Game
from game.comotion_player import CoMotion_Robot, CoMotion_ScriptReaderPlayer, CoMotion_RandomPRMPlayer
from game.comotion_events import COMOTION_EVENT_NAMES
from game_dspgl import CoMotion_DiscoPygal

game = None
events = []
game_dspgl = None
writer = None
should_step = True

# Global history variables
first_player_history = []
second_player_history = []
current_history, other_history = first_player_history, second_player_history

DASH_STR = '------------------------------------'

def setup_logic(gui):
    gui.set_logic('start', lambda: start_game(gui))
    gui.set_logic('step', lambda: step(gui))
    gui.set_logic('save', lambda: save_game_history(gui))
    gui.set_logic('load_config', lambda: get_config_file(gui))
    gui.set_animation_finished_action(lambda: animation_finished(gui))
    gui.pushButtons['save'].setEnabled(False)

def get_file():
    dlg = QFileDialog()
    dlg.setFileMode(QFileDialog.AnyFile)
    dlg.setDirectory('')
    if dlg.exec_():
        filenames = dlg.selectedFiles()
        return filenames[0]

def get_config_file(gui):
    file_path = get_file()
    if file_path:
        gui.set_field('config', file_path)

def animation_finished(gui):
    global should_step

    if should_step:
        gui.pushButtons['step'].setEnabled(True)
        update_labels(gui)
    else:
        update_labels(gui)
        step(gui)

def save_game_history(gui):
    global first_player_history
    global second_player_history
    
    history = {
            'first_player': first_player_history,
            'second_player': second_player_history
        }
    
    msg = QtWidgets.QMessageBox()
    msg.setWindowTitle("Save game history")

    try:
        with open(gui.get_field('history'), 'w') as fp:
            json.dump(history, fp)
        msg.setText("History saved successfuly")
        msg.setInformativeText("Path: " + gui.get_field('history'))
        msg.setIcon(QtWidgets.QMessageBox.Information)
    except Exception as e:
        msg.setText("Failed saving history:")
        msg.setInformativeText(repr(e))
        msg.setIcon(QtWidgets.QMessageBox.Critical)
    
    msg.exec_()


def check_winner(gui):
    global game
    global writer

    winner = game.check_winner()
    if winner > 0:
        msg = "Player {} won!".format(winner)
        gui.set_label('state', msg)
        print(msg, file=writer)
    else:
        msg = "Game ended with draw!"
        gui.set_label('state', msg)
        print(msg, file=writer)
    
    print(DASH_STR, file=writer)
    print("Press 'Start Game' to start a new game...", file=writer)

    gui.pushButtons['step'].setEnabled(False)
    gui.pushButtons['start'].setEnabled(True)
    gui.pushButtons['save'].setEnabled(True)


def update_labels(gui):
    global events
    global game
    global game_dspgl
    global writer
    global should_step
    gui.set_label('p1', "Player 1 (Red) Score: {}".format(game.first_player.score))
    gui.set_label('p2', "Player 2 (Blue) Score: {}".format(game.second_player.score))
    gui.set_label('state', "Turn: {} ({})".format(game.turn // 2 + 1, "Red" if game.turn % 2 == 0 else "Blue"))

    # Also update the halos of the robots (after they are done with their animation)
    game_dspgl.update_robot_halos(events)

    # Also print the current events
    print(DASH_STR, file=writer)
    if game.turn == 0:
        return
    print("### Turn: {} ({}) ###".format((game.turn-1) // 2 + 1, "Red" if (game.turn-1) % 2 == 0 else "Blue"), file=writer)
    event_types = {} # count for each type how many occurences
    for event_list in events:
        for event in event_list:
            if event.event_type not in event_types:
                event_types[event.event_type] = 0
            event_types[event.event_type] += 1
    for event in event_types:
        print("   * {} - {}".format(COMOTION_EVENT_NAMES[event], event_types[event]), file=writer)
    print(events)
    

def step(gui):
    global events
    global game
    global game_dspgl
    global writer
    global should_step
    global current_history
    global other_history
    
    if game.turn == game.max_turns * 2:
        check_winner(gui)
        should_step = True
        return

    paths, events = game.play_turn()
    if len(paths) > 0:
        gui.pushButtons['step'].setEnabled(False)
        game_dspgl.animate_robot_paths(paths, events)
    else:
        animation_finished(gui)

    # store player history
    d = {}
    for i, robot in enumerate(game.other_player.robots):
        if robot not in paths:
            continue
        if len(paths[robot]) == 0:
            continue
        d[str(i)] = [[paths[robot][0].source().x().to_double(), paths[robot][0].source().y().to_double()]] # add first vertex
        for edge in paths[robot]:
            d[str(i)].append([edge.target().x().to_double(), edge.target().y().to_double()])
        
    current_history.append(d)
    current_history, other_history = other_history, current_history


def start_game(gui):
    global game
    global game_dspgl
    global writer
    global events
    global should_step
    global first_player_history
    global second_player_history
    global current_history
    global other_history

    # Clear the scene from previous clutter
    gui.clear_scene()

    # Clear the event queue (when running second games)
    events = []

    # Reset game history
    first_player_history = []
    second_player_history = []
    current_history, other_history = first_player_history, second_player_history

    # You cannot save until the game is over
    gui.pushButtons['save'].setEnabled(False)
    
    # Start with loading the config file
    config_file = gui.get_field('config')
    with open(config_file, 'r') as fp:
        config = json.load(fp)
    
    # Load scene configuration
    scene_config = read_scene(config['scene'])
    
    # Setup the game
    print("Loading and preparing the game...", file=writer)
    first_player_module = importlib.import_module(config['first_module'])
    first_player = getattr(first_player_module, config['first_class'])(0, *config['first_args'].split(','))
    first_player_robots = []
    for robot_location in scene_config['red_robots']:
        first_player_robots.append(CoMotion_Robot(robot_location, scene_config['radius'], first_player))
    first_player.add_robots(first_player_robots)

    second_player_module = importlib.import_module(config['second_module'])
    second_player = getattr(second_player_module, config['second_class'])(1, *config['second_args'].split(','))
    second_player_robots = []
    for robot_location in scene_config['blue_robots']:
        second_player_robots.append(CoMotion_Robot(robot_location, scene_config['radius'], second_player))
    second_player.add_robots(second_player_robots)

    game = CoMotion_Game(scene_config, first_player, second_player)
    first_player.attach_game(game)
    second_player.attach_game(game)

    game_dspgl = CoMotion_DiscoPygal(gui)
    game_dspgl.connect_game(game)

    # Preprocess the players
    print(DASH_STR, file=writer)
    print("Preprocessing first player...", file=writer)
    t0 = time.time()
    first_player.preprocess()
    t1 = time.time()
    print("Finished preprocessing first player. Time took: {} [sec]".format(t1-t0), file=writer)

    print("Preprocessing second player...", file=writer)
    t0 = time.time()
    second_player.preprocess()
    t1 = time.time()
    print("Finished preprocessing second player. Time took: {} [sec]".format(t1-t0), file=writer)

    gui.pushButtons['start'].setEnabled(False)
    gui.pushButtons['step'].setEnabled(True)
    should_step = gui.stepRadio.isChecked()
    
    if should_step:
        gui.pushButtons['step'].setText('Step')
    else:
        gui.pushButtons['step'].setText('Animate')

    update_labels(gui)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    gui = GameGUI()
    gui.set_program_name("CoMotion - DiscoPygal GUI")

    # setup the log writer
    writer = Writer(gui.logEdit)

    if len(sys.argv) > 1:
        config_file = sys.argv[1]
        gui.set_field('config', config_file)
    if len(sys.argv) > 2:
        history_file = sys.argv[2]
        gui.set_field('history', history_file)

    setup_logic(gui)

    gui.mainWindow.show()
    sys.exit(app.exec_())