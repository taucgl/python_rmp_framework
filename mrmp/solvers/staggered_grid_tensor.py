import sys
import os.path
sys.path.insert(0, os.path.dirname(__file__))

import time
import random
import heapq
from geometry_utils import collision_detection
from bindings import CGALPY
SS = CGALPY.SS
Ker = CGALPY.Ker
import gc
from math import sqrt, ceil
import bounding_box
import networkx as nx
import conversions
import json


class NeighborsFinder:
    def __init__(self, points):
        self.tree = SS.Kd_tree()
        self.tree.insert(points)

    def neighbors_in_radius(self, query, r):
        epsilon = Ker.FT(0)
        a = SS.Fuzzy_sphere(query, r, epsilon)
        res = []
        self.tree.search(a, res)
        return res


class Config(object):
    __instance = None

    def __new__(cls):
        if Config.__instance is None:
            Config.__instance = object.__new__(cls)
        return Config.__instance

    def __init__(self,):
        self.eps = 9999
        self.delta = 0.04  # Clearance after the environment is shrunken to [0,1]^2
        self.is_multi_robot = True
        self.sample_method = "staggered_grid"
        self.reset()

    def reset(self):
        alpha = self.eps / sqrt(1 + self.eps ** 2)
        y = self.eps/(2*(2+self.eps))
        self.edge_len = 1 - 2 * self.delta
        if self.is_multi_robot:
            # These may change as we modify bounds in the paper
            self.ball_radius = y * self.delta
            # self.connection_radius = Ker.FT(self.delta)*(Ker.FT(1+self.eps)/Ker.FT(2+self.eps))
            self.connection_radius = Ker.FT(self.delta)*(Ker.FT(1+self.eps)/Ker.FT(2+self.eps))*Ker.FT(1.0001)
            # self.connection_radius = Ker.FT(self.delta*1.001)
        else:
            self.ball_radius = alpha * self.delta
            self.connection_radius = Ker.FT(2*(alpha+sqrt(1-alpha**2))*self.delta)*Ker.FT(1.0001)
        unrounded_balls_per_dim = self.edge_len / (2 * self.ball_radius)
        print("unrounded number of balls:", unrounded_balls_per_dim ** 2 + (unrounded_balls_per_dim + 1) ** 2)
        self.balls_per_dim = ceil(unrounded_balls_per_dim)
        self.num_of_sample_points = self.balls_per_dim ** 2 + (self.balls_per_dim + 1) ** 2
        print("real number of balls:", self.num_of_sample_points)
        self.grid_points_per_dim = ceil(sqrt(self.num_of_sample_points))
        print("real number of grid points:", self.grid_points_per_dim**2)


def point_d_to_point_2(x):
    return Ker.Point_2(x[0], x[1])


class PrmNode:
    def __init__(self, pt, name):
        self.point = SS.Point_d(2, [pt.x(), pt.y()])
        self.point_2 = pt
        self.name = name
        self.out_connections = {}
        self.in_connections = {}


class PrmEdge:
    def __init__(self, src, dest, eid):
        self.src = src
        self.dest = dest
        self.name = "e"+str(eid)
        self.segment = Ker.Segment_2(src.point_2, dest.point_2)
        self.cost = sqrt(Ker.squared_distance(src.point_2, dest.point_2).to_double())


class PrmGraph:
    def __init__(self, milestones, writer):
        self.edge_id = 0
        self.points_to_nodes = {}
        self.edges = []
        for milestone in milestones:
            if milestone in self.points_to_nodes:
                print("Error, re entry of:", milestone.point, "in", milestone, file=writer)
            self.points_to_nodes[milestone.point] = milestone

    def insert_edge(self, milestone, neighbor):
        neighbor_node = self.points_to_nodes[neighbor]
        if neighbor_node == milestone:
            return
        if neighbor_node in milestone.out_connections or neighbor_node in milestone.in_connections:
            return
        self.edges.append(PrmEdge(milestone, neighbor_node, self.edge_id))
        milestone.out_connections[neighbor_node] = self.edges[-1]
        neighbor_node.in_connections[milestone] = self.edges[-1]
        self.edge_id += 1

    def a_star(self, robot_radius, start_points, goal_points, writer):
        goal = tuple([self.points_to_nodes[p] for p in goal_points])

        def h(p):
            res = 0
            for p1, p2 in zip(p, goal):
                res += sqrt(Ker.squared_distance(p1.point_2, p2.point_2).to_double())
            return res

        def get_neighbors(points):
            connections_list = [list(p.out_connections.keys())+list(p.in_connections.keys()) for p in points]
            res = []
            for i in range(len(points)):
                is_good = True
                for next_point in connections_list[i]:
                    for j in range(len(points)):
                        if i == j:
                            continue
                        if Ker.squared_distance(next_point.point_2, points[j].point_2) < Ker.FT(4)*robot_radius*robot_radius:
                            is_good = False
                            break
                    if not is_good:
                        continue
                    seg = points[i].in_connections[next_point] if next_point in points[i].in_connections else points[i].out_connections[next_point]
                    for j in range(len(points)):
                        if i == j:
                            continue
                        if Ker.squared_distance(seg.segment, points[j].point_2) < Ker.FT(4)*robot_radius*robot_radius:
                            is_good = False
                            break
                    if not is_good:
                        continue
                    res.append((tuple([points[k] if k!=i else next_point for k in range(len(points))]), seg))
            return res

        temp_i = 0
        start = tuple([self.points_to_nodes[p] for p in start_points])

        def get_path(cf):
            c = goal
            path = [[p.point_2 for p in c]]
            while c != start:
                c = cf[c]
                path.append([p.point_2 for p in c])
            path.reverse()
            return path

        q = [(h(start), temp_i, start)]
        heapq.heapify(q)
        came_from = {}
        g_score = {start: 0}
        # temp_j = 0
        while len(q) > 0:
            curr_f_score, _, curr = heapq.heappop(q)
            if curr_f_score > (g_score[curr]+h(curr)):
                # temp_j += 1
                # if temp_j % 100000 == 0:
                #     print("temp_j", temp_j, file=writer)
                #     print(len(q), " ", len(came_from), file=writer)
                continue
            if curr == goal:
                return g_score[curr], get_path(came_from)
            for neighbor, edge in get_neighbors(curr):
                tentative_g_score = g_score[curr] + edge.cost
                if neighbor not in g_score or tentative_g_score < g_score[neighbor]:
                    came_from[neighbor] = curr
                    g_score[neighbor] = tentative_g_score
                    temp_i += 1
                    if temp_i % 100000 == 0:
                        print("Added", temp_i, "items to A_star heap", file=writer)
                        # print(len(q), " ", len(came_from), file=writer)
                        if temp_i % 1000000 == 0:
                            # TODO remove duplications?
                            gc.collect()
                    heapq.heappush(q, (tentative_g_score + h(neighbor), temp_i, neighbor))
        print("error no path found", file=writer)
        return 99999999999, []


def cords_to_2d_points(cords_x, cords_y):
    res = [SS.Point_d(2, [Ker.FT(x), Ker.FT(y)]) for x in cords_x for y in cords_y]
    return res


def cords_to_points_2(cords_x, cords_y):
    res = [Ker.Point_2(Ker.FT(x), Ker.FT(y)) for x in cords_x for y in cords_y]
    return res


def generate_milestones(cd, min, max):
    def conv(num):
        return (max-min)*num+min

    res = []
    if Config.sample_method == "staggered_grid":
        i = 0
        half_points_diff = (Config.edge_len / Config.balls_per_dim) / 2
        l1_cords = [conv(Config.delta + (2 * i - 1) * half_points_diff) for i in range(1, Config.balls_per_dim + 1)]
        l2_cords = [conv(Config.delta + (2 * i) * half_points_diff) for i in range(Config.balls_per_dim + 1)]
        l1 = cords_to_points_2(l1_cords, l1_cords)
        l2 = cords_to_points_2(l2_cords, l2_cords)
        all_points = l1+l2
        for point in all_points:
            if cd.is_point_valid(point):
                res.append(PrmNode(point, "v"+str(i)))
                i += 1
        return res

    if Config.sample_method == "grid":
        i = 0
        points_diff = (Config.edge_len / (Config.grid_points_per_dim-1))
        cords = [conv(Config.delta + i * points_diff) for i in range(Config.grid_points_per_dim)]
        points = cords_to_points_2(cords, cords)
        for point in points:
            if cd.is_point_valid(point):
                res.append(PrmNode(point, "v"+str(i)))
                i += 1
        return res

    if Config.sample_method == "random":
        i = 0
        points = [SS.Point_d(2, [Ker.FT(random.uniform(min+Config.delta, max-Config.delta)), Ker.FT(random.uniform(min+Config.delta, max-Config.delta))]) for _ in range(Config.num_of_sample_points)]
        for point in points:
            if cd.is_point_valid(point):
                res.append(PrmNode(point, "v"+str(i)))
                i += 1
        return res

    raise ValueError("Invalid configuration")


def make_graph(cd, milestones, nn, writer, min, max):
    g = PrmGraph(milestones, writer)
    for milestone in milestones:
        p = milestone.point
        nearest = nn.neighbors_in_radius(p, Config.connection_radius*Ker.FT(max-min))
        for neighbor in nearest:
            if neighbor == p:
                continue
            edge = Ker.Segment_2(Ker.Point_2(p[0], p[1]), Ker.Point_2(neighbor[0], neighbor[1]))
            if cd.is_edge_valid(edge):
                g.insert_edge(milestone, neighbor)

    return g


Config = Config()


def generate_path_disc(robots, obstacles, disc_obstacles, destinations, argument, writer, isRunning):
    with open(argument) as conf_file:
        conf_json = json.load(conf_file)
    Config.eps = conf_json["eps"]
    Config.delta = conf_json["delta"]
    Config.sample_method = conf_json["sample_method"]
    Config.is_multi_robot = True if len(robots) > 1 else False
    Config.reset()
    
    G = nx.Graph()
    path = []

    start_t = time.time()
    sources = [robot['center'] for robot in robots]
    radii = [robot['radius'] for robot in robots]
    if len(radii) == 0:
        print("Error: must have at least one robot", file=writer)
        return path, G
    for r in radii:
        if r != radii[0]:
            print("Error: this only works when all robots are of the same radii", file=writer)
            return path, G
    radius = Ker.FT(radii[0])

    cd = collision_detection.Collision_detector(obstacles,disc_obstacles, offset=radius)

    min_x, max_x, min_y, max_y = bounding_box.calc_bbox(obstacles, sources, destinations, radius)
    if min_x != min_y or max_x != max_y:
        print("Error: Must be used on square scenes", file=writer)
        return path, G
    milestones = []
    for (i, start_p) in enumerate(sources):
        milestones.append(PrmNode(start_p, "start"+str(i)))
    for (i, destination_p) in enumerate(destinations):
        milestones.append(PrmNode(destination_p, "goal"+str(i)))
    milestones += generate_milestones(cd, min_x, max_x)
    if not isRunning[0]:
        print("Aborted", file=writer)
        return path, G

    nn = NeighborsFinder([milestone.point for milestone in milestones])
    g = make_graph(cd, milestones, nn, writer, min_x, max_x)
    print("Vertices amount per robot:", len(g.points_to_nodes), file=writer)
    print("Edges amount per robot:", len(g.edges), file=writer)
    if not isRunning[0]:
        print("Aborted", file=writer)
        return path, G

    a_star_res, d_path = g.a_star(radius, [SS.Point_d(2, [start_p.x(), start_p.y()]) for start_p in sources],
                                  [SS.Point_d(2, [destination_p.x(), destination_p.y()]) for destination_p in destinations], writer )
    print("A_star_res: ", a_star_res, file=writer)
    took = time.time()-start_t
    print("Done, took: ", took, file=writer)
    for i in d_path:
        path.append(i)
    if len(path) > 0:
        G.add_node(conversions.to_point_d(path[0]))
        for i in range(len(path)-1):
            G.add_node(conversions.to_point_d(path[i+1]))
            G.add_edge(conversions.to_point_d(path[i]), conversions.to_point_d(path[i+1]))
    return path, G
