# # NEEDS TO BE UPDATED
from CGALPY_kerEpecInt_arr2CsExPl_bso2_bv_ch2_pol2_ms2_pp_ss8_tri2.Ker import *
from CGALPY_kerEpecInt_arr2CsExPl_bso2_bv_ch2_pol2_ms2_pp_ss8_tri2.Arr2 import *
from CGALPY_kerEpecInt_arr2CsExPl_bso2_bv_ch2_pol2_ms2_pp_ss8_tri2.Pol2 import *
from CGALPY_kerEpecInt_arr2CsExPl_bso2_bv_ch2_pol2_ms2_pp_ss8_tri2.SS import *
from CGALPY_kerEpecInt_arr2CsExPl_bso2_bv_ch2_pol2_ms2_pp_ss8_tri2.MN2 import *

import random
import networkx as nx
import time

assert(get_spatial_searching_dimension() == 4)


class Dynamic_kd_tree:
    tree = None
    buff = []
    nn = None
    eps = FT(Gmpq(0.0))  # 0.0 for exact NN, otherwise approximate NN
    search_nearest = True  # set this value to False in order to search farthest
    sort_neighbors = False  # set this value to True in order to obtain the neighbors sorted by distance
    ed = None

    def __init__(self):
        self.tree = Kd_tree()
        self.buff = []
        self.ed = Euclidean_distance()

    def insert(self, p):
        self.buff.append(p)
        if len(self.buff) > 100:
            self.tree.insert(self.buff)
            self.buff.clear()

    def nearest_neighbor(self, k, p):
        nn = K_neighbor_search(self.tree, p, k, self.eps, self.search_nearest, self.ed, self.sort_neighbors)
        dist_from_p = lambda point: self.ed.transformed_distance(p, point)
        res = []
        nn.k_neighbors(res)
        if res and self.buff:
            neighbor = res[0][0]
            return min(min(self.buff, key=dist_from_p), neighbor, key=dist_from_p)
        elif self.buff:
            return min(self.buff, key=dist_from_p)
        else:
            return res[0][0]

    def nearest_neighbors(self, k, p):
        nn = K_neighbor_search(self.tree, p, k, self.eps, self.search_nearest, self.ed, self.sort_neighbors)
        res = []
        nn.k_neighbors(res)
        return res


def calc_bbox(obstacles):
    X = []
    Y = []
    for poly in obstacles:
        for point in poly.vertices():
            X.append(point.x())
            Y.append(point.y())
    min_x = min(X)
    max_x = max(X)
    min_y = min(Y)
    max_y = max(Y)
    return min_x, max_x, min_y, max_y


def is_valid_landmark(pl, q):
    p0 = Point_2(q[0], q[1])
    p1 = Point_2(q[2], q[3])
    res = pl.locate(p0)
    if not res.is_face():
        return False
    else:
        f = Face()
        res.get_face(f)
        if f.data() > 0:
            return False
    res = pl.locate(p1)
    if not res.is_face():
        return False
    else:
        f = Face()
        res.get_face(f)
        if f.data() > 0:
            return False
    dx = p0.x() - p1.x()
    dy = p0.y() - p1.y()
    if FT(-1) < dx < FT(1) and FT(-1) < dy < FT(1):
        return False
    return True


def edge_valid(p, q, cspace, pl):
    p0 = Point_2(p[0], p[1])
    p1 = Point_2(p[2], p[3])
    q0 = Point_2(q[0], q[1])
    q1 = Point_2(q[2], q[3])

    c0 = Curve_2(Segment_2(p0, q0))
    c1 = Curve_2(Segment_2(p1, q1))
    curves = [c0, c1]
    for curve in curves:
        res = []
        zone(cspace, curve, res, pl)
        for obj in res:
            f = Face()
            if obj.get_face(f):
                if f.data() > 0:
                    return False

    # check for no self intersection
    square = Polygon_2([p0 + Vector_2(FT(-1), FT(-1)), p0 + Vector_2(FT(1), FT(-1)), p0 + Vector_2(FT(1), FT(1)),
                        p0 + Vector_2(FT(-1), FT(1))])
    q2 = q1 - Vector_2(p0, q0)
    s = Segment_2(p1, q2)
    for edge in square.edges():
        if not intersection(edge, s).empty(): return False
    if square.has_on_bounded_side(s.source()) or square.has_on_bounded_side(s.target()):
        return False

    return True


def get_new_point(nn, p, etha, d):
    coords = []
    for i in range(4):
        coords.append(nn[i] + FT((p[i] - nn[i]).to_double() / ((d.to_double()) ** 0.5)) * etha)
    new_point = Point_d(4, coords)
    # print(nn, new_point)
    return new_point


def generate_path_polygon(path, robots, obstacles, destination):
    t0 = time.perf_counter()
    etha = FT(1)
    obstacles = [Polygon_2(obstacle) for obstacle in obstacles]
    offset = Vector_2(FT(0), FT(0))
    ref0 = robots[0][0] + offset
    ref1 = robots[1][0] + offset

    G = nx.DiGraph()
    begin = Point_d(4, [ref0.x(), ref0.y(), ref1.x(), ref1.y()])
    end = Point_d(4, [destination[0].x(), destination[0].y(), destination[1].x(), destination[1].y()])
    G.add_node(begin)

    # set up kd tree
    tree = Dynamic_kd_tree()
    tree.insert(begin)
    eps = FT(Gmpq(0.0))  # 0.0 for exact NN, otherwise approximate NN
    search_nearest = True  # set this value to False in order to search farthest
    sort_neighbors = False  # set this value to True in order to obtain the neighbors sorted by distance

    traits = Arr_face_overlay_traits(lambda x, y: x + y)

    minus_square = Polygon_2([Point_2(-1, -1), Point_2(0, -1), Point_2(0, 0), Point_2(-1, 0)])
    arrangements = []
    for obstacle in obstacles:
        ms = minkowski_sum_2(obstacle, minus_square)
        arr = Arrangement_2()
        # Arrangement for sum
        insert(arr, [Curve_2(edge) for edge in ms.outer_boundary().edges()])
        for hole in ms.holes():
            insert(arr, [Curve_2(edge) for edge in hole.edges()])
        ubf = arr.unbounded_face()
        ubf.set_data(0)
        invalid_face = next(next(ubf.inner_ccbs())).twin().face()
        invalid_face.set_data(1)
        for ccb in invalid_face.inner_ccbs():
            valid_face = next(ccb).twin().face()
            valid_face.set_data(0)
        arrangements.append(arr)

    # overlay arrangements
    initial = Arrangement_2()
    ubf = initial.unbounded_face()
    ubf.set_data(0)
    arrangements.insert(0, initial)
    res = None
    for i in range(len(arrangements) - 1):
        res = Arrangement_2()
        overlay(arrangements[i], arrangements[i + 1], res, traits)
        arrangements[i + 1] = res

    cspace = res

    pl = Arr_trapezoid_ric_point_location(cspace)

    bbox = calc_bbox(obstacles)
    x_range = (bbox[0].to_double(), bbox[1].to_double())
    y_range = (bbox[2].to_double(), bbox[3].to_double())

    lst = []
    ed = Euclidean_distance()
    i = 0
    j = 0
    done = False
    while not done:
        print(i)
        if i % 500 == 0:
            print("Number of valid points sampled:", i)
            i += 1
            p = end
            nn = tree.nearest_neighbor(1, p)
            if edge_valid(nn, p, cspace, pl):
                # d = distance.transformed_distance(p, neighbor).to_double()
                d = 1
                G.add_edge(p, nn)
                G.add_edge(nn, p)
                done = True
                break
        else:
            rand_x0 = FT(random.uniform(x_range[0], x_range[1]))
            rand_y0 = FT(random.uniform(y_range[0], y_range[1]))
            rand_x1 = FT(random.uniform(x_range[0], x_range[1]))
            rand_y1 = FT(random.uniform(y_range[0], y_range[1]))
            p = Point_d(4, [rand_x0, rand_y0, rand_x1, rand_y1])
            if is_valid_landmark(pl, p):
                i += 1
                nn = tree.nearest_neighbor(1, p)
                d = ed.transformed_distance(p, nn)
                if d < etha:
                    new_point = p
                    # print("less than etha")
                else:
                    new_point = get_new_point(nn, p, etha, d)
                if edge_valid(nn, new_point, cspace, pl):
                    G.add_node(new_point)
                    G.add_edge(nn, new_point)
                    G.add_edge(new_point, nn)
                    tree.insert(new_point)
                    j += 1

    print("done")
    print("tree size:", j)
    G.add_node(end)

    if nx.has_path(G, begin, end):
        print("path found")
        temp = nx.shortest_path(G, begin, end)
        for p in temp:
            path.append([Point_2(p[0], p[1]), Point_2(p[2], p[3])])
        print(path)
        t1 = time.perf_counter()
        print("time:", t1 - t0)
    return
