import sys
import os.path
sys.path.insert(0, os.path.dirname(__file__))

from bindings import *
import Collision_detection
import networkx as nx
import random
import time
import math
import conversions
import sum_distances
import bounding_box

# the function get_spatial_searching_dimension() is
# part of the CGAL Python bindings for spatial search (Kd_trees etc)
dim = SS.get_spatial_searching_dimension()
assert dim % 2 == 0

# Number of nearest neighbors to search for in the k-d tree
K = 15
# K = 35

# we use a Nearest Neighbor search structure, called Kd_tree, from CGAL

# throughout the code, wherever we need to return a number of type double to CGAL,
# we convert it using FT() (which stands for field number type)

# find one free landmark (milestone) within the bounding box
def sample_valid_landmark(min_x, max_x, min_y, max_y, collision_detectors, num_robots, radii):
    while True:
        points = []
        # for each robot check that its configuration (point) is in the free space
        for i in range(num_robots):
            rand_x = FT(random.uniform(min_x, max_x))
            rand_y = FT(random.uniform(min_y, max_y))
            p = Point_2(rand_x, rand_y)
            if collision_detectors[i].is_point_valid(p):
                points.append(p)
            else:
                break
        # verify that the robots do not collide with one another at the sampled configuration
        if len(points) == num_robots and not Collision_detection.check_intersection_static(points, radii):
            return conversions.to_point_d(points)


# check whether the edge pq is collision free
# the collision detection module sits on top of CGAL arrangements
def edge_valid(collision_detectors, p: Point_d, q: Point_d, num_robots, radii):
    p = conversions.to_point_2_list(p, num_robots)
    q = conversions.to_point_2_list(q, num_robots)
    edges = []
    # for each robot check that its path (line segment) is in the free space
    for i in range(num_robots):
        edge = Segment_2(p[i], q[i])
        if not collision_detectors[i].is_edge_valid(edge):
            return False
        edges.append(edge)
    # verify that the robots do not collide with one another along the C-space edge
    if Collision_detection.check_intersection_against_robots(edges, radii):
        return False
    return True


# generate_path() is our main PRM function
# it constructs a PRM (probabilistic roadmap)
# and searches in it for a path from start (robots) to target (destinations)
def generate_path_disc(robots, obstacles, disc_obstacles, destinations, argument, writer, isRunning):
    t0 = time.perf_counter()
    path = []
    try:
        num_landmarks = int(argument)
    except Exception as e:
        print("argument is not an integer", file=writer)
        return path
    print("num_landmarks=", num_landmarks, file=writer)
    num_robots = len(robots)
    print("num_robots=", num_robots, file=writer)
    # for technical reasons related to the way the python bindings for this project were generated, we need
    # the condition "(dim / num_robots) >= 2" to hold
    if num_robots == 0 or (dim / num_robots) < 2:
        print("unsupported number of robots:", num_robots, file=writer)
        return path
    # compute the free C-space of a single robot by expanding the obstacles by the disc robot radius
    # and maintaining a representation of the complement of the expanded obstacles
    sources = [robot['center'] for robot in robots]
    radii = [robot['radius'] for robot in robots]
    collision_detectors = [Collision_detection.Collision_detector(obstacles, disc_obstacles, radius) for radius in radii]
    min_x, max_x, min_y, max_y = bounding_box.calc_bbox(obstacles, sources, destinations, max(radii))

    # turn the start position of the robots (the array robots) into a d-dim point, d = 2 * num_robots
    sources = conversions.to_point_d(sources)
    # turn the target position of the robots (the array destinations) into a d-dim point, d = 2 * num_robots
    destinations = conversions.to_point_d(destinations)
    # we use the networkx Python package to define and manipulate graphs
    # G is an undirected graph, which will represent the PRM
    G = nx.Graph()
    # we also add these two configurations as nodes to the PRM G
    G.add_nodes_from([sources, destinations])
    # we also maintain a Nearest-Neighbor structure, called Kd_tree, from CGAL
    # initially we insert two d-dim points: the start and target configurations
    tree = Kd_tree([sources, destinations])
    # we also add these two configurations as nodes to the PRM G
    G.add_nodes_from([sources, destinations])
    # setting three parameters that will be used by the NN search function K_neighbor_search_python()
    eps = FT(0)  # 0.0 for exact NN, otherwise approximate NN
    search_nearest = True  # set this value to False in order to search farthest
    sort_neighbors = False  # set this value to True in order to obtain the neighbors sorted by distance
    print('Sampling landmarks', file=writer)

    # sampling
    for i in range(num_landmarks):
        if not isRunning[0]:
            print("Aborted", file=writer)
            return path, G

        p = sample_valid_landmark(min_x, max_x, min_y, max_y, collision_detectors, num_robots, radii)
        G.add_node(p)
        tree.insert(p)
        if i % 500 == 0:
            print(i, "landmarks sampled", file=writer)
    print(num_landmarks, "landmarks sampled", file=writer)

    # preparing the distance object, which describes our distance metric for the nearest neighbor search
    distance = sum_distances.sum_distances(num_robots)

    print('Connecting landmarks', file=writer)
    i = 0
    new_points = []
    tree.points(new_points)
    for p in new_points:
        if not isRunning[0]:
            print("Aborted", file=writer)
            return G
        # Uncomment the first search command and comment out the search command following it to use the custom distance
        # defined in the top of the file as the metric for neighbor search

        # Use custom distance metric as the distance metric for nearest neighbor search
        search = SS.K_neighbor_search_python(tree, p, K, eps, search_nearest, distance, sort_neighbors)

        # Use standard d-dimensional euclidean distance as the distance metric for nearest neighbor search
        # search = K_neighbor_search(tree, p, K, eps, search_nearest, Euclidean_distance(), sort_neighbors)

        res = []
        search.k_neighbors(res)
        if edge_valid(collision_detectors, p, destinations, num_robots, radii):
            d = distance.transformed_distance(p, destinations).to_double()
            G.add_edge(p, destinations, weight=d)
        for pair in res:
            neighbor = pair[0]
            if not G.has_edge(p, neighbor):
                # check if we can add an edge to the graph
                if edge_valid(collision_detectors, p, neighbor, num_robots, radii):
                    d = distance.transformed_distance(p, neighbor).to_double()
                    G.add_edge(p, neighbor, weight=d)
        if i % 500 == 0:
            print('Connected', i, 'landmarks to their nearest neighbors', file=writer)
        i += 1

    if nx.has_path(G, sources, destinations):
        temp = nx.dijkstra_path(G, sources, destinations, weight='weight')
        lengths = [0 for _ in range(num_robots)]
        if len(temp) > 1:
            for i in range(len(temp) - 1):
                p = temp[i]
                q = temp[i + 1]
                for j in range(num_robots):
                    dx = p[2 * j].to_double() - q[2 * j].to_double()
                    dy = p[2 * j + 1].to_double() - q[2 * j + 1].to_double()
                    lengths[j] += math.sqrt((dx * dx + dy * dy))
        print("A path of length", sum(lengths), "was found", file=writer)
        for i in range(num_robots):
            print('Length traveled by robot', i, ":", lengths[i], file=writer)
        for p in temp:
            path.append(conversions.to_point_2_list(p, num_robots))
    else:
        print("No path was found", file=writer)
    t1 = time.perf_counter()
    print("Time taken:", t1 - t0, "seconds", file=writer)
    return path, G
