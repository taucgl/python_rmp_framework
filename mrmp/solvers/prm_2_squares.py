# Assumes scene consists of exactly two unit square robots inside a bounded scene
import math

import random
import networkx as nx
import time
from bindings import *


dim = SS.get_spatial_searching_dimension()
assert dim % 2 == 0


def to_point_d(points) -> Point_d:
    res = []
    for p in points:
        res.append(p.x())
        res.append(p.y())
    for _ in range(len(points), dim//2):
        res.append(FT(0))
        res.append(FT(0))
    res = Point_d(dim, res)
    return res


def calc_bbox(obstacles):
    X = []
    Y = []
    for poly in obstacles:
        for point in poly.vertices():
            X.append(point.x())
            Y.append(point.y())
    min_x = min(X)
    max_x = max(X)
    min_y = min(Y)
    max_y = max(Y)
    return min_x, max_x, min_y, max_y


def is_valid_landmark(pl, q):
    p0 = TPoint(q[0], q[1])
    p1 = TPoint(q[2], q[3])
    res = pl.locate(p0)
    if not res.is_face():
        return False
    else:
        f = Face()
        res.get_face(f)
        if f.data() > 0:
            return False
    res = pl.locate(p1)
    if not res.is_face():
        return False
    else:
        f = Face()
        res.get_face(f)
        if f.data() > 0:
            return False
    dx = (p0.x() - p1.x()).a0()
    dy = (p0.y() - p1.y()).a0()
    if FT(-1) < dx < FT(1) and FT(-1) < dy < FT(1):
        return False
    return True


def edge_valid(p, q, cspace, pl):
    p0 = Point_2(p[0], p[1])
    p1 = Point_2(p[2], p[3])
    q0 = Point_2(q[0], q[1])
    q1 = Point_2(q[2], q[3])

    c0 = X_monotone_curve_2(p0, q0)
    c1 = X_monotone_curve_2(p1, q1)
    curves = [c0, c1]
    for curve in curves:
        res = []
        Aos2.zone(cspace, curve, res, pl)
        for obj in res:
            if type(obj) == Face:
                if obj.data() > 0:
                    return False

    # check for no self intersection
    square = Polygon_2([p0 + Vector_2(FT(-1), FT(-1)), p0 + Vector_2(FT(1), FT(-1)), p0 + Vector_2(FT(1), FT(1)),
                        p0 + Vector_2(FT(-1), FT(1))])
    q2 = q1 - Vector_2(p0, q0)
    s = Segment_2(p1, q2)
    for edge in square.edges():
        if not Ker.intersection(edge, s).empty():
            return False
    if square.has_on_bounded_side(s.source()) or square.has_on_bounded_side(s.target()):
        return False

    return True


def generate_path_polygon(robots, obstacles, destinations, argument, writer, isRunning):
    path = []
    try:
        num_landmarks = int(argument)
    except Exception as e:
        print("argument is not an integer", file=writer)
        return path
    t0 = time.perf_counter()
    obstacles = [Polygon_2(obstacle) for obstacle in obstacles]
    offset = Vector_2(FT(0), FT(0))
    ref0 = robots[0][0] + offset
    ref1 = robots[1][0] + offset

    G = nx.DiGraph()
    begin = to_point_d([ref0, ref1])
    end = to_point_d([destinations[0], destinations[1]])
    G.add_node(begin)
    G.add_node(end)

    # set up kd tree
    tree = Kd_tree([begin, end])
    eps = FT(Gmpq(0.0))  # 0.0 for exact NN, otherwise approximate NN
    search_nearest = True  # set this value to False in order to search farthest
    sort_neighbors = False  # set this value to True in order to obtain the neighbors sorted by distance

    traits = Aos2.Arr_face_overlay_traits(lambda x, y: x + y)
    # Construct c-space composed of dilated obstacles
    minus_square = Polygon_2([Point_2(-1, -1), Point_2(0, -1), Point_2(0, 0), Point_2(-1, 0)])
    arrangements = []
    for obstacle in obstacles:
        ms = MN2.minkowski_sum_2(obstacle, minus_square)
        arr = Arrangement_2()
        # Arrangement for sum
        Aos2.insert(arr, [Curve_2(edge) for edge in ms.outer_boundary().edges()])
        for hole in ms.holes():
            Aos2.insert(arr, [Curve_2(edge) for edge in hole.edges()])
        ubf = arr.unbounded_face()
        ubf.set_data(0)
        invalid_face = next(next(ubf.inner_ccbs())).twin().face()
        invalid_face.set_data(1)
        for ccb in invalid_face.inner_ccbs():
            valid_face = next(ccb).twin().face()
            valid_face.set_data(0)
        arrangements.append(arr)

    # overlay arrangements
    initial = Arrangement_2()
    ubf = initial.unbounded_face()
    ubf.set_data(0)
    arrangements.insert(0, initial)
    res = None
    for i in range(len(arrangements) - 1):
        res = Arrangement_2()
        Aos2.overlay(arrangements[i], arrangements[i + 1], res, traits)
        arrangements[i + 1] = res

    cspace = res
    # for face in cspace.faces():
    #   print(face.data())

    landmarks_pl = Aos2.Arr_trapezoid_ric_point_location(cspace)

    bbox = calc_bbox(obstacles)
    x_range = (bbox[0].to_double(), bbox[1].to_double())
    y_range = (bbox[2].to_double(), bbox[3].to_double())

    lst = []
    i = 0
    while i < num_landmarks:
        rand_x0 = FT(random.uniform(x_range[0], x_range[1]))
        rand_y0 = FT(random.uniform(y_range[0], y_range[1]))
        p1 = Point_2(rand_x0, rand_y0)
        rand_x1 = FT(random.uniform(x_range[0], x_range[1]))
        rand_y1 = FT(random.uniform(y_range[0], y_range[1]))
        p2 = Point_2(rand_x1, rand_y1)
        p = to_point_d([p1, p2])
        if is_valid_landmark(landmarks_pl, p):
            G.add_node(p)
            tree.insert(p)
            i += 1

    k = 15
    i = 0
    new_points = []
    tree.points(new_points)
    # print(new_points)
    for p in new_points:
        # search = SS.K_neighbor_search(tree, p, k, eps, search_nearest, distance, sort_neighbors)
        search = SS.K_neighbor_search(tree, p, k, eps, search_nearest, Euclidean_distance(), sort_neighbors)
        res = []
        search.k_neighbors(res)
        for pair in res:
            neighbor = pair[0]
            if not G.has_edge(p, neighbor):
                # if not ds.connected(p, neighbor):
                # check if we can add an edge to the graph
                if edge_valid(neighbor, p, cspace, landmarks_pl):
                    # d = distance.transformed_distance(p, neighbor).to_double()
                    d = math.sqrt(Euclidean_distance().transformed_distance(p, neighbor).to_double())
                    G.add_edge(p, neighbor, weight=d)
                    G.add_edge(neighbor, p, weight=d)
        if i % 500 == 0:
            print(i, file=writer)
        i += 1

    if nx.has_path(G, begin, end):
        print("path found", file=writer)
        temp = nx.shortest_path(G, begin, end)
        for p in temp:
            path.append([Point_2(p[0], p[1]), Point_2(p[2], p[3])])
        print(path, file=writer)
        t1 = time.perf_counter()
        print("time:", t1 - t0, file=writer)
    return path, G
