# CoFix - Multirobot Coordinated Fixed Motion Paths

The problem of coordinating the movement of multiple robots, while maintaining fixed distance.

Make sure to run `pipenv install` in the root directory first. Then after `pipenv shell` you can run (from the root directory)

> python cofix/cofix.py

You may also add path to JSON file with the format {"first_path": [[x1,y1], [x2,y2],...], "second_path": [[x1,y1], [x2,y2],...]} to preload paths to the editor. Then add the path to JSON as a second argument. An example is provided as follows:

> python cofix/cofix.py cofix/misc/paths_test.json

Press on `Show CSPACE` to both show samples in the CSPACE of the problem and compute a (non-exact) PRM solution. You can animate the PRM solution (or any other one) by pressing the `Animate Motion` button. CSPACE paths are provided as space seperate floats in the format "u1 v1 u2 v2 ..." (where u,v are coordinates in CSPACE).