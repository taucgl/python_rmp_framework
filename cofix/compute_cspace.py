
def l2_distance(p1, p2):
    """
    Get two points p1=(x1, y1) and p2=(x2,y2) and compute their L2 distance
    """
    x1, y1 = p1
    x2, y2 = p2
    return ((x1 - x2)**2 + (y1-y2)**2)**0.5

class Path(object):
    """
    A representation of a single path.
    This allows us to go back and forward from CSPACE to Workspace
    """
    def __init__(self, points):
        self.points = points
        self.lengths = [] # store for each point its relative length in path
        self.preprocess_lengths()


    def preprocess_lengths(self):
        """
        compute for each point its relative length along the path
        """
        total_dist = 0.0
        self.lengths.append(total_dist) # First point if of length zero
        for i in range(1, len(self.points)):
            total_dist += l2_distance(self.points[i-1], self.points[i])
            self.lengths.append(total_dist)

    
    def get_cspace_bounds(self):
        """
        Return the minimum and maximum values of the CSPACE for the given path
        """
        return self.lengths[0], self.lengths[-1]


    def cspace_to_workspace(self, val):
        """
        Get coordinate in CSPACE and translate it to workspace
        """
        # First find the correct segment
        # The correct segment will be (i-1, i)
        i = 0
        while val > self.lengths[i]:
            i += 1
            if i == len(self.lengths):
                i = len(self.lengths) - 1
                break
        if i == 0:
            i = 1

        # Find the alpha value between the points
        alpha = (val - self.lengths[i-1]) / l2_distance(self.points[i-1], self.points[i])
        x = self.points[i-1][0] * (1 - alpha) + self.points[i][0] * alpha
        y = self.points[i-1][1] * (1 - alpha) + self.points[i][1] * alpha
        return (x, y)

    def cspace_segment_to_workspace_path(self, val1, val2):
        """
        Get two values in CSPACE and translate to a list of (valid) points
        in the worksapce

        Note that this is necessary since naive interpolation might "skip"
        points along the path. Linear motion in CSPACE does not translate to linear
        motion in workspace (but to a piecewise-linear motion)
        """
        mid_vals = []
        if val1 <= val2:
            mid_vals.append(val1)
            for val in self.lengths:
                if val1 + 0.01 <= val <= val2 - 0.01:
                    mid_vals.append(val)
            mid_vals.append(val2)
        else:
            mid_vals.append(val1)
            for val in self.lengths[::-1]:
                if val2 + 0.01 <= val <= val1 - 0.01:
                    mid_vals.append(val)
            mid_vals.append(val2)
        
        return [self.cspace_to_workspace(val) for val in mid_vals]

        