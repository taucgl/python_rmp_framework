import os
import sys
import json
import random
import webbrowser

import networkx as nx
import sklearn.neighbors
from gui import RPolygon, RDisc
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtCore import QSequentialAnimationGroup

from cofix_gui import *
import compute_cspace

K = 30

class CSPACE(GUI):
    def setupUi(self):
        #########################
        # Setup the Qt UI layout
        #########################
        # Resize the main window and set the stylesheet (CSS)
        MainWindow = self.mainWindow
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        MainWindow.setStyleSheet("QMainWindow { background-color : rgb(54, 57, 63); color : rgb(220, 221, 222); }\n"
                                 "QLabel { background-color : rgb(54, 57, 63); color : rgb(220, 221, 222); }")

        # Add a graphics widget to the main window
        self.graphicsView = QtWidgets.QGraphicsView(self.mainWindow)
        self.graphicsView.setEnabled(True)
        self.graphicsView.setGeometry(QtCore.QRect(10, 10, 780, 580))
        self.graphicsView.setObjectName("graphicsView")

        # Zoom out a bit
        self.zoom = 50

def setup_logic(gui):
    gui.set_logic('mode_first_path', lambda: set_drawing_mode(gui, MODE_FIRST_PATH))
    gui.set_logic('mode_second_path', lambda: set_drawing_mode(gui, MODE_SECOND_PATH))
    set_drawing_mode(gui, MODE_FIRST_PATH)
    gui.set_animation_finished_action(lambda: None)

    gui.set_logic('clear', lambda: clear_scene(gui))
    gui.set_logic('show_cspace', lambda: show_cspace(gui))
    gui.set_logic('animate', lambda: animate(gui))
    gui.set_logic('set_radius', lambda: set_radius(gui))
    gui.set_logic('set_dist', lambda: set_max_dist(gui))
    gui.set_logic('set_samples', lambda: set_samples(gui))
    

    gui.set_field('radius', str(gui.radius))
    gui.set_field('max_dist', str(gui.max_distance))
    gui.set_field('samples', str(gui.samples))


    gui.scene.right_click_signal.connect(lambda x, y: right_click(gui, x, y))


def approximate_collision_detection(p1, p2, first_path, second_path, max_distance, radius, samples=5):
    u1, v1, = p1
    u2, v2 = p2

    sample_points = []
    du = (u2 - u1) / samples
    dv = (v2 - v1) / samples
    for i in range(1, samples):
        sample_points.append([u1 + i * du, v1 + i * dv])
    
    for u, v in sample_points:
        q1 = first_path.cspace_to_workspace(u)
        q2 = second_path.cspace_to_workspace(v)
        dist = compute_cspace.l2_distance(q1, q2)
        if dist > max_distance or dist < 2 * radius:
            return False
    
    return True


def cspace_prm(landmarks, min_u, max_u, min_v, max_v, first_path, second_path, max_distance, radius):
    G = nx.Graph()
    begin = (min_u, min_v)
    end = (max_u, max_v)
    G.add_node(begin)
    G.add_node(end)
    landmarks += [begin, end]

    nearest_neighbors = sklearn.neighbors.NearestNeighbors(n_neighbors=K, metric=compute_cspace.l2_distance, algorithm='auto')
    nearest_neighbors.fit(landmarks)

    for i in range(len(landmarks)):
        p = landmarks[i]
        k_neighbors = nearest_neighbors.kneighbors([landmarks[i]], return_distance=False)
        for j in k_neighbors[0]:
            neighbor = landmarks[j]
            if approximate_collision_detection(p, neighbor, first_path, second_path, max_distance, radius):
                weight = compute_cspace.l2_distance(p, neighbor)
                G.add_edge(tuple(p), tuple(neighbor), weight=weight)
        
        if i % 100 == 0:
            print('Connected', i, 'landmarks to their nearest neighbors')
    
    if nx.has_path(G, begin, end):
        return nx.shortest_path(G, begin, end)
    
    return None


def show_cspace(gui):
    first_path = compute_cspace.Path(gui.first_path)
    second_path = compute_cspace.Path(gui.second_path)

    print(gui.first_path)
    print(gui.second_path)
    
    cspace = CSPACE()
    cspace.set_program_name("CSPACE")
    cspace.set_animation_finished_action(lambda: None)

    # Draw random samples
    landmarks = []
    for _ in range(gui.samples):
        u = random.uniform(*first_path.get_cspace_bounds())
        v = random.uniform(*second_path.get_cspace_bounds())

        p1 = first_path.cspace_to_workspace(u)
        p2 = second_path.cspace_to_workspace(v)

        color = QtCore.Qt.green
        dist = compute_cspace.l2_distance(p1, p2)
        if dist > gui.max_distance or dist < 2 * gui.radius:
            color = QtCore.Qt.red
        else:
            landmarks.append([u, v])
        cspace.add_disc(SAMPLE_RADIUS, u, v, fill_color=color, line_color=QtCore.Qt.transparent)

    # Also try to PRM
    cspace_path = cspace_prm(
        landmarks, *first_path.get_cspace_bounds(), *second_path.get_cspace_bounds(),
        first_path, second_path, gui.max_distance, gui.radius)
    if cspace_path is not None:
        for i in range(len(cspace_path)):
            cspace.add_disc(POINT_RADIUS, cspace_path[i][0], cspace_path[i][1], fill_color=QtCore.Qt.magenta)
            if i > 0:
                cspace.add_segment(cspace_path[i-1][0], cspace_path[i-1][1], cspace_path[i][0], cspace_path[i][1], QtCore.Qt.magenta)
        # Also add this as string path
        path_floats = []
        for p in cspace_path:
            path_floats += p
        path_str = ' '.join(map(str, path_floats))
        gui.pathEdit.setPlainText(path_str)
            

    # Draw axis and gridlines
    min_u, max_u = first_path.get_cspace_bounds()
    min_v, max_v = second_path.get_cspace_bounds()

    cspace.add_segment(min_u, min_v, max_u + 0.2, min_v)
    cspace.add_segment(min_u, min_v, min_u, max_v + 0.2)

    for i in range(len(first_path.lengths)):
        cspace.add_segment(first_path.lengths[i], min_v, first_path.lengths[i], max_v, opacity=0.4)
        cspace.add_text('{:.2f}'.format(first_path.lengths[i]), first_path.lengths[i], min_u - 1, 12)
    for i in range(len(second_path.lengths)):
        cspace.add_segment(min_u, second_path.lengths[i], max_u, second_path.lengths[i], opacity=0.4)
        cspace.add_text('{:.2f}'.format(second_path.lengths[i]), min_v - 1, second_path.lengths[i], 12)

    cspace.zoom = 50
    cspace.redraw()

    gui.cspace = cspace

    cspace.mainWindow.show()


def animate(gui):
    clear_robots(gui)

    path_str = gui.pathEdit.toPlainText()
    tokens = [token for token in path_str.split() if len(token) > 0]
    uv_path = []
    for i in range(len(tokens) // 2):
        uv_path.append([float(tokens[2*i]), float(tokens[2*i+1])])
    
    # Convert UV path to simulatinous animation in workspace
    first_path = compute_cspace.Path(gui.first_path)
    second_path = compute_cspace.Path(gui.second_path)

    gui.first_robot = gui.add_disc_robot(gui.radius, *first_path.points[0], fill_color=FIRST_PATH_COLOR)
    gui.second_robot = gui.add_disc_robot(gui.radius, *second_path.points[0], fill_color=SECOND_PATH_COLOR)

    print(second_path.cspace_to_workspace(0))
    print(second_path.cspace_to_workspace(3.16))

    animations = []
    for i in range(len(uv_path) - 1):
        u1, v1 = uv_path[i]
        u2, v2 = uv_path[i+1]

        first_segment = first_path.cspace_segment_to_workspace_path(u1, u2)
        first_animation = QSequentialAnimationGroup()
        for i in range(len(first_segment)-1):
            first_animation.addAnimation(gui.linear_translation_animation(gui.first_robot, *first_segment[i], *first_segment[i+1], ANIM_DURATION / (len(first_segment)-1)))
 
        second_segment = second_path.cspace_segment_to_workspace_path(v1, v2)
        second_animation = QSequentialAnimationGroup()
        for i in range(len(second_segment)-1):
            second_animation.addAnimation(gui.linear_translation_animation(gui.second_robot, *second_segment[i], *second_segment[i+1], ANIM_DURATION / (len(second_segment)-1)))

        animations.append(gui.parallel_animation(
            first_animation, second_animation
        ))
    gui.queue_animation(*animations)
        
    try:
        animations = []
        cspace_robot = gui.cspace.add_disc_robot(gui.radius, 0, 0, fill_color=QtCore.Qt.magenta)
        for i in range(len(uv_path) - 1):
            u1, v1 = uv_path[i]
            u2, v2 = uv_path[i+1]
            animations.append(gui.cspace.linear_translation_animation(cspace_robot, u1, v1, u2, v2, ANIM_DURATION))
        gui.cspace.queue_animation(*animations)
        gui.cspace.play_queue()
    except Exception as e:
        print(str(e))
    
    gui.play_queue()



def set_samples(gui):
    gui.samples = int(gui.get_field('samples'))


def set_radius(gui):
    gui.radius = float(gui.get_field('radius'))


def set_max_dist(gui):
    gui.max_distance = float(gui.get_field('max_dist'))


def set_drawing_mode(gui, mode):
    """
    Set the drawing mode to the selected one
    (And update the labels accordingly)
    """
    gui.drawing_mode = mode
    gui.set_label('current_mode', MODE_CURRENT_STR.format(MODE_NAMES[mode])) 

def clear_scene(gui):
    gui.first_path = []
    for vertex in gui.first_path_vertices:
        gui.scene.removeItem(vertex.disc)
    gui.first_path_vertices.clear()
    for edge in gui.first_path_edges:
        gui.scene.removeItem(edge.line)
    gui.first_path_edges.clear()
    gui.second_path = []
    for vertex in gui.second_path_vertices:
        gui.scene.removeItem(vertex.disc)
    gui.second_path_vertices.clear()
    for edge in gui.second_path_edges:
        gui.scene.removeItem(edge.line)
    gui.second_path_edges.clear()

    clear_robots(gui)

def clear_robots(gui):
    if gui.first_robot is not None:
        gui.scene.removeItem(gui.first_robot.disc)
        gui.first_robot = None
    if gui.second_robot is not None:
        gui.scene.removeItem(gui.second_robot.disc)
        gui.second_robot = None

def redraw_grid(gui, size):
    """
    Draw a grid in out gui with a given size
    """
    # Clear any old grid lines
    for segment in gui.grid:
        gui.scene.removeItem(segment.line)
    gui.grid.clear()

    length = size * gui.resolution
    
    # Draw the grid lines (except axes)
    color = QtCore.Qt.lightGray
    for i in range(-size, size):
        if i == 0:
            continue # We will draw the main axes sperately
        i = i * gui.resolution
        gui.grid.append(gui.add_segment(-length, i, length, i, line_color=color))
        gui.grid.append(gui.add_segment(i, -length, i, length, line_color=color))
    
    # Draw the axes
    color = QtCore.Qt.black
    gui.grid.append(gui.add_segment(-length, 0, length, 0, line_color=color))
    gui.grid.append(gui.add_segment(0, -length, 0, length, line_color=color))

    # Push all segments to the back
    for segment in gui.grid:
        segment.line.setZValue(-1)


def right_click(gui, x: float, y: float):
    """
    Handles the drawing by user (which is done by right clicking)
    """
    x = gui.resolution * round(x / gui.resolution)
    y = gui.resolution * round(y / gui.resolution)
    path = gui.first_path
    path_vertices = gui.first_path_vertices
    path_edges = gui.first_path_edges
    color = FIRST_PATH_COLOR
    if gui.drawing_mode == MODE_SECOND_PATH:
        path = gui.second_path
        path_vertices = gui.second_path_vertices
        path_edges = gui.second_path_edges
        color = SECOND_PATH_COLOR
    
    path.append([x, y])
    path_vertices.append(gui.add_disc(POINT_RADIUS, x, y, fill_color=color))
    if len(path) > 1:
        path_edges.append(gui.add_segment(*path[-2], *path[-1], line_color=color))


def load_from_json(gui, filename):
    with open(filename, 'r') as fp:
        d = json.load(fp)
    
    for p in d['first_path']:
        x, y = p
        gui.first_path.append(p)
        gui.first_path_vertices.append(gui.add_disc(POINT_RADIUS, x, y, fill_color=FIRST_PATH_COLOR))
        if len(gui.first_path) > 1:
            gui.first_path_edges.append(gui.add_segment(*gui.first_path[-2], *gui.first_path[-1], line_color=FIRST_PATH_COLOR))
    for p in d['second_path']:
        x, y = p
        gui.second_path.append(p)
        gui.second_path_vertices.append(gui.add_disc(POINT_RADIUS, x, y, fill_color=SECOND_PATH_COLOR))
        if len(gui.second_path) > 1:
            gui.second_path_edges.append(gui.add_segment(*gui.second_path[-2], *gui.second_path[-1], line_color=SECOND_PATH_COLOR))


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    gui = GUI_CoFix()
    gui.set_program_name("CoFix - Multirobot Coordinated Fixed Motion Paths")

    setup_logic(gui)

    # Load paths from json if needed
    if len(sys.argv) >= 2:
        load_from_json(gui, sys.argv[1])

    # Draw the grid with origin
    redraw_grid(gui, GRID_SIZE)
    gui.add_disc(POINT_RADIUS, 0, 0)

    gui.mainWindow.show()
    sys.exit(app.exec_())